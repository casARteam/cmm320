///*****************************************************************************
/*
|*  Copyright       :   (c) 2011 CAS
|*  Filename        :   driver_key.c
|*  Version         :   0.1
|*  Programmer(s)   :   QI ZHY ING (QZI)
|*  Created         :   2011/5/9
|*  Modified        :
|*  Description     :   CT100 key driver function
*/
//****************************************************************************/

#include "globals.h"
#include "stm32f4xx_gpio.h"
#include "RingBuf.h"
#include "key.h"


//Key scan out table
#define KEY_DRV_MAX_KEY_NUM	        16
#define KEY_DRV_BUF_SIZE	        15
#define KEY_DRV_IN_PIN_NUM	 	    4
#define KEY_DRV_OUT_PIN_NUM		    4
#define KEY_DRV_PROC_TIME_INTERVAL	1
#define KEY_DRV_VALID_TIME	        40
#define KEY_DRV_LONG_VALID_TIME	    800//3000 
RING_BUF  KeyDrvRingBuf;


static const INT8U KeyDrvConvTable[KEY_DRV_MAX_KEY_NUM] =
{
    KEY_PRESET_1,  KEY_PRESET_2,   KEY_PRESET_3,   KEY_PRESET_4,
    KEY_PRESET_5,  KEY_PRESET_6,   KEY_PRESET_7,   KEY_PRESET_8,
    KEY_PRESET_9,  KEY_PRESET_10,  KEY_PRESET_11,  KEY_PRESET_12,
    KEY_PRESET_13, KEY_PRESET_14,  KEY_PRESET_15,  KEY_PRESET_16
};


static char  keyDrvData[KEY_DRV_BUF_SIZE];
static INT8U  keyDrvIndex;
static INT16U keyDrvPressCount;
static INT16U keyDrvValidCount;
static INT16U keyDrvLongValidCount;

static INT8U keyRawData = 0;
static INT8U keyValidRawData = 0;

BOOLEAN keyPressedFlag;
BOOLEAN KeyLongPressedFlag;
BOOLEAN KeyLongPressFlag;


/* UN Version Key Pad
----------------------------------------------------------------
| 01 | 02 | 03 | 04 | 05 |            | 31 | 32 | 33 | 34 | / |
----------------------------------------------------------------
| 06 | 07 | 08 | 09 | 10 |            | 42 | 43 | 44 | 51 | 52 |
----------------------------------------------------------------
| 11 | 12 | 13 | 14 | 15 |            | 39 | 40 | 41 | 53 | 54 |
----------------------------------------------------------------
| 16 | 17 | 18 | 19 | 20 |            | 36 | 37 | 38 | 55 | 56 |
----------------------------------------------------------------
| 21 | 22 | 23 | 24 | 25 |            | 35 | 45 | 46 | 57 | 58 |
----------------------------------------------------------------
| 26 | 27 | 28 | 29 | 30 |            | 47 | 48 | 49 |   50   |
----------------------------------------------------------------
*/

void KeyDrvInit(void)
{
    RingBufInit(&KeyDrvRingBuf, &keyDrvData[0], KEY_DRV_BUF_SIZE);
    
    keyDrvValidCount = KEY_DRV_VALID_TIME / (KEY_DRV_OUT_PIN_NUM * KEY_DRV_PROC_TIME_INTERVAL);
    keyDrvLongValidCount = KEY_DRV_LONG_VALID_TIME / (KEY_DRV_OUT_PIN_NUM * KEY_DRV_PROC_TIME_INTERVAL);
}

void Key_Init(void)
{ 	
    KeyDrvInit();
    KeyResetPressedFlag();
}

void keyOut_Init()
{
    KEY_OUT_1_H;
    KEY_OUT_2_H;
    KEY_OUT_3_H;
    KEY_OUT_4_H;

}

void keyOutProc(INT16U H_L)
{
    keyOut_Init();

    if (H_L == 0xfffe) KEY_OUT_1_L;
    else if (H_L == 0xfffd) KEY_OUT_2_L;
    else if (H_L == 0xfffb) KEY_OUT_3_L;
    else if (H_L == 0xfff7) KEY_OUT_4_L;

}

const INT16U	keyOutScanTable[KEY_DRV_OUT_PIN_NUM] = {0xfffe, 0xfffd, 0xfffb, 0xfff7};

void KEY_Scan(void)//1 ms
{
    INT8U keyInByte;

    if (KEY_IN_1 == 0) keyInByte = 0x00;
    else if (KEY_IN_2==0) keyInByte = 0x04;
    else if (KEY_IN_3==0) keyInByte = 0x08;
    else if (KEY_IN_4==0) keyInByte = 0x0C;
    else keyInByte = 0xF0;

    if (keyInByte != 0xF0) //키가 눌린경우
    {
        keyRawData = keyInByte + KeyDrvConvTable[keyDrvIndex]; 
        //kevin : keyDrvIndex는 초기에 0이고 스캔할때마다 계속 늘어나지만 아래쪽에 keyDrvIndex %= KEY_DRV_OUT_PIN_NUM; 에 의해 3이 이 라인에 들어올 수 있는 최대치가 된다.
        //kevin : 그래서 KeyDrvConvTable의 KEY_PRESET_1,  KEY_PRESET_2,   KEY_PRESET_3,   KEY_PRESET_4 4가지만 사용 할 수 있다.
        
    }
    if(keyDrvIndex == (KEY_DRV_OUT_PIN_NUM - 1)) //kevin : keyDrvIndex가 초기값이 0인데 왜 4도 아니고 3일때까지 기다리지? 최소 3번은 들어와야하니 4ms 이후에 들어온거네? 체터링 방지용으로 아래 10번 더한다. (총40번)
    {
        //SerialTxBufPutChar(keyRawData);
        if (keyRawData == 0xFF) //kevin : 아무것도 안눌린 경우 
        {
            keyValidRawData = 0xFF;
            keyDrvPressCount = 0;
        }
        else //키가 눌린경우
        {
            if (keyValidRawData == keyRawData) //kevin : 계속해서 같은 키가 눌리고 있으면서 게속해서 같은 키가 눌리고 있으면 여기로 진입
            {
                keyDrvPressCount++; //kevin : 
                if (keyDrvPressCount == keyDrvValidCount) //keyDrvValidCount =10
                {
                    RingBufPutCharForKeyInterrupt(&KeyDrvRingBuf, keyRawData);
                }
            }
            else //키가 눌려있는 건 맞지만 다른키로 눌림이 바꼇을 경우
            {
                keyValidRawData = keyRawData; //kevin : 키가 눌린경우 keyRawData 값을 keyValidRawData에 넣음
                keyDrvPressCount = 0; //kevin : 얼마나 게속 눌리고 있는지 관찰 (초기화함)
            }
        }
        keyRawData = 0xFF;
    }

    keyDrvIndex++;
    keyDrvIndex %= KEY_DRV_OUT_PIN_NUM;
    keyOutProc(keyOutScanTable[keyDrvIndex]);
}




void KeyResetPressedFlag(void)
{
    keyPressedFlag = 0;
    KeyLongPressedFlag = 0;
    KeyLongPressFlag = 0;
}


INT8U KeyDrvSetLongKey(INT8U rawKey)
{  
	INT8U longKey;
     if ((rawKey <= KEY_NOT_PRESSED)||(KeyLongPressedFlag))
	{
			return KEY_NOT_PRESSED;
	}

    longKey = rawKey;

    if(!KeyLongPressedFlag)
    {
        if (keyDrvPressCount > keyDrvValidCount)
	    {
		    if (keyDrvPressCount >= keyDrvLongValidCount)
		    {
		    	KeyLongPressedFlag = ON;
                keyDrvPressCount =0;
                RingBufPutCharForKeyInterrupt(&KeyDrvRingBuf, rawKey);
		    }
            else
            {
                longKey = KEY_NOT_PRESSED;
		        RingBufRewindChar(&KeyDrvRingBuf);
                return longKey;
            }
        }
	}
 	return longKey;
}


BOOLEAN KeyCheck(void)
{  
    if (RingBufCheckBuf(&KeyDrvRingBuf))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


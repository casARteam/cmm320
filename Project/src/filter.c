/*****************************************************************************
 |*			
 |*  Copyright		:	(c) 2002 CAS
 |*  Filename		:	filter.c
 |*  Version			:	0.1
 |*  Programmer(s)	:	Yun Yeo Chul (YYC)
 |*  Created			:	2016/07/11
 |*  Modify              :       
 |*  Description		:	WC300 MOG System
 |*				
 ****************************************************************************/


#include <stdio.h>
#include <string.h>
#include <float.h>
#include "globals.h"
#include "commbuf.h"
#include "comm_app.h"
#include "common.h"
#include "RingBuf.h"
#include "timer.h"
#include "filter.h"
#include "wc_calfilter.h"

//#ifdef USE_COMPENSATION
FP64 Pk_1st = 1.0;
FP64 varP_1st = 0.001;
FP64 R_1st = 7.5;
FP64 Kk_1st = 1.0;
FP64 Xk_1st = 0;

FP64 Pk_2nd = 1.0;
FP64 varP_2nd = 0.0001;
FP64 R_2nd = 15;
FP64 Kk_2nd = 1;
FP64 Xk_2nd = 0;
   
   
   
MAF_STRUCT Maf1stOrder;

void FilterSettingInit(void)
{
    INT8U capaFlag = 0; // 컴파일을 위한 임시 활성화
    // eeprom에 저장된 capaflag를 꼭 읽어서 대응을 시켱함
    
    /* 간섭계 공진기가 (eeprom으로 )판단이되면 각 변수에 매칭시켜야하는 값
        1. kel filter 값
        2. moving average winsize값
        3. 각 g에 맞는 cal value값 
    */
        
        
     capaFlag = Eeprom_bread(SCALESELECTADD);
     
     capaFlag = TempCapaSetting;        // 임시 디파인으로 값을 셋팅
     
        
    switch(capaFlag)
    {
        case LOWCAPA:
            Pk_1st = LowCapa_Pk_1st;
            varP_1st = LowCapa_varP_1st;
            R_1st = LowCapa_R_1st;
            Kk_1st = LowCapa_Kk_1st;
            Xk_1st = LowCapa_Xk_1st;

            Pk_2nd = LowCapa_Pk_2nd;
            varP_2nd = LowCapa_varP_2nd;
            R_2nd = LowCapa_R_2nd;
            Kk_2nd = LowCapa_Kk_2nd;
            Xk_2nd = LowCapa_Xk_2nd;
            
            WC300_MAF_Init(&Maf1stOrder,LowMoving1stwinsize,MAF_1ST_WC_ORDER);
            break;
            
        case HIGHCAPA:
            Pk_1st = HighCapa_Pk_1st;
            varP_1st = HighCapa_varP_1st;
            R_1st = HighCapa_R_1st;
            Kk_1st = HighCapa_Kk_1st;
            Xk_1st = HighCapa_Xk_1st;

            Pk_2nd = HighCapa_Pk_2nd;
            varP_2nd = HighCapa_varP_2nd;
            R_2nd = HighCapa_R_2nd;
            Kk_2nd = HighCapa_Kk_2nd;
            Xk_2nd = HighCapa_Xk_2nd;
            
            WC300_MAF_Init(&Maf1stOrder,HighMoving1stwinsize,MAF_1ST_WC_ORDER);
            break;
            
        default:            // default시 highcapa : 간섭계 기준
            Pk_1st = HighCapa_Pk_1st;
            varP_1st = HighCapa_varP_1st;
            R_1st = HighCapa_R_1st;
            Kk_1st = HighCapa_Kk_1st;
            Xk_1st = HighCapa_Xk_1st;

            Pk_2nd = HighCapa_Pk_2nd;
            varP_2nd = HighCapa_varP_2nd;
            R_2nd = HighCapa_R_2nd;
            Kk_2nd = HighCapa_Kk_2nd;
            Xk_2nd = HighCapa_Xk_2nd;
            
            WC300_MAF_Init(&Maf1stOrder,HighMoving1stwinsize,MAF_1ST_WC_ORDER);
            break;
    }
}


INT32U MAF_buf_1st_order[MAF_BUF_MAX_SIZE];

static double Pk = 1.0;
static double varP = 0.1;
static double R = 7.5;
static double Kk = 1;
static double Xk = 0;

/*simply Kalman Filter*/
INT32U KalmanFilter(INT32U rawData)
{
	static INT8U initFlag = FALSE;
        
        
	if(!initFlag)
	{
		Xk = rawData;
		initFlag = TRUE;
	}
    
	Pk = Pk + varP;
    Kk = Pk / (Pk + R);
    Xk = (Kk * rawData) + (1 - Kk) * Xk;
    Pk = (1 - Kk) * Pk;
	
	return (INT32U)Xk;
}


FP64 WC300_KalmanFilter_1st(FP64 rawData)
{
	static INT8U initFlag = FALSE;
        
        
	if(!initFlag)
	{
		Xk_1st = rawData;
		initFlag = TRUE;
	}
    
	Pk_1st = Pk_1st + varP_1st;
    Kk_1st = Pk_1st / (Pk_1st + R_1st);
    Xk_1st = (Kk_1st * rawData) + (1 - Kk_1st) * Xk_1st;
    Pk_1st = (1 - Kk_1st) * Pk_1st;
	
	return Xk_1st;
}

FP64 WC300_KalmanFilter_2nd(FP64 rawData)
{
	static INT8U initFlag = FALSE;
	
	if(!initFlag)
	{
		Xk_2nd = rawData;
		initFlag = TRUE;
	}
    
	Pk_2nd = Pk_2nd + varP_2nd;
    Kk_2nd = Pk_2nd / (Pk_2nd + R_2nd);
    Xk_2nd = (Kk_2nd * rawData) + (1 - Kk_2nd) * Xk_2nd;
    Pk_2nd = (1 - Kk_2nd) * Pk_2nd;
	
	return Xk_2nd;
}

void WC300_MAF_Init(MAF_STRUCT *MAFBuf, INT32U windowSize,INT8U order)
{
	(*MAFBuf).index = 0;
	(*MAFBuf).winSize = windowSize;
	(*MAFBuf).mafOnFlag = OFF;
	switch(order)
	{
		case MAF_1ST_ORDER:
			memset(MAF_buf_1st_order,0,sizeof(MAF_buf_1st_order));
			(*MAFBuf).buf = MAF_buf_1st_order;
			break;
//		case MAF_2ND_ORDER:
//			memset(MAF_buf_2nd_order,0,sizeof(MAF_buf_2nd_order));
//			(*MAFBuf).buf = MAF_buf_2nd_order;
//			break;
//		case MAF_3RD_ORDER:
//			memset(MAF_buf_3rd_order,0,sizeof(MAF_buf_3rd_order));
//			(*MAFBuf).buf = MAF_buf_3rd_order;
//			break;
//		case MAF_4TH_ORDER:
//			memset(MAF_buf_4th_order,0,sizeof(MAF_buf_4th_order));
//			(*MAFBuf).buf = MAF_buf_4th_order;
//			break;
//		case MAF_5TH_ORDER:
//			memset(MAF_buf_5th_order,0,sizeof(MAF_buf_5th_order));
//			(*MAFBuf).buf = MAF_buf_5th_order;
//			break;
//		case MAF_6TH_ORDER:
//			memset(MAF_buf_6th_order,0,sizeof(MAF_buf_6th_order));
//			(*MAFBuf).buf = MAF_buf_6th_order;
//			break;
//		case MAF_CREEP_ORDER:
//			memset(MAF_buf_Creep_order,0,sizeof(MAF_buf_Creep_order));
//			(*MAFBuf).buf = MAF_buf_Creep_order;
//			break;
//		case MAF_CREEPTRACKING_ORDER:
//			memset(MAF_buf_CreepTracking_order,0,sizeof(MAF_buf_CreepTracking_order));
//			(*MAFBuf).buf = MAF_buf_CreepTracking_order;
//			break;
//        case MAF_7TH_ORDER:
//			memset(MAF_buf_7th_order,0,sizeof(MAF_buf_7th_order));
//			(*MAFBuf).buf = MAF_buf_7th_order;
//			break;
//        case MAF_DCTRACKING_ORDER:
//            memset(MAF_buf_DCTracking_order,0,sizeof(MAF_buf_DCTracking_order));
//			(*MAFBuf).buf = MAF_buf_DCTracking_order;
//			break;
	}
}

INT32U WC300_MovingAVG(MAF_STRUCT *MAFBuf, INT32U inData)
{
    
	INT16U i;
	INT32U mafFilteredData = 0;
	FP64 sumData = 0;	
	INT32U bufIndex;
	INT32U currIndex;
	INT32U mafWinSize;
    
	
	currIndex = (*MAFBuf).index;
	mafWinSize = (*MAFBuf).winSize;
	
	if((*MAFBuf).mafOnFlag == OFF)
	{
		if (!inData) return inData;
		if (currIndex == mafWinSize) (*MAFBuf).mafOnFlag = ON;
		(*MAFBuf).buf[currIndex] = inData;
		(*MAFBuf).index++;
		if ((*MAFBuf).mafOnFlag != ON)
		{
			return 0;
		}
		else
		{
			return inData;
		}
	}
    
	(*MAFBuf).buf[currIndex] = inData;
	
	sumData = 0;
	
	for (i = 0; i < mafWinSize; i++)
	{
		bufIndex = (currIndex + MAF_BUF_MAX_SIZE - i) % MAF_BUF_MAX_SIZE;
		sumData = sumData + (*MAFBuf).buf[bufIndex];
	}
	mafFilteredData = (INT32U)(sumData / mafWinSize);
    
	(*MAFBuf).index++;
	(*MAFBuf).index = (*MAFBuf).index % MAF_BUF_MAX_SIZE;
    
	return mafFilteredData;	
    
}

INT32U DC_Tracking_Filter(INT32U inData)
{
    INT32U calcBZero = 0;
    INT32U calcAOne = 0;
    static INT32U DctFilterOut = 0;
	calcBZero = B_ZERO * inData;
    calcAOne = A_ONE * DctFilterOut;
    DctFilterOut = (calcBZero + calcAOne)/Q_FIFTEEN;
	
//	return ((DctFilterOut-10000) + 11000000);
    return DctFilterOut;
}
#define SPARK_BUFFER_NUM	5
static INT32S	SparkRejectBuf[SPARK_BUFFER_NUM];	
INT32U sparkRejectFilter(INT32U inData)
{
	
   	INT16U i, j;
	INT32U list[SPARK_BUFFER_NUM], temp;	
	
	static INT8U SparkBufferCnt = 0;	
		
	SparkRejectBuf[SparkBufferCnt] = inData;
	SparkBufferCnt++;
	if(SparkBufferCnt >= SPARK_BUFFER_NUM)
		SparkBufferCnt = 0;
	
	for(i= 0; i<SPARK_BUFFER_NUM; i++)
	{
		list[i] = SparkRejectBuf[i];
	}

	for (i=0; i<SPARK_BUFFER_NUM-1; i++)
	{
		for (j=i+1; j<SPARK_BUFFER_NUM; j++)
			if ( list[i] >= list[j] )
			{
				temp=list[i];
				list[i] = list[j];
				list[j] = temp;
			}
	}
	return list[2];    // 2= SPARK_BUFFER_NUM 의 중간값
}



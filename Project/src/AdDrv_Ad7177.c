/**
********************************************************************************
* Copyright (c) 2016 CAS
* @brief   AD Driver Source File\n
           for middle class indicator
           Chip : AD7177
* @file    AdDrvAD7710.c
* @version 1.0
* @date    2016/03/10
* @author  Yeo Chul Yun (YYC)
********************************************************************************
* @remark  [Version History]\n
*          v1.0 2016/3/10 created by YYC \n
********************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "globals.h"
#include "AdDrv.h"
#include "ringbuf.h"
#include "timer.h"
#include "key.h"


#ifdef AD_DRV_AD7177    // globas.h 에서 이 정의를 이용하게 되어 아래를 수행하기 시작한다.
/*
********************************************************************************
*                       INCLUDES
********************************************************************************
*/
/* Insert include files here */

/*
********************************************************************************
*                       GLOBAL(EXPORTED) VARIABLES & TABLES
********************************************************************************
*/
/* Insert global variables & tables here */

#define AD_BUF_SIZE		200

INT32U adDrvData[AD_BUF_SIZE];

/*
********************************************************************************
*                       LOCAL DEFINITIONS & MACROS
********************************************************************************
*/

/* AD7177-2 Register Map */
#define AD7177_COMM_REG       0x00
#define AD7177_STATUS_REG     0x00
#define AD7177_ADCMODE_REG    0x01
#define AD7177_IFMODE_REG     0x02
#define AD7177_REGCHECK_REG   0x03
#define AD7177_DATA_REG       0x04
#define AD7177_GPIOCON_REG    0x06
#define AD7177_ID_REG         0x07
#define AD7177_CHMAP0_REG     0x10
#define AD7177_CHMAP1_REG     0x11
#define AD7177_CHMAP2_REG     0x12
#define AD7177_CHMAP3_REG     0x13
#define AD7177_CHMAP4_REG     0x14
#define AD7177_CHMAP5_REG     0x15
#define AD7177_CHMAP6_REG     0x16
#define AD7177_CHMAP7_REG     0x17
#define AD7177_CHMAP8_REG     0x18
#define AD7177_CHMAP9_REG     0x19
#define AD7177_CHMAP10_REG    0x1A
#define AD7177_CHMAP11_REG    0x1B
#define AD7177_CHMAP12_REG    0x1C
#define AD7177_CHMAP13_REG    0x1D
#define AD7177_CHMAP14_REG    0x1E
#define AD7177_CHMAP15_REG    0x1F
#define AD7177_SETUPCON0_REG  0x20
#define AD7177_SETUPCON1_REG  0x21
#define AD7177_SETUPCON2_REG  0x22
#define AD7177_SETUPCON3_REG  0x23
#define AD7177_SETUPCON4_REG  0x24
#define AD7177_SETUPCON5_REG  0x25
#define AD7177_SETUPCON6_REG  0x26
#define AD7177_SETUPCON7_REG  0x27
#define AD7177_FILTCON0_REG   0x28
#define AD7177_FILTCON1_REG   0x29
#define AD7177_FILTCON2_REG   0x2A
#define AD7177_FILTCON3_REG   0x2B
#define AD7177_FILTCON4_REG   0x2C
#define AD7177_FILTCON5_REG   0x2D
#define AD7177_FILTCON6_REG   0x2E
#define AD7177_FILTCON7_REG   0x2F
#define AD7177_OFFSET0_REG    0x30
#define AD7177_OFFSET1_REG    0x31
#define AD7177_OFFSET2_REG    0x32
#define AD7177_OFFSET3_REG    0x33
#define AD7177_OFFSET4_REG    0x34
#define AD7177_OFFSET5_REG    0x35
#define AD7177_OFFSET6_REG    0x36
#define AD7177_OFFSET7_REG    0x37
#define AD7177_GAIN0_REG      0x38
#define AD7177_GAIN1_REG      0x39
#define AD7177_GAIN2_REG      0x3A
#define AD7177_GAIN3_REG      0x3B
#define AD7177_GAIN4_REG      0x3C
#define AD7177_GAIN5_REG      0x3D
#define AD7177_GAIN6_REG      0x3E
#define AD7177_GAIN7_REG      0x3F

/* Communication Register bits */
#define AD7177_COMM_REG_WEN    (0 << 7)
#define AD7177_COMM_REG_WR     (0 << 6)
#define AD7177_COMM_REG_RD     (1 << 6)
#define AD7177_COMM_REG_RA(x)  ((x) & 0x3F) //kevin? : 여기서 x 는 어디서 정의될까?

/* Status Register bits */
#define AD7177_STATUS_REG_RDY      (1 << 7)
#define AD7177_STATUS_REG_ADC_ERR  (1 << 6)
#define AD7177_STATUS_REG_CRC_ERR  (1 << 5)
#define AD7177_STATUS_REG_REG_ERR  (1 << 4)
#define AD7177_STATUS_REG_CH(x)    ((x) & 0x03)

/* ADC Mode Register bits */
#define AD7177_ADCMODE_REG_REF_EN     (1 << 15)
#define AD7177_ADCMODE_SING_CYC       (1 << 13)
#define AD7177_ADCMODE_REG_DELAY(x)   (((x) & 0x7) << 8)
#define AD7177_ADCMODE_REG_MODE(x)    (((x) & 0x7) << 4)
#define AD7177_ADCMODE_REG_CLKSEL(x) (((x) & 0x3) << 2)


/* Interface Mode Register bits */                                                                     
#define AD7177_IFMODE_REG_ALT_SYNC      (1 << 12)
#define AD7177_IFMODE_REG_IOSTRENGTH    (1 << 11)
#define AD7177_IFMODE_REG_HIDE_DELAY    (1 << 10)
#define AD7177_IFMODE_REG_DOUT_RESET    (1 << 8)
#define AD7177_IFMODE_REG_CONT_READ     (1 << 7)
#define AD7177_IFMODE_REG_DATA_STAT     (1 << 6)
#define AD7177_IFMODE_REG_REG_CHECK     (1 << 5)
#define AD7177_IFMODE_REG_XOR_EN        (0x01 << 2)
#define AD7177_IFMODE_REG_CRC_EN        (0x02 << 2)
#define AD7177_IFMODE_REG_XOR_STAT(x)   (((x) & AD7177_IFMODE_REG_XOR_EN) == AD7177_IFMODE_REG_XOR_EN)
#define AD7177_IFMODE_REG_CRC_STAT(x)   (((x) & AD7177_IFMODE_REG_CRC_EN) == AD7177_IFMODE_REG_CRC_EN)
#define AD7177_IFMODE_REG_DATA_WL32     (1 << 1)

/* GPIO Configuration Register bits */                                                                 
#define AD7177_GPIOCON_REG_MUX_IO      (1 << 12)
#define AD7177_GPIOCON_REG_SYNC_EN     (1 << 11)
#define AD7177_GPIOCON_REG_ERR_EN(x)   (((x) & 0x3) << 9)
#define AD7177_GPIOCON_REG_ERR_DAT     (1 << 8)
#define AD7177_GPIOCON_REG_IP_EN1      (1 << 5)
#define AD7177_GPIOCON_REG_IP_EN0      (1 << 4)
#define AD7177_GPIOCON_REG_OP_EN1      (1 << 3)
#define AD7177_GPIOCON_REG_OP_EN0      (1 << 2)
#define AD7177_GPIOCON_REG_DATA1       (1 << 1)
#define AD7177_GPIOCON_REG_DATA0       (1 << 0)

/* Channel Map Register 0-3 bits */
#define AD7177_CHMAP_REG_CH_EN        (1 << 15)
#define AD7177_CHMAP_REG_CH_DIS        (0 << 15)
#define AD7177_CHMAP_REG_SETUP_SEL(x)  (((x) & 0x3) << 12)
#define AD7177_CHMAP_REG_AINPOS(x)     (((x) & 0x1F) << 5)
#define AD7177_CHMAP_REG_AINNEG(x)     (((x) & 0x1F) << 0)

/* Setup Configuration Register 0-3 bits */
#define AD7177_SETUP_CONF_REG_BI_UNIPOLAR  (1 << 12)
#define AD7177_SETUP_CONF_REG_REF_SEL(x)   (((x) & 0x3) << 4)

/* Setup Configuration Register additional bits for AD7172-2, AD7172-4, AD7175-2 */
#define AD7177_SETUP_CONF_REG_REFBUF_P    (1 << 11)
#define AD7177_SETUP_CONF_REG_REFBUF_N    (1 << 10)
#define AD7177_SETUP_CONF_REG_AINBUF_P    (1 << 9)
#define AD7177_SETUP_CONF_REG_AINBUF_N    (1 << 8)

/* Filter Configuration Register 0-3 bits */
#define AD7177_FILT_CONF_REG_SINC3_MAP    (1 << 15)
#define AD7177_FILT_CONF_REG_ENHFILTEN    (1 << 11)
#define AD7177_FILT_CONF_REG_ENHFILT(x)   (((x) & 0x7) << 8)
#define AD7177_FILT_CONF_REG_ORDER(x)     (((x) & 0x3) << 5)
#define AD7177_FILT_CONF_REG_ODR(x)       (((x) & 0x1F) << 0)

/* Gain Register */
#define AD7177_GAIN_REG_VALUE       0x555555 //Nominal value of ADC gain register is 0x555555 

/* Offset Register */
#define AD7177_OFFSET_REG_VALUE       0x800000 //Nominal value of ADC offset register is 0x800000



/* AD7177 Constants */
#define AD7177_CRC8_POLYNOMIAL_REPRESENTATION 0x07 /* x8 + x2 + x + 1 */



/* AD Register Setting Value */
#define AD_MODE_REG_DEFALUT_VALUE		0x00000000	
#define AD_MODE_REG_INT_CLK_VALUE		0x00080000
#define AD_MODE_REG_EXT_CLK_VALUE		0x00000000
#define AD_MODE_REG_INTERNAL_ZERO_CAL	0x00800000
#define AD_MODE_REG_INTERNAL_SPAN_CAL	0x00A00000
#define AD_MODE_REG_SYSTEM_ZERO_CAL		0x00C00000
#define AD_MODE_REG_SYSTEM_SPAN_CAL		0x00E00000

#define AD_CONFIG_REG_DEFALUT_VALUE		0x00000110//0x000001D0	
#define AD_CONFIG_UNIPOLAR_BIT			0x00000008
#define AD_CONFIG_CHOP_BIT				0x00800000

#define AD_MODE_REG_SYNC_FILTER_BIT		0x00008000


#define AD_CONINUOUS_READ_CMD			0x5C
#define AD7177_READ_BIT					0x40
#define AD7177_READ_DATA				0x44
#define AD7177_READ_OFFSET0				0x70
#define AD7177_READ_GAIN0				0x78


#define AD7177_SIZE_8BIT				8
#define AD7177_SIZE_24BIT				24
#define AD7177_SIZE_32BIT				32
#define READ_MASK						0x40

/*
********************************************************************************
*                       LOCAL DATA TYPES & STRUCTURES
********************************************************************************
*/
/* Insert local typedef & structure here */
/** @brief Loadcell AD 값 Err를 확인하는 Flag */
static INT8U	adDrvlcErrFlag;
/** @brief Loadcell AD 값 Err시의 Counter */
static BOOLEAN lcErrCnt;

/*
********************************************************************************
*                       GLOBAL(FILE SCOPE) VARIABLES & TABLES
********************************************************************************
*/
/* Insert include files here */
/** @brief Loadcell AD 값을 넣을 Ring Buffer */
RING_BUF AdDrvRingBuf;
RING_BUF AdDrvWeightRingBuf; //kevin20180627 ADC CH0 데이터를 읽어오기위한 링버퍼 추가


/** @brief AD 값을 넣을 Ring Buffer */
RING_BUF *adRingBufPtr;
RING_BUF *adWeightRingBufPtr; //kevin20180627 ADC CH0 데이터를 읽어오기위한 링버퍼 추가

/*
********************************************************************************
*                       LOCAL FUNCTION PROTOTYPES
********************************************************************************
*/
void ad7177SetRegData(INT8U reg, INT32U data, INT8U size);
INT32U ad7177ReadRegData(INT8U reg, INT8U size);
void ad7177WriteByte(INT8U data);
void AdReset(void);
INT32U ad7177ContConvReadAdData(void);
INT32U ad7177ContConvReadAdStatusRsgister(void);
INT32U ad7177ReadAdData(void);
/*
********************************************************************************
*                       GLOBAL(EXPORTED) FUNCTIONS
********************************************************************************
*/
/* Insert global functions here */

/**
********************************************************************************
* @brief    AD Driver Init 함수\n
*           AD Ring Buffer Init\n
*           AD Chip Init
* @param    none
* @return   none
* @remark   초기에 실행 되는 함수
********************************************************************************
*/
void AdDrvInit(void)
{
    INT32U writeBuf;
    INT32U readBuf;
    INT8U errFlag = 0;
	
    RingBufInit(&AdDrvRingBuf, (char *)(&adDrvData[0]), AD_BUF_SIZE); //kevin : 링버퍼 설정
    RingBufInit(&AdDrvWeightRingBuf, (char *)(&adDrvData[0]), AD_BUF_SIZE); //kevin20180627 ADC CH0 데이터를 읽어오기위한 링버퍼 추가
    adRingBufPtr = &AdDrvRingBuf;
    adWeightRingBufPtr = &AdDrvWeightRingBuf; //kevin20180627 ADC CH0 데이터를 읽어오기위한 링버퍼 추가
    
    
    AdReset(); //kevin : ADC초기 리셋 (모두 초기화 하고 ADC 컨버전 준비 마침)
	
    AD_7177_CLK_HIGH;
    AD_7177_DATA_IN_HIGH;
    
    ////////////////SET ADC MODE//////////////////////////////////////
    /*
    Clock Select : External Crystal
    MODE Select : Continuous conversion mode
    Delay Convertion : None
    Single Cycle Mode : Disable
    Hide Delay : Disable
    Internal Reference : Disable
    */
    //////////////////////////////////////////////////////////////////

    writeBuf = 0;
    //writeBuf = AD7177_ADCMODE_REG_CLKSEL(3) | AD7177_ADCMODE_REG_MODE(0) | AD7177_ADCMODE_REG_DELAY(0); //kevin20180615 다채널 ADC 를 위해 컨버젼모드 변경
    writeBuf = 0x400C;
    
    
    //writeBuf = AD7177_ADCMODE_REG_CLKSEL(3) | AD7177_ADCMODE_REG_MODE(0) | AD7177_ADCMODE_REG_DELAY(0); //external clk
    //writeBuf = AD7177_ADCMODE_REG_CLKSEL(3) | AD7177_ADCMODE_REG_MODE(0) | AD7177_ADCMODE_REG_DELAY(0) | AD7177_ADCMODE_REG_REF_EN; //external clk, ref enable
    //writeBuf = AD7177_ADCMODE_REG_CLKSEL(3) | AD7177_ADCMODE_REG_MODE(0) | AD7177_ADCMODE_REG_DELAY(111); //external clk, delay 1ms
    //writeBuf = AD7177_ADCMODE_REG_CLKSEL(0) | AD7177_ADCMODE_REG_MODE(0) | AD7177_ADCMODE_REG_DELAY(0); //internal clk
    ad7177SetRegData(AD7177_ADCMODE_REG, writeBuf,2);
    
    readBuf = ad7177ReadRegData(AD7177_ADCMODE_REG, 2);
    if(writeBuf != readBuf)
    {
        errFlag = 1;
    }

        
    ////////////////SET ADC MODE//////////////////////////////////////
    /*
    Word Length : 32-bit Data
    CRC Mode : Disable
    Register Check : Disable
    Data + Status Output : Disable
    Continuous Read : Disable
    DOUT Pin Reset : Disable
    I/O Strength : Disable
    Alternative SYNC : Disable
    */
    //////////////////////////////////////////////////////////////////
    writeBuf = 0;
    writeBuf = 0x0002;
    //writeBuf = AD7177_IFMODE_REG_DATA_STAT | AD7177_IFMODE_REG_DATA_WL32;
    ad7177SetRegData(AD7177_IFMODE_REG, writeBuf,2);
    readBuf = ad7177ReadRegData(AD7177_IFMODE_REG,2);
    if(writeBuf != readBuf)
    {
        errFlag = 1;
    }
    
    ////////////////GPIO CONTROL//////////////////////////////////////
    /*
    GPIO 0 Data : 0
    GPIO 1 Data : 0
    GPIO 0 Output : Disable
    GPIO 1 Output : Disable
    GPIO 0 Input Enable : Disable
    GPIO 1 Input Enable : Disable
    Error Pin Data : 0
    Error Pin Mode: Disable
    SYNC Input Enable : Enable
    GPIO Mux : Disable
    */
    //////////////////////////////////////////////////////////////////
    writeBuf = 0;
    writeBuf = 0x0800;

    ad7177SetRegData(AD7177_GPIOCON_REG, writeBuf,2);
    readBuf = ad7177ReadRegData(AD7177_GPIOCON_REG,2);
    if(writeBuf != readBuf)
    {
        errFlag = 1;
    }
    
    ////////////////SET SETUP CONTROL //////////////////////////////////////
    /*
    Reference Select : External Reference
    Burnout Enable : Disable
    AIN- Buffer : Enable
    AIN+ Buffer : Enable
    REF- Buffer : Disable
    REF+ Buffer : Disable
    Bipolar/Unipolar : Unipolar coded
    */
    //////////////////////////////////////////////////////////////////
    writeBuf = 0;
    writeBuf = 0x1f00;
    //writeBuf = AD7177_SETUP_CONF_REG_REF_SEL(0) |AD7177_SETUP_CONF_REG_AINBUF_P | AD7177_SETUP_CONF_REG_AINBUF_N ;

    ad7177SetRegData(AD7177_SETUPCON0_REG, writeBuf,2);
    readBuf = ad7177ReadRegData(AD7177_SETUPCON0_REG,2);
    if(writeBuf != readBuf)
    {
        errFlag = 1;
    }
    
    
    ////////////////SET CHANNEL //////////////////////////////////////
    /*
    CH0
    AIN- Select : Ain 3
    AIN+ Select : Ain 0
    Setup : Setup 0
    Channel Enable : Enable
    
    CH1
    AIN- Select : Ain 3
    AIN+ Select : Ain 2
    Setup : Setup 0
    Channel Enable : Disable
    
    CH2
    AIN- Select : -
    AIN+ Select : -
    Setup : Setup 0
    Channel Enable : Disable
    
    CH3
    AIN- Select : -
    AIN+ Select : -
    Setup : Setup 0
    Channel Enable : Disable
    
    */
    //////////////////////////////////////////////////////////////////
    
    
    /* /kevin20180716 :기존에 잘하고 있던 2ch 사용 프로그램 
    writeBuf = 0;
    writeBuf = AD7177_CHMAP_REG_CH_EN | AD7177_CHMAP_REG_SETUP_SEL(0) | AD7177_CHMAP_REG_AINPOS(0) | AD7177_CHMAP_REG_AINNEG(4);
    //writeBuf = AD7177_CHMAP_REG_CH_EN | AD7177_CHMAP_REG_SETUP_SEL(0) | AD7177_CHMAP_REG_AINPOS(0) | AD7177_CHMAP_REG_AINNEG(22);//-Vref
    //writeBuf = AD7177_CHMAP_REG_CH_EN | AD7177_CHMAP_REG_SETUP_SEL(0) | AD7177_CHMAP_REG_AINPOS(0) | AD7177_CHMAP_REG_AINNEG(0);//
    ad7177SetRegData(AD7177_CHMAP0_REG, writeBuf,2);
    readBuf = ad7177ReadRegData(AD7177_CHMAP0_REG,2);
    if(writeBuf != readBuf)
    {
        errFlag = 1;
    }
    
    writeBuf = 0;
    writeBuf = AD7177_CHMAP_REG_CH_EN | AD7177_CHMAP_REG_SETUP_SEL(0) | AD7177_CHMAP_REG_AINPOS(2) | AD7177_CHMAP_REG_AINNEG(3);
    //kevin20180614 : ADC 채널 한개 더 활성화
    ad7177SetRegData(AD7177_CHMAP1_REG, writeBuf,2);
    readBuf = ad7177ReadRegData(AD7177_CHMAP1_REG,2);
    if(writeBuf != readBuf)
    {
        errFlag = 1;
    }
    */

    //kevin20180716 : CH1 만 활성화 시키고 고속으로 스캔 준비
    writeBuf = 0;
    writeBuf = 0x8076;
    ad7177SetRegData(AD7177_CHMAP0_REG, writeBuf,2);
    readBuf = ad7177ReadRegData(AD7177_CHMAP0_REG,2);
    if(writeBuf != readBuf)
    {
        errFlag = 1;
    }

    
    writeBuf = 0;
    ad7177SetRegData(AD7177_CHMAP2_REG, writeBuf,2);
    readBuf = ad7177ReadRegData(AD7177_CHMAP2_REG,2);
    if(writeBuf != readBuf)
    {
        errFlag = 1;
    }
    
    writeBuf = 0;
    ad7177SetRegData(AD7177_CHMAP3_REG, writeBuf,2);
    readBuf = ad7177ReadRegData(AD7177_CHMAP3_REG,2);
    if(writeBuf != readBuf)
    {
        errFlag = 1;
    }
    



    ////////////////SET FILTER CONTROL //////////////////////////////////////
    /*
    Output Data Rate  : 20 sps
    Filter Type       : Sinc5+Sinc1
    Enhanced Filter Select : Reject = 50&60 Hz settle 50ms ODR = 20 Hz
    Enhanced Filter : Enable
    Sinc3 Map Enable : Disable
    */
    //////////////////////////////////////////////////////////////////
    writeBuf = 0;
    //writeBuf = 0xE1A8; //kevin : 10SPS
    writeBuf = AD7177_FILT_CONF_REG_SINC3_MAP|(8000000/(32*MAFS_SRATE)); 
    
    //writeBuf = AD7177_FILT_CONF_REG_ODR(0x14)|AD7177_FILT_CONF_REG_ENHFILT(0x05)|
    //                  AD7177_FILT_CONF_REG_ORDER(0)|AD7177_FILT_CONF_REG_SINC3_MAP;
    //10 sos, 
    //writeBuf = AD7177_FILT_CONF_REG_ODR(0x14)|AD7177_FILT_CONF_REG_ENHFILTEN|AD7177_FILT_CONF_REG_ENHFILT(0x05)|
    //                  AD7177_FILT_CONF_REG_ORDER(0);    
    
    ad7177SetRegData(AD7177_FILTCON0_REG, writeBuf,2);
    readBuf = ad7177ReadRegData(AD7177_FILTCON0_REG,2);
    if(writeBuf != readBuf)
    {
        errFlag = 1;
    }
    
    
    ////////////////SET GAIN 0 //////////////////////////////////////
    /*
    Gain Value  : 0x555555 //Nominal value of ADC gain register is 0x555555 
    */
    //////////////////////////////////////////////////////////////////
    /*
    writeBuf = 0;
    writeBuf = AD7177_GAIN_REG_VALUE;
   

    ad7177SetRegData(AD7177_SETUPCON0_REG, writeBuf,3);
    readBuf = ad7177ReadRegData(AD7177_GAIN0_REG,3);
    if(writeBuf != readBuf)
    {
        errFlag = 1;
    }
    */
	
    ////////////////SET OFFSET 0 //////////////////////////////////////
    /*
    OFFSET Value  : 0x800000 //Nominal value of ADC OFFSET register is 0x800000 
    OFFSET Value Range : 0.4 * Vref ~ 1.05 * Vref
    */
    //////////////////////////////////////////////////////////////////
    
    writeBuf = 0;
    writeBuf = AD7177_OFFSET_REG_VALUE;
    ad7177SetRegData(AD7177_OFFSET0_REG, writeBuf,3);
    readBuf = ad7177ReadRegData(AD7177_OFFSET0_REG,3);
    if(writeBuf != readBuf)
    {
        errFlag = 1;
    }
    
    
    
    while(!AD_7177_DOUT_DRDY); 
}


/**
********************************************************************************
* @brief    AD Driver Procedure\n
*           AD Chip에서 읽은 값을 Ring Buffer에 저장
* @param    none
* @return   none
* @remark   일정 시간에 한번씩 계속 실행 되는 함수\n
*           AD Chip의 Sampling 시간 보다는 빨리 실행 되어야 함
********************************************************************************
*/
void AdDrvProc(void)
{
    static INT32U adData;
    static INT32U adDataCh0;
    /* //kevin20180716 : 원소스
     if ( ad7177ContConvReadAdStatusRsgister() == 1)  // kevin20180627 : ADC CH1 포토다이오드 값
    {
        adData = ad7177ContConvReadAdData();
        RingBufPutInt32sForAdDrv(&AdDrvRingBuf, adData); //kevin : 링버퍼중 &AdDrvRingBuf 주소값에 에 adData값을 넣어준다.
        adData = 0;
    }
    
    //kevin20180627 ADC CH0 데이터를 읽어오기위한 링버퍼 추가
     if ( ad7177ContConvReadAdStatusRsgister() == 0) // kevin20180627 : ADC CH0 전자석 입력전압 (계량) 값
    {
        adDataCh0 = ad7177ContConvReadAdData();
        RingBufPutInt32sForAdDrv(&AdDrvWeightRingBuf, adDataCh0); //kevin : 링버퍼중 &AdDrvWeightRingBuf 주소값에 에 adDataCh0값을 넣어준다.
        adDataCh0 = 0;
    }
    */
    
     if ( ad7177ContConvReadAdStatusRsgister() == 0)  // kevin20180627 : ADC CH1 포토다이오드 값
    {
        adData = ad7177ContConvReadAdData();
        RingBufPutInt32sForAdDrv(&AdDrvRingBuf, adData); //kevin : 링버퍼중 &AdDrvRingBuf 주소값에 에 adData값을 넣어준다.
        adData = 0;
    }
    
}
        



/*****************************************************************************/
/*
********************************************************************************
*                       LOCAL FUNCTIONS
********************************************************************************
*/
/* Insert local functions here */
void AdReset(void)
{	
    INT8U i;
    INT16U start, delay;
	
    // AD_7177_DATA_IN_HIGH;	
    //Chip Enable			
    delay16Clock();
    delay16Clock();
    /////////////////////////// AD Converter Reset /////////////////////////////
    //AD7177 Reset is 64 Serial CLOCK with DIN HIGH YYC 2016.03.10
    ad7177WriteByte(0xFF); //kevin : 8번 클럭  (64번 클럭이 되면 ADC Din 에 Low to High.)
    ad7177WriteByte(0xFF); //kevin : 8번 클럭 추가
    ad7177WriteByte(0xFF); //kevin : 8번 클럭 추가
    ad7177WriteByte(0xFF); //kevin : 8번 클럭 추가
    ad7177WriteByte(0xFF); //kevin : 8번 클럭 추가
    ad7177WriteByte(0xFF); //kevin : 8번 클럭 추가
    ad7177WriteByte(0xFF); //kevin : 8번 클럭 추가
    ad7177WriteByte(0xFF); //kevin : 위에서부터 도합 64번 클럭완료, ADC Din 에 Low to High. 인가 / ADC IC Reset 완료

    TimerSetDelay10ms(TIMER_DELAY_10MS_DELAY,1);
    while(!TimerCheckDelay10ms(TIMER_DELAY_10MS_DELAY)); 
    // Allow at least 500 us before accessing any of the on-chip registers.
    
    ///////////////////////// System Synchronization////////////////////////////
    // When the SYNC_EN bit in the GPIOCON register is set to 1(Default = 0x1), the SYNC/ERROR pin functions as a synchronization input.
    //Sync의 경우 ADC 내부의 디지털 필터를 Reset하고 내부의 Modulator를 Reset한다.
    
    AD_7177_SYNC_LOW; 

    // AD_7177_CLK_HIGH;
    for(i=0;i<4;i++) //kevin : 클럭 32번
    {
        ad7177WriteByte(0xFF);
    }
    
    AD_7177_SYNC_HIGH; //kevin : start conversion command (Starting to Analog to Digital convertion )
    //kevin : After the conversion is complete, bring the SYNC input low in preparation for the next conversion start signal.
    delay16Clock();
    delay16Clock();
    
///////////////////////// System Synchronization////////////////////////////
}

/*
******************************************************************************
* @brief    ad Driver Setting 하는  함수 \n
* @param    none
* @return   none
* @remark
********************************************************************************
*/
void ad7177SetRegData(INT8U reg, INT32U data, INT8U size)
{
    INT8U i;
    INT8U j;
    INT8U wrBuf[8] = {0,0,0,0,0,0,0,0};
    INT32U regValue;
    /* Build the Command word */
    wrBuf[0] = AD7177_COMM_REG_WEN | AD7177_COMM_REG_WR | AD7177_COMM_REG_RA(reg);

	/* Fill the write buffer */
    regValue = data;
    for(i = 0; i < size; i++)
    {
        wrBuf[size - i] = regValue & 0xFF;
        regValue >>= 8;
    }

    // AD_7177_CLK_HIGH;
    delay16Clock();
    delay16Clock();
    delay16Clock();
    delay16Clock();
    
    for(j = 0; j < size+1; j++)
    {
        ad7177WriteByte(wrBuf[j]);
//        for (i = 0; i < 8 ;i++)										//Write data
//        {
//            AD_7177_CLK_LOW;
//            delay16Clock();
//            delay16Clock();
//            
//            if ((wrBuf[j] & 0x80) != 0)
//                AD_7177_DATA_IN_HIGH;
//            else
//                AD_7177_DATA_IN_LOW;
//            
//            delay16Clock();
//            delay16Clock();
//            AD_7177_CLK_HIGH;
//            delay16Clock();
//            delay16Clock();
//            delay16Clock();
//            delay16Clock();
//            wrBuf[j] <<= 1;
//        }
//        AD_7177_DATA_IN_HIGH;
    }
    delay16Clock();
    delay16Clock();
    delay16Clock();
    delay16Clock();
    
}
/*
******************************************************************************
* @brief    ad Driver Setting 하는  함수 \n
* @param    none
* @return   none
* @remark
********************************************************************************
*/
void ad7177WriteByte(INT8U data) //H,L,H,L..(80번) 하고나서 Din(PIN.12) 을 High 에서 low 로 변경
{
    INT8U i;
    
    AD_7177_CLK_HIGH;
    
    delay16Clock();
    delay16Clock();
    delay16Clock();
    delay16Clock();
    
    for(i=0; i<8 ;i++)	//Write data
    {
        AD_7177_CLK_LOW;
        
        delay16Clock();
        delay16Clock();
        
        if ((data & 0x80) != 0) 
        {
            AD_7177_DATA_IN_HIGH;
        }
        else
        {
            AD_7177_DATA_IN_LOW;
        }
        
        delay16Clock();
        delay16Clock();
		
        AD_7177_CLK_HIGH;
        delay16Clock();
        delay16Clock();
        delay16Clock();
        delay16Clock();
        data <<= 1; // data = data << 1;
    }
}
/*
******************************************************************************
* @brief    ad Driver Setting 하는  함수 \n
* @param    none
* @return   none
* @remark
********************************************************************************
*/
INT32U ad7177ReadRegData(INT8U reg, INT8U size)
{
    INT8U  i, j, data;
    INT32U  regData;
	
    data = AD7177_COMM_REG_WEN | AD7177_COMM_REG_RD | AD7177_COMM_REG_RA(reg);
    
    AD_7177_CLK_HIGH;
	
    delay16Clock();
    delay16Clock();
    delay16Clock();
    delay16Clock();
	
    for(i=0; i<8; i++)		
    {
        AD_7177_CLK_LOW;
        delay16Clock();
        delay16Clock();
        
        if (data & 0x80)
        {
            AD_7177_DATA_IN_HIGH;
        }
        else
        {
            AD_7177_DATA_IN_LOW;
        }
        delay16Clock();
        delay16Clock();
        
        AD_7177_CLK_HIGH;
        delay16Clock();
        delay16Clock();
        delay16Clock();
        delay16Clock();
        data <<= 1;
    }
    AD_7177_DATA_IN_HIGH;
    delay16Clock();
    delay16Clock();
    delay16Clock();
    delay16Clock();
	    
    regData = 0;
    
    for(i=0; i<size; i++)
    {
        for(j=0; j<8; j++)
        {
            regData <<= 1;		
            AD_7177_CLK_LOW;		
            delay16Clock();
            delay16Clock();
            delay16Clock();
            delay16Clock();

            AD_7177_CLK_HIGH;
            delay16Clock();
            delay16Clock();

            if(AD_7177_DOUT_DRDY)
                {
                    regData++;
                }
            delay16Clock();
            delay16Clock();
        }
    }
    
    AD_7177_DATA_IN_HIGH;
    delay16Clock();
    delay16Clock();
    delay16Clock();
    delay16Clock();
    
    return regData;
}
/*
******************************************************************************
* @brief    ad 컨버팅된 Data를 읽는 함수 \n
*
* @param    none
* @return   none
* @remark   연속적으로 AD 변환됨
********************************************************************************
*/
INT32U ad7177ReadAdData(void)
{
    INT8U  i;
    INT32S  adsData;
	
    adsData = 0x00000000;

    if(AD_7177_DOUT_DRDY)
    {
        return 0;
    }
	
    ad7177WriteByte(0x44);
    
    for(i=0; i<32; i++)
    {
        adsData <<= 1;		
        AD_7177_CLK_LOW;
        delay16Clock();
        delay16Clock();
  
        AD_7177_CLK_HIGH;
        delay16Clock();
        delay16Clock();
  
        if(AD_7177_DOUT_DRDY)
        {
            adsData++;
        }
        delay16Clock();
        delay16Clock();
    }
		
    return adsData;	
}




/*
******************************************************************************
* @brief    ad 컨버팅된 Data를 읽는 함수 \n
*
* @param    none
* @return   none
* @remark   연속적으로 AD 변환됨
********************************************************************************
*/
INT32U ad7177ContConvReadAdData(void)
{
    INT8U  i, j, data;
    INT32U  adData;
    
    data = AD7177_READ_DATA; //kevin : 0x44
    
//kevin : ADC에서 데이터 컨버팅이 마쳐지면 RDY 가 High 에서 Low로 바뀐다. AD_7177_DOUT_DRDY 는 이 값을 가지고 있는 변수
    if(AD_7177_DOUT_DRDY) 
    {
        return 0;
    }
	
    AD_7177_CLK_HIGH;
	
    delay16Clock();
    delay16Clock();
    delay16Clock();
    delay16Clock();
	
    for(i=0; i<8; i++) //kevin : data에 들어있는 8비트 신호를 DATA_IN 으로 내보낸다.(ADC 에 DATA_IN 신호가 전달된다.)
    {
        AD_7177_CLK_LOW;
        delay16Clock();
        delay16Clock();
        
        if (data & 0x80) //kevin : data 초기값은 0x44다, 따라서 0x44 & 0x80 = 0100 0100 & 1000 0000 = 0000 0000 여기로 처음에 진입 불가. 우번째는 data가 왼쪽 쉬프트 되면서 1000 1000 이된다. 
        {
            AD_7177_DATA_IN_HIGH; //실제 GPIO DATA IN 핀에 HIGH 신호 전달 (ADC 에서는 이 DIN 핀으로 0x44 신호가 들어오면 컨버젼된 신호를 DOUT으로 내보내 준다.)
        }
	else
        {
            AD_7177_DATA_IN_LOW;
        }
        
        delay16Clock();
        delay16Clock();
        
        AD_7177_CLK_HIGH;
        delay16Clock();
        delay16Clock();
        delay16Clock();
        delay16Clock();
        data <<= 1;
    }
    
    AD_7177_DATA_IN_HIGH;
    delay16Clock();
    delay16Clock();
	
    adData = 0;
    
    for(i=0; i<4; i++) //kevin : 컨버팅된 32비트 신호를  adDATA 에다가 차곡차곡 넣어준다.
    {
        for(j=0; j<8; j++)
        {
            adData <<= 1;
            AD_7177_CLK_LOW;		
            delay16Clock();
            delay16Clock();
            delay16Clock();
            delay16Clock();

            if(AD_7177_DOUT_DRDY) //kevin : DOUT 으로 들어온 데이터가 1일때만 adData 를 1 씩올려줌 
            {
                adData++;
            }
            
            AD_7177_CLK_HIGH;
            delay16Clock();
            delay16Clock();
            delay16Clock();
            delay16Clock();
        }
    }
    
    delay16Clock();
    delay16Clock();
    return adData;	// 최종적으로 32비트 신호가 adData를 통해 함수 밖으로 내보내 진다.
}




/*
******************************************************************************
//keinv20180615 : 여러채널의 ad값을 컨버젼해서 출력으로 내고있을때 무슨 채널에서 나온 출력인지 알아내는
********************************************************************************
*/
INT32U ad7177ContConvReadAdStatusRsgister(void)
{
//    INT8U  i;
//    INT8U data;
//    INT8U adStatusRegister;
//    
//    data= 0x40; //kevin20180615 : COMMUNICATIONS REGISTER > Status register
//    
////kevin : ADC에서 데이터 컨버팅이 마쳐지면 RDY 가 High 에서 Low로 바뀐다. AD_7177_DOUT_DRDY 는 이 값을 가지고 있는 변수
    
    
    if(AD_7177_DOUT_DRDY) 
    {
        return 0xFF; 
        //kevin20180628 : 원소스는 0 을 반환하는거지만, 0을 반환해버리면 ADStatusRegister 의 값 중 0은 
        //kevin20180628 : 채널0번이 데이터를 내보낼 준비가 되었다는 값이기때문에 에러가 있다는 뜻으로 0xFF 를 반환하게 변경
    }
    return 0;
    
	
//    AD_7177_CLK_HIGH;
//	
//    delay16Clock();
//    delay16Clock();
//    delay16Clock();
//    delay16Clock();
//    
//	
//    for(i=0; i<8; i++) //kevin : data에 들어있는 8비트 신호를 DATA_IN 으로 내보낸다.(ADC 에 DATA_IN 신호가 전달된다.)
//    {
//        AD_7177_CLK_LOW;
//        delay16Clock();
//        delay16Clock();
//        
//        
//        if (data & 0x80) //kevin : data 초기값은 0x44다, 따라서 0x44 & 0x80 = 0100 0100 & 1000 0000 = 0000 0000 여기로 처음에 진입 불가. 우번째는 data가 왼쪽 쉬프트 되면서 1000 1000 이된다. 
//        {
//            AD_7177_DATA_IN_HIGH; //실제 GPIO DATA IN 핀에 HIGH 신호 전달 (ADC 에서는 이 DIN 핀으로 0x44 신호가 들어오면 컨버젼된 신호를 DOUT으로 내보내 준다.)
//        }
//	else
//        {
//            AD_7177_DATA_IN_LOW;
//        }
//        
//        delay16Clock();
//        delay16Clock();
//        
//        AD_7177_CLK_HIGH;
//        delay16Clock();
//        delay16Clock();
//        delay16Clock();
//        delay16Clock();
//        data <<= 1;
//    }
//    
//    AD_7177_DATA_IN_HIGH;
//    delay16Clock();
//    delay16Clock();
//	
//    adStatusRegister = 0;
//    
//    
//    for(i=0; i<8; i++) //kevin : 컨버팅된 8비트 신호를  adDATA 에다가 차곡차곡 넣어준다.
//    {
//        adStatusRegister <<= 1;
//        AD_7177_CLK_LOW;		
//        delay16Clock();
//        delay16Clock();
//        delay16Clock();
//        delay16Clock();
//
//        if(AD_7177_DOUT_DRDY) //kevin : DOUT 으로 들어온 데이터가 1일때만 adData 를 1 씩올려줌 
//        {
//            adStatusRegister++;
//        }
//        AD_7177_CLK_HIGH;
//        delay16Clock();
//        delay16Clock();
//        delay16Clock();
//        delay16Clock();
//    }
//
//    delay16Clock();
//    delay16Clock();
//        
//    return adStatusRegister;	// 최종적으로 32비트 신호가 adData를 통해 함수 밖으로 내보내진다.
}




#endif

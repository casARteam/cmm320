///*****************************************************************************
/*
|*  Copyright       :   (c) 2011 CAS
|*  Filename        :   comm_app.c
|*  Version         :   0.1
|*  Programmer(s)   :   Yun Yeo-Chul (YYC)
|*  Created         :   2016/3/22
|*  Modified        :
|*  Description     :   WC300 Temperature Sensor communication function
*/
//****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "globals.h"
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include "commbuf.h"
#include "comm_app.h"



#define MAX_SIZE_MsgBuf	1024
char MsgBuf[MAX_SIZE_MsgBuf];
char MsgTempBuf[MAX_SIZE_MsgBuf];
INT8U CBuf_Type;

//
//header start

INT8U readSavedData(INT8U *buffer, INT16U *sLength);
INT8U writeReceivedData(INT8U *buffer, INT16U *sLength);
BOOLEAN findNextPacketHeader(COMM_BUF *CBuf, INT8U *data, INT16U length);


void Uart1SendData(void)
{
    CommBufUart1.Txing = 1;
//    sendData = GetCharTxBuf(&CommBufUart1);
//    USART1->DR = (sendData & (INT16U)0x01FF);
    USART_ITConfig(USART1,USART_IT_TXE,ENABLE);
}

void Uart3SendData(void)
{
    CommBufUart3.Txing = 1; 
//    sendData = GetCharTxBuf(&CommBufUart1);
//    USART1->DR = (sendData & (INT16U)0x01FF);
    USART_ITConfig(USART3,USART_IT_TXE,ENABLE);
}


void MsgOut(char *str)
{
	PutStrTxBuf(&CommBufUart1,str);
        PutStrTxBuf(&CommBufUart3,str); //kevin : uart3
	if(CheckTxBuf(&CommBufUart1)) 
	{
	        Uart1SendData();           
	}
        else if(CheckTxBuf(&CommBufUart3)) //kevin : uart3
	{
	        Uart3SendData();           
	}
}




void CommSendAdDATA(char *adData, char *tempData)
{
    INT8U packet[12];
    
    memset(packet,0x00,sizeof(packet)); //kevin : 메모리 셋팅(만들어진 공간에다 0x00 을 다 집어넣는 것)
    /*
    memset 은
    INT8U packet[12]={0,0,0,0,0,0,0,0,0,0,0,0} ;이렇게 표현하는것이라고 생각할 수 있다.
    */
    
    /*
    packet[0] = 0x01;//SOH - 헤딩의 시작을 나타냄
    packet[1] = 0x02;//STX - 헤딩의 종료 및 text의 개시를 나타냄
    packet[2] = (INT8U)((data&0xFF000000)>>24);
    packet[3] = (INT8U)((data&0x00FF0000)>>16);
    packet[4] = (INT8U)((data&0x0000FF00)>>8);
    packet[5] = (INT8U)(data&0x000000FF);
    packet[6] = 0x03;///ETX - text의  끝
    packet[7] = 0x04;//EOT - 전송의 끝 및 데이터 링크의 초기화
*/

    //packet[0] = 0x01;//SOH - 헤딩의 시작을 나타냄
    //packet[1] = 0x02;//STX - 헤딩의 종료 및 text의 개시를 나타냄
    packet[0] = 0x01;
    packet[1] = 0x02;
    packet[2] = adData[3];
    packet[3] = adData[2];
    packet[4] = adData[1];
    packet[5] = adData[0];
    packet[6] = tempData[3];
    packet[7] = tempData[2];
    packet[8] = tempData[1];
    packet[9] = tempData[0];
    packet[10] = 0x03;
    packet[11] = 0x04;
    //packet[10] = ','; // 이 비트는 원래 /null 이 들어가는 자리인데 그냥 무시하고 이거 넣어버림.
    //packet[11] = ' '; // 0x0d 다음에 0x0a 를 하면 하이퍼 터미널에서 엔터로 보이게된다.
    //packet[13] = 0x03;///ETX - text의  끝
    //packet[14] = 0x04;//EOT - 전송의 끝 및 데이터 링크의 초기화
    //packet[15] = 0x0a; //엔터?
    
    
    PutDataTxBuf(&CommBufUart1,packet,sizeof(packet));

}

                
        



//void SendAdRawDATA(INT32U data) //kevin : 원래 함수 정의
void SendAdRawDATA(char *data) //kevin_c : 변수형 변환한 함수 정의
{
    INT8U packet[12];
    
    memset(packet,0x00,sizeof(packet)); //kevin : 메모리 셋팅(만들어진 공간에다 0x00 을 다 집어넣는 것)
    /*
    memset 은
    INT8U packet[12]={0,0,0,0,0,0,0,0,0,0,0,0} ;이렇게 표현하는것이라고 생각할 수 있다.
    */
    
    /*
    packet[0] = 0x01;//SOH - 헤딩의 시작을 나타냄
    packet[1] = 0x02;//STX - 헤딩의 종료 및 text의 개시를 나타냄
    packet[2] = (INT8U)((data&0xFF000000)>>24);
    packet[3] = (INT8U)((data&0x00FF0000)>>16);
    packet[4] = (INT8U)((data&0x0000FF00)>>8);
    packet[5] = (INT8U)(data&0x000000FF);
    packet[6] = 0x03;///ETX - text의  끝
    packet[7] = 0x04;//EOT - 전송의 끝 및 데이터 링크의 초기화
*/

    //packet[0] = 0x01;//SOH - 헤딩의 시작을 나타냄
    //packet[1] = 0x02;//STX - 헤딩의 종료 및 text의 개시를 나타냄
    packet[0] = data[0];
    packet[1] = data[1];
    packet[2] = data[2];
    packet[3] = data[3];
    packet[4] = data[4];
    packet[5] = data[5];
    packet[6] = data[6];
    packet[7] = data[7];
    packet[8] = data[8];
    packet[9] = data[9];
    //packet[10] = ','; // 이 비트는 원래 /null 이 들어가는 자리인데 그냥 무시하고 이거 넣어버림.
    //packet[11] = ' '; // 0x0d 다음에 0x0a 를 하면 하이퍼 터미널에서 엔터로 보이게된다.
    //packet[13] = 0x03;///ETX - text의  끝
    //packet[14] = 0x04;//EOT - 전송의 끝 및 데이터 링크의 초기화
    //packet[15] = 0x0a; //엔터?
    
    
    PutDataTxBuf(&CommBufUart1,packet,sizeof(packet));

}

void CommSendData(char *data, INT8U size, INT8U end_flag)
{
    INT8U packet[3];
    
    PutDataTxBuf(&CommBufUart1, data, size);
    
    memset(packet,0x00,sizeof(packet)); //kevin : 메모리 셋팅(만들어진 공간에다 0x00 을 다 집어넣는 것)

    if (end_flag == 1)
    {
        packet[0] = 0x0d; 
        packet[1] = 0x0a; // 0x0d 다음에 0x0a 를 하면 하이퍼 터미널에서 엔터로 보이게된다.
        PutDataTxBuf(&CommBufUart1, packet, 2);
    }
    else
    {
        packet[0] = ','; // 0x0d 다음에 0x0a 를 하면 하이퍼 터미널에서 엔터로 보이게된다.
        PutDataTxBuf(&CommBufUart1, packet, 1);
    }
}

void CommSendData3(char *data, INT8U size, INT8U end_flag)
{
    INT8U packet[3];
    
    PutDataTxBuf(&CommBufUart3, data, size);
    
    memset(packet,0x00,sizeof(packet)); //kevin : 메모리 셋팅(만들어진 공간에다 0x00 을 다 집어넣는 것)

    if (end_flag == 1)
    {
        packet[0] = 0x0d; 
        packet[1] = 0x0a; // 0x0d 다음에 0x0a 를 하면 하이퍼 터미널에서 엔터로 보이게된다.
        PutDataTxBuf(&CommBufUart3, packet, 2);
    }
    else
    {
        packet[0] = ',';
        PutDataTxBuf(&CommBufUart3, packet, 1);
    }
}


void SendAdRawNFLDATA(INT32U rawData,INT32U flData)
{
    INT8U packet[12];
    
    memset(packet,0x00,sizeof(packet)); //kevin : 메모리 셋팅(만들어진 공간에다 0x00 을 다 집어넣는 것)
    /*
    memset 은
    INT8U packet[12]={0,0,0,0,0,0,0,0,0,0,0,0} ;이렇게 표현하는것이라고 생각할 수 있다.
    */
    
    packet[0] = 0x01;//SOH - 헤딩의 시작을 나타냄
    packet[1] = 0x02;//STX - 헤딩의 종료 및 text의 개시를 나타냄
    rawData;
    packet[2] = (INT8U)((rawData&0xFF000000)>>24);
    packet[3] = (INT8U)((rawData&0x00FF0000)>>16);
    packet[4] = (INT8U)((rawData&0x0000FF00)>>8);
    packet[5] = (INT8U)(rawData&0x000000FF);
    packet[6] = (INT8U)((flData&0xFF000000)>>24);
    packet[7] = (INT8U)((flData&0x00FF0000)>>16);
    packet[8] = (INT8U)((flData&0x0000FF00)>>8);
    packet[9] = (INT8U)(flData&0x000000FF);
    packet[6] = 0x03;//ETX - text의  끝
    packet[7] = 0x04;//EOT - 전송의 끝 및 데이터 링크의 초기화
        
    PutDataTxBuf(&CommBufUart1,packet,sizeof(packet));

}
#define STATUS_READY_0x65 0
#define STATUS_READY_0x0D 1

void ReadyToStart(COMM_BUF *CBuf)
{
    INT8U revChar;
    INT8U readyStatus = STATUS_READY_0x65;
    INT8U startFlag = OFF;
    
    while(!startFlag)
    {
        if (CheckRxBuf(CBuf))
        {
            revChar = GetCharRxBuf(CBuf);
            
            switch(readyStatus)
            {
                case STATUS_READY_0x65:
                    if(revChar == 0x65)
                    {
                        readyStatus = STATUS_READY_0x0D;
                    }
                    break;
                case STATUS_READY_0x0D:
                    if(revChar == 0x0D)
                    {
                        startFlag = ON;
                    }
                    else
                    {
                        readyStatus = STATUS_READY_0x65;
                    }
                    break;
            }
        }
    }
}
/** 
* @brief	패킷 구문 분석 함수
* @param	*CBuf    통신 데이터 구조체
* @param	trans    전송 여부
* @return  void
*/
void CommInterpreter(COMM_BUF *CBuf)
{
	PACKET_HEADER *header;
	PACKET_TAIL *tail;
	INT16U length;
	INT8U  checksum;
	INT16U i;
	INT8U errNo;
	INT16U rxSize;
	INT16U sendLength;
	INT16U check_end_pos;
	INT8U opCode;

	sendLength = 0;
    
	rxSize = GetRxBufSize(CBuf);
    
	GetDataRxBuf(CBuf, MsgBuf, rxSize);

	if ((rxSize) < sizeof(PACKET_HEADER))
	{
		return;
	}

	header = (PACKET_HEADER *)MsgBuf;
	opCode = header->opCode[1];
	length = header->length;

	if (header->colon != ':')
	{
		goto ERR_HANDLER;
	}
	else if (header->delilimeter != ',')
	{
		goto ERR_HANDLER;
	}

    else if (('W' != header->opCode[0]) && ('R' != header->opCode[0]) && 
             ('G' != header->opCode[0]) && ('N' != header->opCode[0]))
	{
		goto ERR_HANDLER;
	}

	if (length > COM_MAX_RX_BUF_HOST_SIZE)
	{
		goto ERR_HANDLER_CHECKSUM;
	}

	if (rxSize < length + sizeof(PACKET_HEADER) + sizeof(PACKET_TAIL))
	{
		return;
	}

	tail = (PACKET_TAIL *)&MsgBuf[length + sizeof(PACKET_HEADER)];

	if (tail->colon != ':')							// 패킷 tail 확인
	{

		goto ERR_HANDLER_CHECKSUM;
	}
	else if (tail->cr != ASC_CR)
	{
		goto ERR_HANDLER_CHECKSUM;
	}

	checksum = 0;

	i = 2;
	check_end_pos = length + sizeof(PACKET_HEADER);//data 마지막 까지
	while(i < check_end_pos)
	{
		checksum += MsgBuf[i++];
	}

	checksum += ':';//tail ':' 까지 checksum 계산

	if (tail->checksum != checksum)
	{
		goto ERR_HANDLER_CHECKSUM;
	}

	if (header->opCode[0] == 'W')
	{
		errNo = writeReceivedData((INT8U *)MsgBuf, &sendLength);
	}
	else if (header->opCode[0] == 'R')
	{
		errNo = readSavedData((INT8U *)MsgBuf, &sendLength);
	}
    else if (header->opCode[0] == 'G' || header->opCode[0] == 'N')
    {
        switch(header->opCode[1])
        {


        }

    }
	else
	{
		errNo = ERROR_INTERPRETER;
	}

    if (errNo == 0)
	{
		WindRxBuf(CBuf, length + sizeof(PACKET_HEADER) + sizeof(PACKET_TAIL));

        if (errNo == ERROR_RS485_ID)
        {

        }
        else
        {
            PutDataTxBuf(CBuf, MsgBuf, sendLength);
        }        
        return;
	}

SEND_ERR:
	MsgBuf[0]='N';
	MsgBuf[1]=opCode;
    MsgBuf[2]=1; //src
    MsgBuf[3]=0;
    MsgBuf[4]=0; //dest,
    MsgBuf[5]=0;
	MsgBuf[6]=',';
	MsgBuf[7]=5;
	MsgBuf[8]=0;
	MsgBuf[9]=':';
//  기존에 받은 번호 그대로 반환하거나
//	해석기에서 modify 했다고 가정함
//	MsgBuf[10]=0;
//	MsgBuf[11]=0;
//	MsgBuf[12]=0;
//	MsgBuf[13]=0;
	MsgBuf[14]=errNo;
	MsgBuf[15]=':';

	checksum = 0;

	for(i=2; i<16; i++) checksum += MsgBuf[i];

	MsgBuf[16]=checksum;
	MsgBuf[17]=ASC_CR;

	FlushRxBuf(CBuf);
	PutDataTxBuf(CBuf, MsgBuf, 18);
	return;

ERR_HANDLER:
	if (TRUE == findNextPacketHeader(CBuf, (INT8U *)MsgBuf, rxSize))	
	{
		return;	
	}
	else 
	{
ERR_HANDLER_CHECKSUM:
		errNo = ERROR_RECEIVE;
		goto SEND_ERR;
	}
}

/**
*  @brief		패킷 데이터에 잘못된 데이터가 들어 왔을 때 다음 패킷의 헤더를 찾는다. 
*  @param		*CBuf	통신 버퍼 구조체
*  @param		*data	패킷 데이터
*  @param		length	들어온 패킷의 길이
*  @return		다음 헤더 찾기 성공 여부
*/
BOOLEAN findNextPacketHeader(COMM_BUF *CBuf, INT8U *data, INT16U length)
{
	INT16U i;
	PACKET_HEADER *header;

	for (i = 0; i <= length - sizeof(PACKET_HEADER); i++)
	{
		header = (PACKET_HEADER *)(data + i);

		if (':' == header->colon) {
			if (',' == header->delilimeter) {
				if (('W' == header->opCode[0]) || ('R' == header->opCode[0]) || ('G' == header->opCode[0])) {
					WindRxBuf(CBuf, i);
					return TRUE;
				}
			}
		}
	}
	WindRxBuf(CBuf, i);
	return TRUE;
}

INT8U writeReceivedData(INT8U *buffer, INT16U *sLength)
{
	INT32U number;
	INT8U checksum;
	INT8U *dataPtr;
	INT16U i;
    INT16U j;

	INT32U address;


    INT8U tmp8u;
    INT32U tmp32u;


    INT16U start;
    INT16U end;

	lpDATA_HEADER dataHeader;

	INT8U opCode;
	INT8U *data;
	INT16U length;
	PACKET_HEADER *header;


    INT32S temp_inven = 0;


	INT8U long_exe_flag;
	long_exe_flag = 0;

	header = (PACKET_HEADER *)buffer;
	opCode = header->opCode[1];
	data = &buffer[sizeof(PACKET_HEADER)];
	length = header->length;

	address = header->address;

	dataPtr = data + sizeof(INT32U);
	number = *((INT32U _PACKED_DEF *)data);//data의 처음 4byte를 dword로 변환
  
	switch(opCode)
	{
        case 0x57://'W'
        
            return ERROR_NUMBER_OVER;
            
        default:
            return ERROR_INTERPRETER;
	}	

	checksum = 0;

	buffer[0]='G';
	buffer[1]=opCode;
    buffer[2]=1;//src
    buffer[3]=0;
    buffer[4]=0;//dest
    buffer[5]=0;
	buffer[6]=',';
	buffer[7] = 4;
	buffer[8] = 0;
	buffer[9]=':';
	memcpy(&buffer[10], &number, sizeof(INT32U));
	buffer[14]=':';

	for(i=2; i<15; i++)
		checksum += buffer[i];

	buffer[15]=checksum;
	buffer[16]=ASC_CR;

	*sLength = 17;

    return long_exe_flag;
}


INT8U readSavedData(INT8U *buffer, INT16U *sLength)
{
	INT32U number;
	INT16U packetLen;
	INT32U maddr;
	INT8U checksum;
	INT16U dataLen;
	INT8U *dataPtr;
	INT16U i;
	lpDATA_HEADER dataHeader;
	INT32U int32uBuf;
	INT8U opCode;
	INT8U *data;
	PACKET_HEADER _PACKED_DEF *header;

    INT32S temp_inven;
    INT32U address;
    INT8U tmp8u;
    INT32U tmp32u;

    
	header = (PACKET_HEADER *)buffer;
	opCode = header->opCode[1];
	data = &buffer[sizeof(PACKET_HEADER)];

	address = header->address;

	dataPtr = data;
	number = *((INT32U _PACKED_DEF *)data);

	switch (opCode)
	{
        case 0x57://'R'

            break;
		default :
			return ERROR_INTERPRETER;
	}
	buffer[0] = 'W';
	buffer[1] = opCode;
    buffer[2]=1;//src
    buffer[3]=0;
    buffer[4]=0;//dest
    buffer[5]=0;
	buffer[6] = ',';
	buffer[7] = (INT8U)dataLen;
	buffer[8] = (INT8U)(dataLen >> 8);
	buffer[9]=':';
	packetLen = sizeof(PACKET_HEADER) + dataLen;
	buffer[packetLen++] = ':';

	checksum = 0;
	for(i=2; i<packetLen; i++)
		checksum += buffer[i];

	buffer[packetLen++] = checksum;
	buffer[packetLen++] = ASC_CR;

	*sLength = packetLen;

	return 0;
}

void network_tx_proc(void)
{
    if (CommBufUart1.Txing != 1)
    {
        if(CheckTxBuf(&CommBufUart1))
        {
            //CommFlag = ON;
            Uart1SendData();
        }
    }
    if  (CommBufUart3.Txing != 1)
    {
        if(CheckTxBuf(&CommBufUart3))
        {
            Uart3SendData();
        }
    }    
}

#define MAX_SERIAL_BUFFER_LEN	256
#pragma pack(2)
typedef struct
{
	INT8U	sdtType;
	INT8U	sdtScaleId;
	INT8U	sdtPLUtype;
	INT8U	sdtDept;
	INT32U	sdtPLUNo;
	long	sdtItemCode;
	long	sdtWeight;
	INT16S	sdtQty;
	INT16S	sdtPCS;
	long	sdtUnitPrice;
	long	sdtTotalPrice;
	long	sdtDiscountPrice;
	long	sdtTransactionCounter;
	INT16U	sdtPrefix;
	INT8U	sdtStatus;
	INT8U	sdtDate[6];
	INT8U	sdtSaleDate[3];
	INT8U	sdtBarcodeStr[20];
	INT8U	sdtTraceNo[20];
	INT8U	sdtSlaughterHouse;
	INT8U	Reserved;
} TRANSACTION_SALE_DATA_TYPE;
#pragma pack()

INT16U serial_mode;
INT8U  serial_command;
INT8U  serial_cmdtype;
INT8U  serial_errtype;
INT8U  serial_bcc;
INT8U  serial_ethtype;
INT16U serial_length;
INT32U serial_number;
INT32U serial_fnumber;
INT16U serial_timeout_n;
short  serial_param_nstr;
INT8U serial_send_point[MAX_SERIAL_BUFFER_LEN];
INT8U serial_buffer_point[MAX_SERIAL_BUFFER_LEN];

INT8U gen_bcc(INT8U *buffer,INT16S leng)
{
	INT8U bcc;
	INT16S   i;

	bcc = 0;
	for (i=0; i<leng; i++) bcc^=buffer[i];
	return bcc;
}
#ifdef USE_ENET
INT16S ethernet_gen_head(char *str)
{
	INT16S slen;
	slen=0;
	//sprintf(str,"^=%02X.*=%02X.$=%01X.",status_scale.scaleid,status_scale.departmentid,status_scale.scale_lock);
    sprintf(str,"^=%02X.*=%02X.$=%01X.",1,1,0);
	slen=strlen(str);
	sprintf(&str[slen],"&=%02X%02X%02X%02X.",EthCardStruct.IP[3],EthCardStruct.IP[2],EthCardStruct.IP[1],EthCardStruct.IP[0]);
	slen+=strlen(&str[slen]);
	sprintf(&str[slen],"@=%04X.",EthCardStruct.TCPSourcePort);
	slen+=strlen(&str[slen]);
	//sprintf(&str[slen],"?=%1X.",ethernet_list.status);
    sprintf(&str[slen],"?=%1X.",2);
	slen+=strlen(&str[slen]);
	return slen;
}
#define BY_WEIGHT_PLU   1
#define BY_COUNT_PLU    2
static INT8U rtt_retry_cnt=0;
void transaction_sale_make_data_type4_Send(COMM_BUF *CBuf)
{
    //INT16U head, tail;  
	//INT16U cnt;
	//INT32U addr;
	//INT8U temp1;
	long  mul;
	TRANSACTION_SALE_DATA_TYPE trans_sdt;
    PLU_STRUCT art;
	//TRANSACTION_CHANGE_DATA_TYPE trans_cdt;
	INT16S   slen;
	INT8U bcc;
	//INT8U tflag;
	//INT16U plu_key;
	//INT16U len;
	char  *buffer;

    if(tm5fg) {
       timer_setup(5,1000);  //1000ms (1 * 1000)
                        
    if (rep_log_tail == rep_log_head) return;
    
    if (rtt_retry_cnt > 5) rtt_retry_cnt = 0;

    if (rtt_retry_cnt == 0) {
    //tail = 1;
    buffer=(char *)serial_send_point;
    FlushTxBuf(CBuf);//for test need check later
    
	slen = 15;

	//Scale Capa에 따른 무게값을 "g" 단위로 전송 하고자 할때 
	//temp1=get_global_bparam(GLOBAL_SALE_SETUP_FLAG7);
	//temp1=(temp1>>6)&0x03;
	mul = 1L;
	//if(temp1 > 0) mul = power10(temp1);

	//cnt = 1;
/* calc addr of Trans data
	addr=tail; 
	addr%=Startup.max_send_buf;
	addr*=MAX_TRANSACTION_TYPE4_SALE_SIZE;//sizeof(TRANSACTION_RECORD_TYPE); 
	addr+=Startup.addr_send_buf;
*/
	slen+= ethernet_gen_head(&buffer[slen]);
	sprintf(&buffer[slen],"T=%04X.",rep_log_tail);  	    
    slen+=7; // 현재 tail에 전송할 tr수를 더한 값. buffer 개수보다 클 수 있다. 

	//tflag, 0:transaction, 0x80: change unit price
	//tflag = get_nvram_bparam(addr);
    //tflag = 0;

/* read trans data trans_sdt
    get_nvram_sparam(addr,(INT8U *)&trans_sdt,sizeof(TRANSACTION_SALE_DATA_TYPE));
*/
    Eeprom_sread(LOG_BASE_ADDR+(rep_log_tail*LOG_STRUCTURE_SIZE), (INT8U *)&rep_log, LOG_STRUCTURE_SIZE);//rep_log_load();//
 
    //make test data
	memset((INT8U *)&trans_sdt, 0, sizeof(TRANSACTION_SALE_DATA_TYPE));

    trans_sdt.sdtType = 0;//sale data
    trans_sdt.sdtScaleId = 1;
    trans_sdt.sdtStatus = rep_log.saleType;//ex) add, void
    trans_sdt.sdtPLUtype = rep_log.pluType;//0;  
    trans_sdt.sdtDept = 1;
    trans_sdt.sdtPLUNo = rep_log.pluNo;//1;
    trans_sdt.sdtItemCode = rep_log.itemNo;//1; 
    if (rep_log.pluType == BY_WEIGHT_PLU) 
    {
        trans_sdt.sdtWeight = rep_log.weightOrcount;//100;
        trans_sdt.sdtQty = 0;//1;        
    }
    else 
    {
        trans_sdt.sdtWeight = 0;//100;
        trans_sdt.sdtQty = rep_log.weightOrcount;//1;                
    }
    trans_sdt.sdtQty = 1;
    trans_sdt.sdtPCS = 1;
    trans_sdt.sdtUnitPrice = rep_log.uprice;//10;
    trans_sdt.sdtTotalPrice = rep_log.tprice;//1000;
    trans_sdt.sdtDiscountPrice = 0;
    trans_sdt.sdtTransactionCounter = rep_log.ticketNo;//1;    
	trans_sdt.sdtDate[0] = rep_log.year;//1;//date.year;
	trans_sdt.sdtDate[1] = rep_log.month;//1;//date.month;
	trans_sdt.sdtDate[2] = rep_log.day;//1;//&date.day;
	trans_sdt.sdtDate[3] = rep_log.hours;//1;//&time.hours;
	trans_sdt.sdtDate[4] = rep_log.minutes;//1;//&time.minutes;
	trans_sdt.sdtDate[5] = rep_log.seconds;//1;//&time.seconds;
    trans_sdt.sdtSaleDate[0] = 1;    
	trans_sdt.sdtSaleDate[1] = 1;    
	trans_sdt.sdtSaleDate[2] = 1;    

    //HYP trace code
    memcpy(trans_sdt.sdtTraceNo, rep_log.traceCode, 20);//test 20byte
    
    trans_sdt.sdtWeight *= mul;
	memcpy(&buffer[slen],&trans_sdt,sizeof(TRANSACTION_SALE_DATA_TYPE));
	slen+=sizeof(TRANSACTION_SALE_DATA_TYPE);
/* read trans data trans_sdt
	plu_table_search(trans_sdt.sdtDept, trans_sdt.sdtPLUNo, &plu_key,0);
	memset(&buffer[slen], 0, 55);
	plu_get_field(plu_key-1, 10, (INT8U *)&buffer[slen], (INT16S *)&len, 55); // name1
*/
    //make test plu name
	//memset(&buffer[slen], 0, 55);
    //sprintf((INT8U *)&buffer[slen], "Test PLU");//plu name
    memset(&buffer[slen], 0, 31);
    plu_load(&art, rep_log.pluNo%1000000);
    memcpy(&buffer[slen], art.name, 31);
    //sprintf((INT8U *)&buffer[slen], "Test PLU");//plu name
    
	//buffer[slen+len] = 0;
	//slen += 55;
    slen += 31;
//	sprintf(buffer,"i00F070,%02XL%03X", code, slen-15);
  	sprintf(buffer,"i00F070,%02XL%03X", 0x40, slen-15);

	buffer[14]=':';
	bcc=gen_bcc((INT8U *)&buffer[15],slen-15);
	buffer[slen++]=bcc;

    commun_outleng((COMM_BUF *)CBuf,buffer,slen);
    
    }
    rtt_retry_cnt++;
    }
}


// "i00F070..."
INT8U recv_transaction_sale(COMM_BUF *CBuf,INT16S param2,INT8U *str,INT16S length)
// =====================================================
// Login 후 Sale Data 의 전송시만 사용.
// Transaction 시만 사용함.
// =====================================================
{
	//ETH_CONNECT_LIST eth;
	//TRANSACTION_RECORD_TYPE trans;
	INT16S status,r;//,z, x;
	INT32U v32;//,ticketno;
	INT8U code;//,clerk;
	INT16U tail;
	//INT8U ret;

	status=0;
	r     =0;
	tail  =0;

	//ticketno=0L;
	//memset((INT8U *)&eth,0,sizeof(ETH_CONNECT_LIST));
	//memset(&trans,0,sizeof(TRANSACTION_RECORD_TYPE));
	//z = sizeof(TRANSACTION_RECORD_TYPE);
	//x = 20;
	while (1) {
		if (r>=length) break;
		switch (status) {
			case 0: v32=0;
				code=str[r];
				if (code==0) goto END;
				status=1;
				break;
			case 1: status=0;
				if (str[r]=='=') status=2;
				break;
			case 2: if (str[r]=='.') {
					switch (code) {
					    case 'T': tail = (INT16U)v32;
						      //network_status.rcv_tail = tail;
                                if (rep_log_tail == tail) {
                                    rep_log_tail++;
                                    if (rep_log_tail >= MAX_REP_LOG) rep_log_tail = 0;
                                    rtt_retry_cnt = 0;
                                }

                              //tail 번호로 ACK확인 후 버퍼 삭제
					    	      break;
					    default : 
					    	      break;
					}
					status=0;
				} else {
					v32<<=4;
					v32|=ctoh(str[r]);
				}
		}
		r++;
	}
END:

	// ================================================
	// Param2 = 2 일때전송 완료.clerkid=0,
	// Param2 = 1 일때수신중 , 
	// ================================================
	switch (param2) {
		case 0x02: //slave
			//network_status.send_trans_sale = 1;
			break;
	}
	return 0;
}

void commun_setinfo(COMM_BUF *CBuf,INT8U action,INT16U cmd,INT32U fnumber,INT32U length,INT8U *str)
{
	//INT16S  ret;

	//recv_counter=0;
	//ret=1;
	cmd&=0x0ff;
	switch (cmd) {
		case 0x070: recv_transaction_sale(CBuf,(INT16S)fnumber,str,(INT16S)length);
			    goto NOAC;
		default   : break;//ret=FALSE;
	}
	fnumber=0;
	//commun_write_err_info(CBuf,(INT8U)action,cmd,ret);
NOAC:;
}

void serial_proc(COMM_BUF *CBuf)
{
	INT8U r_data;
	INT16S   com_type,rept,ret,isz,q;
	INT8U *serial_buffer;
	//WLAN_CARD_STRUCT ecard;
	//ETH_CONNECT_LIST eth;
	//INT8U first,cur_page;
    //INT8U backup_directdraw;

	rept  =0;
	//first =0;
	//recv_counter=0;
	serial_buffer=(INT8U *)serial_buffer_point;
REPT:
	isz = CheckRxBufCountInt(CBuf);

	for (q=0; q<isz; ) {
        r_data = GetCharRxBuf(CBuf);
		q++;
		switch(serial_mode) {
			case   0:
                serial_command=0;
				serial_ethtype = 0;
				serial_cmdtype=r_data;
				switch(r_data) {
					case 'i' : serial_mode = 50; break;
					default  : serial_mode = 0;  break;
				}
				break;
			case  50:
				 if (isdigit(r_data)) {
					serial_command<<=4;
					serial_command|=ctoh(r_data);
				 } else if (r_data=='F') {
					serial_number=0;
					serial_mode+=6;	//56
				 } else serial_mode=0;
				 break;
			case  56:
                   if (r_data==',') {
					serial_fnumber=0;
					serial_mode++;
					break;
				 }
				 serial_number<<=4;
				 serial_number |=ctoh(r_data);
				 break;
			case 57:
                 if (r_data=='L') {
					serial_mode++;
					serial_length=0;
					break;
				 }
				 serial_fnumber<<=4;
				 serial_fnumber |=ctoh(r_data);
				 break;
			case  58:
			     if (r_data==':') {
					serial_param_nstr=0;
					serial_bcc  =0;
					serial_mode++;
					if (serial_length >= MAX_SERIAL_BUFFER_LEN) 
					{
						//commun_write_err_info(CBuf,serial_command,serial_number,0x97);
						serial_mode = 0;
						goto TX_PROC;
					}
					break;
				 }
				 serial_length <<=4;
				 serial_length |=ctoh(r_data);
				 break;
			case  59:
REPT_1:	 
                 if (serial_param_nstr==serial_length) {
				    serial_mode=serial_mode/10;
					serial_buffer[serial_param_nstr]=0;
       			    if (r_data!=serial_bcc) {
       					//commun_write_err_info(CBuf,serial_command,serial_number,0xfe);
	    		    } else {
	        			commun_setinfo(CBuf,serial_command,(INT16U)serial_number,
    		    		serial_fnumber,(INT32U)serial_param_nstr,(INT8U *)serial_buffer);
			        }
			        serial_mode=0;
   					goto TX_PROC;
				 } else {
					serial_buffer[serial_param_nstr++]=r_data;
					serial_bcc^=r_data;
				 }
				 if (q<isz) {
					r_data = GetCharRxBuf(CBuf); q++;
					goto REPT_1;
				 }
				 break;
			default :serial_mode = 0;
				 break;
		}
		//serial_timeout_reset(com_type);
		rept=1;
		continue;
TX_PROC:    network_tx_proc();
	}
	//ret=serial_timeout_check(com_type);
	//if (network_status.allow_sale || flagTempAllowSale) {
		if (serial_mode==0) goto END;
	//}
	if (ret) goto END;
	if (rept) {
		if (com_type) {
			//EthDataProc(CBuf);
		}
		network_tx_proc();
		goto REPT;
	}
END:
    return;
}
#endif//#ifdef USE_ENET






//kevin : INT32S를 Decimal String으로 변환 //

/** @brief Decimal 자리수를 확인하기 위한 Table */
const INT32U DecPowerConvTable[DEC_POWER_CONV_TABLE_NUM] = {1L, 10L, 100L, 1000L, 10000L, 100000L, 1000000L, 10000000L, 100000000L, 1000000000L, 10000000000L, 10000000000L};
/** @brief Hexa 자리수를 확인하기 위한 Table */
const INT32U HexPowerConvTable[HEX_POWER_CONV_TABLE_NUM] = {0x01L, 0x10L, 0x100L, 0x1000L, 0x10000L, 0x100000L, 0x1000000L, 0x10000000L};


/**
********************************************************************************
* @brief    INT32S를 Decimal String으로 변환
* @param    dst : 출력할 String Pointer\n
*           numData : 변환할 number data\n
*           size : dst String size\n
*           decPos : Decimal Position (소수점 위치)
*           decChar : Decimal Point Char (0x00 이면 삽입하지 않음), Print시(String 변환시) 사용
*           useThousandPt : use flag for thousnad point, Print시(String 변환시) 사용
* @return   String size내에서 '-'sign이 처리가 안될 때 추가로 '-'sign flag 
*           필요하다는 의미로 '1'반환, 그외 '0' 반환
* @remark   Decimal Position 까지는 '0' 채움 (예 0,00)
*           남는 자리는 space로 채움 (오른쪽 정렬)
********************************************************************************
*/

INT8U Int32sToDecStr(char *dst, INT32S numData, INT8U size, char fillChar, INT8U decPos, char decChar, INT8U useThousandPt)
{
    
    
	INT8U pos;
	INT8U i;
	INT8U byte;
	INT8U endFlag;
	INT8U auxMinusSignFlag;
	INT8U sign;
	INT8U strSize;
	INT32U	absNumData;
	INT8U use1000Pt;

	endFlag = 0;
	strSize = 0;
	pos = size - 1;
	auxMinusSignFlag = OFF;

	if (fillChar == 0) 
	{
		fillChar = 0x20; //space
	}

	if (numData < 0) 
	{
		sign = 1;
		absNumData = -numData;
	}
	else
	{
		sign = 0;
		absNumData = numData;
	}

	dst[size] = 0x00;	//null

	if (((absNumData / DecPowerConvTable[decPos]) >= 1000) && (useThousandPt == 1))  //kevin : 실제값/자릿수배열값 >=1000 , decPos는 자릿수를 의미
	{
		use1000Pt = 1; 
	}
	else
	{
		use1000Pt = 0;
	}

	for (i = 0; i < size; i++)
	{
		if ((endFlag == 0) || (i <= decPos))
		{
                    if (pos > 0) //kevin : 최종 변환된 데이터의 자릿수 -1 > 0 ?
			{
				if ((decChar != 0x00) && (decPos != 0) && (decPos == i))
				{
					dst[pos] = decChar;
					pos--;
					strSize++;
				} 
				else if ((use1000Pt == 1) && ((decPos + 3) == i))
				{
					dst[pos] = ',';	//COMMA
					pos--;
					strSize++;
				}
			}
			byte = (INT8U)(absNumData % 10);
                        dst[pos] = byte + '0'; //kevin : 0x30이 문자열로 0 이니까
			strSize++;
			absNumData = absNumData / 10;
			if (absNumData == 0) 
			{
				endFlag = 1;
			}
		}
		else
		{
			dst[pos] = fillChar;//Space Character
		}
		if (pos == 0) 
		{
			break;
		}
		pos--;
	}

	if (sign == 1) //kevin : 음수일때 - 부호 추가
	{
		if (strSize < size)
		{
			dst[size - strSize - 1] = '-';
		}
		else
		{
			auxMinusSignFlag = ON;
		}
	}

	return auxMinusSignFlag;
}


INT8U Int32sToUnsignedDecStr(char *dst, INT32U numData, INT8U size, char fillChar, INT8U decPos, char decChar, INT8U useThousandPt)
{
    
    
	INT8U pos;
	INT8U i;
	INT8U byte;
	INT8U endFlag;
	INT8U auxMinusSignFlag;
	INT8U sign;
	INT8U strSize;
	INT32U	absNumData;
	INT8U use1000Pt;

	endFlag = 0;
	strSize = 0;
	pos = size - 1;
	auxMinusSignFlag = OFF;

	if (fillChar == 0) 
	{
		fillChar = 0x20; //space
	}

//	if (numData < 0) 
//	{
//		sign = 1;
//		absNumData = -numData;
//	}
//	else
//	{
		sign = 0;
		absNumData = numData;
//	}

	dst[size] = 0x00;	//null

	if (((absNumData / DecPowerConvTable[decPos]) >= 1000) && (useThousandPt == 1))  //kevin : 실제값/자릿수배열값 >=1000 , decPos는 자릿수를 의미
	{
		use1000Pt = 1; 
	}
	else
	{
		use1000Pt = 0;
	}

	for (i = 0; i < size; i++)
	{
		if ((endFlag == 0) || (i <= decPos))
		{
                    if (pos > 0) //kevin : 최종 변환된 데이터의 자릿수 -1 > 0 ?
			{
				if ((decChar != 0x00) && (decPos != 0) && (decPos == i))
				{
					dst[pos] = decChar;
					pos--;
					strSize++;
				} 
				else if ((use1000Pt == 1) && ((decPos + 3) == i))
				{
					dst[pos] = ',';	//COMMA
					pos--;
					strSize++;
				}
			}
			byte = (INT8U)(absNumData % 10);
                        dst[pos] = byte + '0'; //kevin : 0x30이 문자열로 0 이니까
			strSize++;
			absNumData = absNumData / 10;
			if (absNumData == 0) 
			{
				endFlag = 1;
			}
		}
		else
		{
			dst[pos] = fillChar;//Space Character
		}
		if (pos == 0) 
		{
			break;
		}
		pos--;
	}

//	if (sign == 1) //kevin : 음수일때 - 부호 추가
//	{
//		if (strSize < size)
//		{
//			dst[size - strSize - 1] = '-';
//		}
//		else
//		{
//			auxMinusSignFlag = ON;
//		}
//	}

	return auxMinusSignFlag;
}


INT8U Int32sToDecStrMachine(char *dst, INT32S numData, INT8U size, char fillChar, INT8U decPos, char decChar, INT8U useThousandPt, INT8U stableFlag)
{
    INT8U auxMinusSignFlag;

    
    dst[0] = 'T';
    dst[1] = '=';

    auxMinusSignFlag = Int32sToDecStr(dst + 2, numData, size - 6,  fillChar, decPos, decChar, useThousandPt);

    dst[size-4] = 0x20;    
    dst[size-3] = 'm';
    dst[size-2] = 'g';
    if (stableFlag == 1)
    {
        dst[size-1] = 'S';
    }
    else
    {
        dst[size-1] = ' ';
    }
    dst[size] = 0x00;
	return auxMinusSignFlag;
}


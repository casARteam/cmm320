/*****************************************************************************
 |*			
 |*  Copyright		:	(c) 2002 CAS
 |*  Filename		:	filter.c
 |*  Version			:	0.1
 |*  Programmer(s)	:	Yun Yeo Chul (YYC)
 |*  Created			:	2016/07/11
 |*  Modify              :       
 |*  Description		:	WC300 MOG System
 |*				
 ****************************************************************************/


#include <stdio.h>
#include <string.h>
#include <float.h>
#include "globals.h"
#include "serial.h"
#include "mode_main.h"
#include "commbuf.h"
#include "comm_app.h"
#include "common.h"
#include "RingBuf.h"
#include "timer.h"
#include "analyze.h"
#include "filter.h"

//#ifdef USE_COMPENSATION
FP64 Pk_1st = 1.0;
FP64 varP_1st = 0.001;
FP64 R_1st = 7.5;
FP64 Kk_1st = 1.0;
FP64 Xk_1st = 0;

FP64 Pk_2nd = 1.0;
FP64 varP_2nd = 0.0001;
FP64 R_2nd = 15;
FP64 Kk_2nd = 1;
FP64 Xk_2nd = 0;


MAF_STRUCT Maf1stOrder;
MAF_STRUCT Maf2ndOrder;
MAF_STRUCT Maf3rdOrder;
MAF_STRUCT Maf4thOrder;
MAF_STRUCT Maf5thOrder;
MAF_STRUCT Maf6thOrder;
MAF_STRUCT Maf7thOrder;
MAF_STRUCT MafCreepOrder;
MAF_STRUCT MafCreepTrackingOrder;
MAF_STRUCT MafDCTrackingOrder;



FP64 MAF_buf_1st_order[MAF_BUF_MAX_SIZE];
FP64 MAF_buf_2nd_order[MAF_BUF_MAX_SIZE];
FP64 MAF_buf_3rd_order[MAF_BUF_MAX_SIZE];
FP64 MAF_buf_4th_order[MAF_BUF_MAX_SIZE];
FP64 MAF_buf_5th_order[MAF_BUF_MAX_SIZE];
FP64 MAF_buf_6th_order[MAF_BUF_MAX_SIZE];
FP64 MAF_buf_7th_order[MAF_7TH_ORDER_BUF_MAX_SIZE]; //insert for mg check test 2016.08.24 YYC
FP64 MAF_buf_Creep_order[MAF_BUF_MAX_SIZE];
FP64 MAF_buf_CreepTracking_order[MAF_BUF_MAX_SIZE];
//DC TRACKING FILTER
FP64 MAF_buf_DCTracking_order[MAF_BUF_MAX_SIZE];


/*simply Kalman Filter*/
FP64 WC300_KalmanFilter_1st(FP64 rawData)
{
	static INT8U initFlag = false;
	
	if(!initFlag)
	{
		Xk_1st = rawData;
		initFlag = true;
	}
    
	Pk_1st = Pk_1st + varP_1st;
    Kk_1st = Pk_1st / (Pk_1st + R_1st);
    Xk_1st = (Kk_1st * rawData) + (1 - Kk_1st) * Xk_1st;
    Pk_1st = (1 - Kk_1st) * Pk_1st;
	
	return Xk_1st;
}

FP64 WC300_KalmanFilter_2nd(FP64 rawData)
{
	static INT8U initFlag = false;
	
	if(!initFlag)
	{
		Xk_2nd = rawData;
		initFlag = true;
	}
    
	Pk_2nd = Pk_2nd + varP_2nd;
    Kk_2nd = Pk_2nd / (Pk_2nd + R_2nd);
    Xk_2nd = (Kk_2nd * rawData) + (1 - Kk_2nd) * Xk_2nd;
    Pk_2nd = (1 - Kk_2nd) * Pk_2nd;
	
	return Xk_2nd;
}

void WC300_MAF_Init(MAF_STRUCT *MAFBuf, INT32U windowSize,INT8U order)
{
	(*MAFBuf).index = 0;
	(*MAFBuf).winSize = windowSize;
	(*MAFBuf).mafOnFlag = OFF;
	switch(order)
	{
		case MAF_1ST_ORDER:
			memset(MAF_buf_1st_order,0,sizeof(MAF_buf_1st_order));
			(*MAFBuf).buf = MAF_buf_1st_order;
			break;
		case MAF_2ND_ORDER:
			memset(MAF_buf_2nd_order,0,sizeof(MAF_buf_2nd_order));
			(*MAFBuf).buf = MAF_buf_2nd_order;
			break;
		case MAF_3RD_ORDER:
			memset(MAF_buf_3rd_order,0,sizeof(MAF_buf_3rd_order));
			(*MAFBuf).buf = MAF_buf_3rd_order;
			break;
		case MAF_4TH_ORDER:
			memset(MAF_buf_4th_order,0,sizeof(MAF_buf_4th_order));
			(*MAFBuf).buf = MAF_buf_4th_order;
			break;
		case MAF_5TH_ORDER:
			memset(MAF_buf_5th_order,0,sizeof(MAF_buf_5th_order));
			(*MAFBuf).buf = MAF_buf_5th_order;
			break;
		case MAF_6TH_ORDER:
			memset(MAF_buf_6th_order,0,sizeof(MAF_buf_6th_order));
			(*MAFBuf).buf = MAF_buf_6th_order;
			break;
		case MAF_CREEP_ORDER:
			memset(MAF_buf_Creep_order,0,sizeof(MAF_buf_Creep_order));
			(*MAFBuf).buf = MAF_buf_Creep_order;
			break;
		case MAF_CREEPTRACKING_ORDER:
			memset(MAF_buf_CreepTracking_order,0,sizeof(MAF_buf_CreepTracking_order));
			(*MAFBuf).buf = MAF_buf_CreepTracking_order;
			break;
        case MAF_7TH_ORDER:
			memset(MAF_buf_7th_order,0,sizeof(MAF_buf_7th_order));
			(*MAFBuf).buf = MAF_buf_7th_order;
			break;
        case MAF_DCTRACKING_ORDER:
            memset(MAF_buf_DCTracking_order,0,sizeof(MAF_buf_DCTracking_order));
			(*MAFBuf).buf = MAF_buf_DCTracking_order;
			break;
	}
}

FP64 WC300_MovingAVG(MAF_STRUCT *MAFBuf, FP64 inData)
{
    
	INT16U i;
	FP64 mafFilteredData = 0;
	FP64 sumData = 0;	
	INT32U bufIndex;
	INT32U currIndex;
	INT32U mafWinSize;
    
	
	currIndex = (*MAFBuf).index;
	mafWinSize = (*MAFBuf).winSize;
	
	if((*MAFBuf).mafOnFlag == OFF)
	{
		if (!inData) return inData;
		if (currIndex == mafWinSize) (*MAFBuf).mafOnFlag = ON;
		(*MAFBuf).buf[currIndex] = inData;
		(*MAFBuf).index++;
		if ((*MAFBuf).mafOnFlag != ON)
		{
			return 0;
		}
		else
		{
			return inData;
		}
	}
    
	(*MAFBuf).buf[currIndex] = inData;
	
	sumData = 0;
	
	for (i = 0; i < mafWinSize; i++)
	{
		bufIndex = (currIndex + MAF_BUF_MAX_SIZE - i) % MAF_BUF_MAX_SIZE;
		sumData = sumData + (*MAFBuf).buf[bufIndex];
	}
	mafFilteredData = sumData / mafWinSize;
    
	(*MAFBuf).index++;
	(*MAFBuf).index = (*MAFBuf).index % MAF_BUF_MAX_SIZE;
    
	return mafFilteredData;	
    
}

//AdData.rawSetZeroData = AdData.rawInitZeroData - zeroBuf;	
void WC300_zeroTracking(FP64 dispFilteredData)
{
	INT16U zeroBufBackup;
	static INT8U count = 0;
    
	count++;
	
	if (count >= zeroTrackingEnterCount)
	{
		count = 0;
        
		zeroBufBackup = zeroBuf;			// Backup
		
		if (AbsFP64(dispFilteredData - zeroBuf) < (INT32S)(zeroTrackingRange) && (dispFilteredData - zeroBuf) != 0) 
		{
			if ((dispFilteredData - zeroBuf) > 0)
			{
				zeroBuf++;
			}
			else 
			{
				zeroBuf--;
			}
		}
		
		if (AbsFP64(zeroBuf) > rezeroLimit) 
		{
			zeroBuf = zeroBufBackup;	// Restore
		}
	}
}

FP64 WC300_DispToExtValue(FP64 displacementVal)
{
	
	FP64 weightVal = 0;
	
	weightVal = displacementVal / 4.7;
	
	return weightVal;
	
	
}
//#endif


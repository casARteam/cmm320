///*****************************************************************************
/*
|*  Copyright       :   (c) 2011 CAS
|*  Filename        :   timer.c
|*  Version         :   0.1
|*  Programmer(s)   :   Yun Yeo-Chul (YYC)
|*  Created         :   2016/3/22
|*  Modified        :
|*  Description     :   WC300 Temperature Sensor timer function
*/
//****************************************************************************/
#include "globals.h"
#include "timer.h"

/*
********************************************************************************
*                       GLOBAL(FILE SCOPE) VARIABLES & TABLES
********************************************************************************
*/
/* Insert file scope variable & tables here */
/** @brief 10ms Timer 시작 시간 */
static INT16U timerStart10ms[TIMER_DELAY_10MS_MAX_NUM];
/** @brief 10ms Timer Delay 시간 */
static INT16U timerDelay10ms[TIMER_DELAY_10MS_MAX_NUM];
/** @brief 100ms Timer 시작 시간 */
static INT16U timerStart100ms[TIMER_DELAY_100MS_MAX_NUM];
/** @brief 100ms Timer Delay 시간 */
static INT16U timerDelay100ms[TIMER_DELAY_100MS_MAX_NUM];

INT16U SysTimer100ms;
INT16U SysTimer10ms;
INT16U SysTimer16ms;
INT16U SysTimer1ms;



/**
********************************************************************************
* @brief    10ms 단위로 돌아가는 System Timer 값을 읽는 함수\n
* @param    none
* @return   none
* @remark   
********************************************************************************
*/
INT16U TimerDrvGetSysTimer10ms(void)
{
	return SysTimer10ms;
}

/**
********************************************************************************
* @brief    100ms 단위로 돌아가는 System Timer 값을 읽는 함수\n
* @param    none
* @return   none
* @remark   
********************************************************************************
*/
INT16U TimerDrvGetSysTimer100ms(void)
{
	return SysTimer100ms;
}

/**
********************************************************************************
* @brief    Timer API Init 함수\n
*           Timer Driver 초기화\n
*           각종 Timer 초기화\n
* @param    smode : 다음에 실행할 모드
* @return   none
* @remark   초기에 한번 실행 되는 함수\n
********************************************************************************
*/
void TimerInit(void)
{
	INT8U i;

//	TimerDrvInit();
    SysTimer1ms = 0;
    SysTimer100ms = 0;
    SysTimer10ms = 0;
    SysTimer16ms = 0;

	for (i = 0; i < TIMER_DELAY_100MS_MAX_NUM; i++)
	{
		timerDelay100ms[i] = 0;
	}
    for (i = 0; i < TIMER_DELAY_10MS_MAX_NUM; i++)
	{
		timerDelay10ms[i] = 0;
	}
}
/**
********************************************************************************
* @brief    100ms 단위로 돌아가는 System Timer 값을 읽는 함수\n
* @param    none
* @return   none
* @remark   
********************************************************************************
*/
INT16U TimerGetSysTimer10ms(void)
{
	return TimerDrvGetSysTimer10ms();
}

/**
********************************************************************************
* @brief    100ms 단위로 돌아가는 System Timer 값을 읽는 함수\n
* @param    none
* @return   none
* @remark   
********************************************************************************
*/
INT16U TimerGetSysTimer100ms(void)
{
	return TimerDrvGetSysTimer100ms();
}

/**
********************************************************************************
* @brief    10ms 단위의 delay Time을 Setting하는 함수\n
* @param    num : 10ms timer 번호(TIMER_DELAY_10MS_DISP_MSG ~ )
*           delay : delay 시간 (단위 : 10ms)
* @return   none
* @remark   TimerCheckDelay10ms()를 이용하여 Timer가 끝났는지 Check할 수 있음
********************************************************************************
*/
void TimerSetDelay10ms(INT8U num, INT16U delay)
{
	timerStart10ms[num] = TimerDrvGetSysTimer10ms();
	timerDelay10ms[num] = delay;
}

/**
********************************************************************************
* @brief    100ms 단위의 delay Time을 Setting하는 함수\n
* @param    num : 100ms timer 번호(TIMER_DELAY_100MS_DISP_MSG ~ )
*           delay : delay 시간 (단위 : 100ms)
* @return   none
* @remark   TimerCheckDelay100ms()를 이용하여 Timer가 끝났는지 Check할 수 있음
********************************************************************************
*/
void TimerSetDelay100ms(INT8U num, INT16U delay)
{
	timerStart100ms[num] = TimerDrvGetSysTimer100ms();
	timerDelay100ms[num] = delay;
}

/**
********************************************************************************
* @brief    TimerSetDelay10ms()에서 Setting Timer를 Check하는 함수\n
* @param    num : 10ms timer 번호(TIMER_DELAY_10MS_DISP_MSG ~ )
* @return   0 -> not complete, 1 -> complete (timer is over)
* @remark   see TimerSetDelay10ms()
********************************************************************************
*/
INT8U TimerCheckDelay10ms(INT8U num)
{
	
	if (timerDelay10ms[num] == 0)
	{
		return 0;
	}

	if (timerDelay10ms[num] > ((TimerDrvGetSysTimer10ms() - timerStart10ms[num])&0xFFFF)) 
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

/**
********************************************************************************
* @brief    TimerSetDelay100ms()에서 Setting Timer를 Check하는 함수\n
* @param    num : 100ms timer 번호(TIMER_DELAY_100MS_DISP_MSG ~ )
* @return   0 -> not complete, 1 -> complete (timer is over)
* @remark   see TimerSetDelay100ms()
********************************************************************************
*/
INT8U TimerCheckDelay100ms(INT8U num)
{
	
	if (timerDelay100ms[num] == 0)
	{
		return 0;
	}

	if (timerDelay100ms[num] > ((TimerDrvGetSysTimer100ms() - timerStart100ms[num])&0xFFFF)) 
	{
		return 0;
	}
	else
	{
		return 1;
	}
}



void _nop__(void){
};

void DelayClock(volatile INT16U count)
{
	volatile INT16U i;
	for(i=0;i<count;i++)
	{
		;
	}
}


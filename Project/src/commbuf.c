///*****************************************************************************
/*
|*  Copyright       :   (c) 2002 CAS
|*  Filename        :   commbuf.c
|*  Version         :   0.1
|*  Programmer(s)   :   Hong Yi-Phyo (HYP)
|*  Created         :   2002/12/10
|*  Description     :   CLP Printer communication buffer routine    
*/
///****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "globals.h"
#include "main.h"
#include "commbuf.h"


COMM_BUF CommBufUart1;     //UART1
COMM_BUF CommBufUart3;     //UART3
char CommTxBufUart1Memory[COM_MAX_TX_BUF_UART1_SIZE];
char CommRxBufUart1Memory[COM_MAX_RX_BUF_UART1_SIZE];
char CommTxBufUart3Memory[COM_MAX_TX_BUF_UART3_SIZE]; //kevin uart3
char CommRxBufUart3Memory[COM_MAX_RX_BUF_UART3_SIZE]; //kevin uart3

void Inline_PutCharTxBuf(COMM_BUF *CBuf, char p)
{
    (*CBuf).TxBuf[(*CBuf).TxBufInPtr] = p;
	
    switch ((*CBuf).Type) 
    {
        case COMM_TYPE_UART1:
            if ((*CBuf).TxBufInPtr >= (COM_MAX_TX_BUF_UART1_SIZE-1)) 
            {
                (*CBuf).TxBufInPtr = 0;
            }
            else 
            {
                (*CBuf).TxBufInPtr++;
            }
            break;
        case COMM_TYPE_UART3: //kevin uart3
            if ((*CBuf).TxBufInPtr >= (COM_MAX_TX_BUF_UART3_SIZE-1)) 
            {
                (*CBuf).TxBufInPtr = 0;
            }
            else 
            {
                (*CBuf).TxBufInPtr++;
            }
            break;
    }
}
////////////////

void CommBufInit(COMM_BUF *CBuf, INT8U type, INT8U num)
{
    (*CBuf).Txing = 0;
    (*CBuf).Rxing = 0;
    (*CBuf).TxBufOutPtr = 0;
    (*CBuf).TxBufInPtr = (*CBuf).TxBufOutPtr;
    (*CBuf).RxBufOutPtr = 0;
    (*CBuf).RxBufInPtr = (*CBuf).RxBufOutPtr;
    
    switch (type) 
    {
        case COMM_TYPE_UART1:
            (*CBuf).MaxRxBufSize = COM_MAX_RX_BUF_UART1_SIZE;
            (*CBuf).Type = COMM_TYPE_UART1;
            (*CBuf).TxBuf = CommTxBufUart1Memory;
            (*CBuf).RxBuf = CommRxBufUart1Memory;
            break;
            
        case COMM_TYPE_UART3: //kevin uart3
            (*CBuf).MaxRxBufSize = COM_MAX_RX_BUF_UART3_SIZE;
            (*CBuf).Type = COMM_TYPE_UART3;
            (*CBuf).TxBuf = CommTxBufUart3Memory;
            (*CBuf).RxBuf = CommRxBufUart3Memory;
            break;
    }
}

void FlushTxBuf(COMM_BUF *CBuf)
{
    (*CBuf).Txing = 0;
    (*CBuf).TxBufInPtr = 0;
    (*CBuf).TxBufInPtr = (*CBuf).TxBufOutPtr;
}

void FlushRxBuf(COMM_BUF *CBuf)
{
    (*CBuf).Rxing = 0;
    (*CBuf).RxBufInPtr = 0;
    (*CBuf).RxBufInPtr = (*CBuf).RxBufOutPtr;
}

void PutCStrTxBuf(COMM_BUF *CBuf, const char *p)
{
    ROMDATA char *s;
    
    s = (ROMDATA char*)p;
    while (*s != '\0') {
        Inline_PutCharTxBuf(CBuf, *s);
        s++;
    }
}

void PutStrTxBuf(COMM_BUF *CBuf,  char *p)
{
    while (*p != '\0') {
        Inline_PutCharTxBuf(CBuf, *p);
        p++;
    }
}

char GetCharTxBuf(COMM_BUF *CBuf)
{
    char c;
    
    c = (*CBuf).TxBuf[(*CBuf).TxBufOutPtr];
    
    switch ((*CBuf).Type) 
    {
        case COMM_TYPE_UART1:
            if ((*CBuf).TxBufOutPtr >= (COM_MAX_TX_BUF_UART1_SIZE-1)) (*CBuf).TxBufOutPtr = 0;
            else (*CBuf).TxBufOutPtr++;
            break;
        case COMM_TYPE_UART3: //kevin uart3
            if ((*CBuf).TxBufOutPtr >= (COM_MAX_TX_BUF_UART3_SIZE-1)) (*CBuf).TxBufOutPtr = 0;
            else (*CBuf).TxBufOutPtr++;
            break;
    }
    return c;
}

void RewindTxBuf(COMM_BUF *CBuf)
{
    switch ((*CBuf).Type) 
    {
        case COMM_TYPE_UART1:
            if ((*CBuf).TxBufOutPtr <= 0) (*CBuf).TxBufOutPtr = COM_MAX_TX_BUF_UART1_SIZE-1;
            else (*CBuf).TxBufOutPtr--;
            break;
    }
}

BOOLEAN CheckTxBuf(COMM_BUF *CBuf)
{
    if ((*CBuf).TxBufInPtr != (*CBuf).TxBufOutPtr)	{
        return TRUE;
    }
    return FALSE;
}

BOOLEAN CheckRxBuf(COMM_BUF *CBuf)
{
    if ((*CBuf).RxBufInPtr != (*CBuf).RxBufOutPtr) {
        return TRUE;
    } 
    return FALSE;
}


int CheckRxBufCountInt(COMM_BUF *CBuf)
{
    INT16S sz;

    if ((*CBuf).RxBufInPtr > (*CBuf).RxBufOutPtr)
    {
        return ((*CBuf).RxBufInPtr - (*CBuf).RxBufOutPtr);
    }
    else if ((*CBuf).RxBufInPtr < (*CBuf).RxBufOutPtr)
    {
        switch ((*CBuf).Type)
        {
            case COMM_TYPE_UART1: sz = COM_MAX_RX_BUF_UART1_SIZE; break;
        }
        return ((*CBuf).RxBufInPtr + sz - (*CBuf).RxBufOutPtr);
    } 
    return 0;
}

BOOLEAN CheckRxBufCount(COMM_BUF *CBuf, INT16U count)
{
    if ((*CBuf).RxBufInPtr > (*CBuf).RxBufOutPtr)
    {
        if(((*CBuf).RxBufInPtr - (*CBuf).RxBufOutPtr) >= count) 
        {
            return TRUE;
        } 
        else 
        {
            return FALSE;
        }
    } 
    else if ((*CBuf).RxBufInPtr < (*CBuf).RxBufOutPtr) 
    {
        switch ((*CBuf).Type) 
        {
            case COMM_TYPE_UART1:
                if(((*CBuf).RxBufInPtr + COM_MAX_RX_BUF_UART1_SIZE - (*CBuf).RxBufOutPtr) >= count) return TRUE;
                else return FALSE; 
        }
    } 
    return FALSE;
}


INT16U CheckRxBufChars(COMM_BUF *CBuf)
{
    if ((*CBuf).RxBufInPtr > (*CBuf).RxBufOutPtr)
    {
        return (*CBuf).RxBufInPtr - (*CBuf).RxBufOutPtr;
    } 
    else if ((*CBuf).RxBufInPtr < (*CBuf).RxBufOutPtr) 
    {
        switch ((*CBuf).Type)
        {
            case COMM_TYPE_UART1:
                return (*CBuf).RxBufInPtr + COM_MAX_RX_BUF_UART1_SIZE - (*CBuf).RxBufOutPtr;
        }
    } 
    return 0;
}
//HYP 20041213
INT16U CheckTxBufChars(COMM_BUF *CBuf)
{
    if ((*CBuf).TxBufInPtr > (*CBuf).TxBufOutPtr)
    {
        return (*CBuf).TxBufInPtr - (*CBuf).TxBufOutPtr;
    }
    else if ((*CBuf).TxBufInPtr < (*CBuf).TxBufOutPtr) 
    {
        switch ((*CBuf).Type) 
        {
            case COMM_TYPE_UART1:
                return (*CBuf).TxBufInPtr + COM_MAX_TX_BUF_UART1_SIZE - (*CBuf).TxBufOutPtr;		
        }
    } 
    return 0;
}

char GetCharRxBuf(COMM_BUF *CBuf)
{
    char c;
    
    c = (*CBuf).RxBuf[(*CBuf).RxBufOutPtr];

    switch ((*CBuf).Type) 
    {
        case COMM_TYPE_UART1:
            if ((*CBuf).RxBufOutPtr >= (COM_MAX_RX_BUF_UART1_SIZE-1)) (*CBuf).RxBufOutPtr = 0;
            else (*CBuf).RxBufOutPtr++;
            break;            
        case COMM_TYPE_UART3:
            if ((*CBuf).RxBufOutPtr >= (COM_MAX_RX_BUF_UART3_SIZE-1)) (*CBuf).RxBufOutPtr = 0;
            else (*CBuf).RxBufOutPtr++;
            break;            
    }
    return c;
}

INT16U GetRxBufSize(COMM_BUF *CBuf)
{
    INT32S tmp, tmpUART;

    tmp = (*CBuf).RxBufInPtr - (*CBuf).RxBufOutPtr;
    tmpUART = (*CBuf).RxBufInPtr + COM_MAX_RX_BUF_UART1_SIZE - (*CBuf).RxBufOutPtr;
    

    if (tmp > 0)
    {

        return (INT16U)tmp;
    }

    else if (tmp < 0) 
    {
        switch ((*CBuf).Type) 
        {
            case COMM_TYPE_UART1:

                return (INT16U)tmpUART;
        }
    } 
    return 0;
}

void 
PutCharTxBuf(COMM_BUF *CBuf, char c)
{
    (*CBuf).TxBuf[(*CBuf).TxBufInPtr] = c; //kevin : c는 눌린키의 hex 값(아스키 코드 참조)
    
    switch ((*CBuf).Type) 
    {
        case COMM_TYPE_UART1: //kevin : (*CBuf).Type 이 1인지 확인
            if ((*CBuf).TxBufInPtr >= (COM_MAX_TX_BUF_UART1_SIZE-1)) //COM_MAX_TX_BUF_UART1_SIZE   1024
            {
                (*CBuf).TxBufInPtr = 0;
            }
            else (*CBuf).TxBufInPtr++;
        break;
        case COMM_TYPE_UART3: //kevin uart3
            if ((*CBuf).TxBufInPtr >= (COM_MAX_TX_BUF_UART3_SIZE-1))
            {
                (*CBuf).TxBufInPtr = 0;
            }
            else (*CBuf).TxBufInPtr++;
        break;
    }
}

void PutCharRxBuf(COMM_BUF *CBuf, char c)
{
    (*CBuf).RxBuf[(*CBuf).RxBufInPtr] = c;
    
    switch ((*CBuf).Type) 
    {
        case COMM_TYPE_UART1:
            if ((*CBuf).RxBufInPtr >= (COM_MAX_RX_BUF_UART1_SIZE-1)) (*CBuf).RxBufInPtr = 0;
            else (*CBuf).RxBufInPtr++;
            break;
        case COMM_TYPE_UART3: //keinv uart3
            if ((*CBuf).RxBufInPtr >= (COM_MAX_RX_BUF_UART3_SIZE-1)) (*CBuf).RxBufInPtr = 0;
            else (*CBuf).RxBufInPtr++;
            break;
    }
}

void PutDataRxBuf(COMM_BUF *CBuf,  char *p, INT16U count)
{
    while (count != 0) {
        PutCharRxBuf(CBuf, *p);
        count--;
        p++;
    }
}

void PutDataTxBuf(COMM_BUF *CBuf,  char *p, INT16U count)
{
    while (count != 0)
    {
        Inline_PutCharTxBuf(CBuf, *p);
        count--;
        p++;
    }
}

void GetDataTxBuf(COMM_BUF *CBuf,  char *p, INT16U count)
{
    while (count != 0) {
        *p = GetCharTxBuf(CBuf);
        count--;
        p++;
    }
}

void GetDataRxBuf(COMM_BUF *CBuf, char *data, INT16U size)
{
	INT16U dataSize;

	if ((CBuf->RxBufOutPtr + size) > CBuf->MaxRxBufSize)
	{
		dataSize = CBuf->MaxRxBufSize - CBuf->RxBufOutPtr;
		memcpy(data, &CBuf->RxBuf[CBuf->RxBufOutPtr], dataSize);
		memcpy(&data[dataSize], CBuf->RxBuf, size - dataSize);
	}
	else
	{
		memcpy(data, &CBuf->RxBuf[CBuf->RxBufOutPtr], size);
	}
}

void RewindRxBuf(COMM_BUF *CBuf, short num)
{
    short i;
    
    for (i = 0; i<num; i++) 
    {
        switch ((*CBuf).Type) 
        {
            case COMM_TYPE_UART1:
                if ((*CBuf).RxBufOutPtr <= 0) (*CBuf).RxBufOutPtr = COM_MAX_RX_BUF_UART1_SIZE-1;
                else (*CBuf).RxBufOutPtr--;
                break;
        }
    }
}

void WindRxBuf(COMM_BUF *CBuf, short num)
{
    short i;
    
    for (i = 0; i<num; i++) {
        GetCharRxBuf(CBuf);
    }
}

void PutStrTxBuf_copy(COMM_BUF *CBuf_dst, COMM_BUF *CBuf_src)
{
    char ch;
    
    while (CheckRxBuf(CBuf_src)) {
        ch = GetCharRxBuf(CBuf_src);
        Inline_PutCharTxBuf(CBuf_dst, ch);
    }
}

void PutStrTx2Buf_copy(COMM_BUF *CBuf_dst, COMM_BUF *CBuf_src)
{
    char ch;
    
    while (CheckTxBuf(CBuf_src)) {
        ch = GetCharTxBuf(CBuf_src); 
        Inline_PutCharTxBuf(CBuf_dst, ch);
    }
}


void PutStrRxBuf_copy(COMM_BUF *CBuf_dst, COMM_BUF *CBuf_src)
{
    char ch;
    
    while (CheckRxBuf(CBuf_src)) {
        ch = GetCharRxBuf(CBuf_src); 
        PutCharRxBuf(CBuf_dst, ch);
    }
}	 


void Commbuf_Init()
{
    CommBufInit(&CommBufUart1,COMM_TYPE_UART1,0);
    CommBufInit(&CommBufUart3,COMM_TYPE_UART3,0);
//    memset(gmsg, 0, sizeof(gmsg));
}
//#pragma endoptimize
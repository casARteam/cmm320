#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "globals.h"
#include "stm32f4xx_gpio.h"
#include "buzzer.h"


BUZZER_STR Buzzer;

void buzzer_init(void)
{
    BUZZER_HIGH;
    Buzzer.Status = 0;
	Buzzer.Times = 0;
    
}

void Buzzer_On(INT8U cnt)
{
	Buzzer.Times = cnt;
		
}

void BuzOnAdd(INT8U cnt)
{
	Buzzer.Times += cnt;
}
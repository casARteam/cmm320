/*****************************************************************************
|*			
|*  Copyright		:	(c) 2002 CAS
|*  Filename		:	analyze.c
|*  Version			:	0.1
|*  Programmer(s)	:	Yun Yeo Chul (YYC)
|*  Created			:	2016/07/11
|*  Modify              :       
|*  Description		:	WC300 MOG System
|*				
****************************************************************************/


#include <stdio.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include "globals.h"
#include "serial.h"
#include "mode_main.h"
#include "commbuf.h"
#include "comm_app.h"
#include "RingBuf.h"
#include "timer.h"
#include "analyze.h"
#include "filter.h"
#include "key.h"

extern RING_BUF RawPhaseRingBuf;
extern RING_BUF *RawPhaseRingBufPtr;
extern RING_BUF TempRingBuf;
extern RING_BUF *TempRingBufPtr;
extern RING_BUF KeyRingBuf;
extern RING_BUF *KeyRingBufPtr;

INT64S rawPhaseData = 0;
INT32U temperature = 0;
INT32U LCD_Temp_data = 0;
FP64 FLData = 0;
//#ifdef USE_COMPENSATION
INT8U zeroTrackingEnterCount;
FP64 zeroBuf;
FP64 zeroTrackingRange;
FP64 rezeroLimit;

FP64 filteredData_1st;
FP64 filteredData_2nd;
FP64 filteredData_3rd;
FP64 filteredData_4th;
FP64 filteredData_5th;
FP64 filteredData_6th;
FP64 filteredData_7th; //insert for mg check test  yyc(2016.08.24)
FP64 filteredData_Last;
FP64 creepCompData;
FP64 creepTrackingData;
FP64 LinearCompData;
FP64 WeightData;

INT32U globalCounter = 0;

//initial value
// 20160718_CYCLE_1.csv
//FP64 C_max_a = 8000;//29500
//FP64 C_max_b = 9000;
//FP64 C_max_c = 50000;
//FP64 T_a_up = 3;
//FP64 T_b_up = 8;
//FP64 T_c_up = 30;
//FP64 T_a_down = 3*1.5;
//FP64 T_b_down = 8*2.5;
//FP64 T_c_down = 25*3.5;

//20160721_800g_com_check_100Mhz_CYCLE_MAGNET_2EA_1_temp
//FP64 C_max_a = 6000;//29500
//FP64 C_max_b = 6000;
//FP64 C_max_c = 85000;
//FP64 T_a_up = 5;
//FP64 T_b_up = 30;
//FP64 T_c_up = 50;
//FP64 T_a_down = 5*3;
//FP64 T_b_down = 20*2.5;
//FP64 T_c_down = 40*3.5;

#ifdef CAPA_1kg
//1kg CRISS 테스트 한 것
FP64 C_max_a = 5000;//29500
FP64 C_max_b = 9000;//12000;%9000;
FP64 C_max_c = 158000;//39000%50000;
FP64 T_a_up = 2;
FP64 T_b_up = 4;
FP64 T_c_up = 100;
FP64 T_a_down = 1*2;
FP64 T_b_down = 1*4;
FP64 T_c_down = 1*100;file:///C:/Users/micky/Downloads/201707244200002110659047.htmlzero

FP64 SampleingRate = 500;
FP64 t_delta_thres = 0.1;//sec
FP64 RawW_max = 1.7716082856510044e+10;//1kg
FP64 RawW_zero = 1.7244936192e+10;
FP64 RawW_500g = 1.7472876933735928e+10;
FP64 RawW_200g = 1.733432997157661e+10;
FP64 RawW_100g = 1.7289336475798176e+10;
FP64 RawW_50g = 1.7267063572222702e+10;
FP64 NormW_delta_thres = 1000;//10mg(1kg)
FP64 NormW_zero_trac_thres = 100;//100;//1mg(1kg)
FP64 Weight_fact = 0.1; //10^7/10^8 
FP64 Weight_div = 1;//1kg -> 1, 50g -> 5
FP64 auto_zero_range = 10000;//100mg

////value
FP64 CalW_max = 1.7716082856510044e+10;
FP64 CalW_zero = 1.7244936192e+10;
FP64 CalW_500g = 1.7472876933735928e+10;
FP64 CalW_200g = 1.733432997157661e+10;
FP64 CalW_100g = 1.7289336475798176e+10;
FP64 CalW_50g = 1.7267063572222702e+10;

//1kg
FP64 NormW_linear_50g = 0;
FP64 NormW_linear_100g = 0;
FP64 NormW_linear_200g = 0;
FP64 NormW_linear_500g = 0;

#elif defined(CAPA_50g)
//50g
//FP64 CalW_max = 1.7330541632090866e+10;//50g
//FP64 CalW_max = 1.729176354040766e+10; //insert for test 20160825
//FP64 CalW_zero = 1.7245120691251213e+10;
//FP64 CalW_zero = 1.7244930687705166e+10;//insert for test 20160825

//FP64 CalW_max = 8.665277652367820e+09;//50g
//FP64 CalW_zero = 8.622592204989173e+09;//insert for test 20160825

volatile FP64 CalW_max = 8.665277652367820E9;//50g
volatile FP64 CalW_zero = 8.622592204989173E9;//insert for test 20160825
FP64 DctFilterOut = 8.622592204989173E9;//20160901 IAN TEST

FP64 CalW_20g =  1.7279071836543312e+10;
FP64 CalW_10g =  1.726206109810854e+10;
FP64 CalW_5g = 1.7253582106058212e+10;
FP64 CalW_1g = 1.7246812163633377e+10;
//#ifdef MG_CHECK_FILTER
//FP64 C_max_a = 100;
//FP64 C_max_b = 10000;
//FP64 C_max_c = 100000;
//FP64 T_a_up = 1;
//FP64 T_b_up = 10;
//FP64 T_c_up = 600;
//FP64 T_a_down = 5*3;
//FP64 T_b_down = 20*2.5;
//FP64 T_c_down = 40*3.5;
//#else
//50g CRISS 테스트 한 것
FP64 C_max_a = 10000;
FP64 C_max_b = 35000;
FP64 C_max_c = 150000;
FP64 T_a_up = 3;
FP64 T_b_up = 10;
FP64 T_c_up = 200;
FP64 T_a_down = 5*3;
FP64 T_b_down = 20*2.5;
FP64 T_c_down = 40*3.5;
//#endif
FP64 SampleingRate = 500;
FP64 t_delta_thres = 0.1;//sec

FP64 NormW_delta_thres = 1000;//10mg(1kg)
FP64 NormW_zero_trac_thres = 100000;//0.5mg(50g) // duckspg 
FP64 Weight_fact = 0.1; //10^7/10^8 
FP64 Weight_div = 5;//1kg -> 1, 50g -> 5
FP64 auto_zero_range = 20000;//50g -> 10mg

//value
FP64 RawW_max = 0;
FP64 RawW_zero = 0;
FP64 RawW_20g = 0;
FP64 RawW_10g = 0;
FP64 RawW_5g = 0;
FP64 RawW_1g = 0;

FP64 NormW_linear_1g = 0;
FP64 NormW_linear_5g = 0;
FP64 NormW_linear_10g = 0;
FP64 NormW_linear_20g = 0;

#endif

FP64 C_curr = 0;
FP64 C_curr_a = 0;
FP64 C_curr_b = 0;
FP64 C_curr_c = 0;
FP64 C_acc = 0;
FP64 C_acc_a = 0;
FP64 C_acc_b = 0;
FP64 C_acc_c = 0;
FP64 C_max = 0;
FP64 T_a = 0;
FP64 T_b = 0;
FP64 T_c = 0;
FP64 t_curr = 0;
FP64 RawW_curr = 0;
FP64 Cal_fact = 0;
FP64 NormW_max = 0;//1kg -> Norm value
FP64 NormW_zero = 0;//0kg -> Norm value
//50g

FP64 Linearity_on_flag = 0;
FP64 ZeroTracking_on_flag = 0;
FP64 AutoZero_on_flag = 0;
FP64 ZeroKey_on_flag = 0;
FP64 ResetCreep_on_flag = 0;

FP64 WeightUpperDataPoint = 0;
FP64 WeightUpperData = 0;
FP64 WeightLowerData = 0;
FP64 WeightDataCompValue = 0;
FP64 WeightCompData = 0;

 FP64 NormW_zero_tracking = 0;
#ifdef MG_CHECK_FILTER 
    //=================================duckspg 
 
FP64 lip1 =   7.188e-17;
FP64 lip2 =    -5.355e-09;
FP64 lip3 =       1.092;
FP64 lip4 =       0;
//=================1g~5g 
FP64 lip5_1 =   1.793e-13;
FP64 lip5_2 =    -1.184e-06;
FP64 lip5_3 =       2.682;
FP64 lip5_4 =       0;

//=============20mg~500mg
FP64 mglip1 =   5.161e-13;       //duckspg3
FP64 mglip2 =    -3.173e-07;       //duckspg3
FP64 mglip3 =       1.23;       //duckspg3
FP64 mglip4 =       0;       //duckspg3
        
//============= 1mg ~ 10mg
//FP64 mg10lip1 =   4.339e-08;       //duckspg3
//FP64 mg10lip2 =    -0.0005472;       //duckspg3
//FP64 mg10lip3 =       2.834;       //duckspg3
//FP64 mg10lip4 =       0;       //duckspg3
FP64 mg10lip1 =  1.429e-07;       //duckspg5
FP64 mg10lip2 =  -0.001378;       //duckspg5
FP64 mg10lip3 =  4.521;       //duckspg5
FP64 mg10lip4 =  0;          //duckspg5


FP64 LinFitNormW = 0;
FP64 Linearity_fitting_flag = 1;
                
FP64 differ_indi_flag = 0;
//FP64 differ_delta = 1; // 기울기 값
FP64 differ_delta = 0.5; // 기울기 값  0.8
FP64 mg_differ_delta_max = 1.5;

FP64 differ_curr = 0;
FP64 differ_prev = 0;
FP64 differ_value = 0;
                
FP64 differ_conti = 0;
FP64 differ_conti_lenght = DIFFER_X_DELTA*DIFFER_X_DELTA_LENGTH_UP;
FP64 mdiffer_conti = 0;
FP64 mdiffer_conti_lenght = DIFFER_X_DELTA*DIFFER_X_DELTA_LENGTH_DN;

FP64 differ_down = 0;        // duckspg 5 내려가는 기울기시 zero로 보낼지 현상을 유지할지 결정
FP64 differ_down_length = 200; // duckspg 5   
FP64 differ_down_check = 5000; // duckspg 5    이값이하는 내려가는거 따지지 않고 그냥 zero

FP64 mg_ZeroTracking_on_flag = 1;
FP64 mg_MormW_zero_tracking = 0;
FP64 mg_check_threshold = 2.020181812000657e+10; //init 단계에서 RawW_zero를 꼭 빼줘야함.
FP64 NormW_zero_trac_thres_rate = 100;
FP64 NormW_zero_trac_thres_min = 10;
FP64 zero_trac_point_flag = 0;
FP64 test_point_disp_value = 0;
FP64 autoZeroFlag = 0;
FP64 mg_weight_check = 0;
FP64 New_mg_weight_check = 0;
FP64 old_mg_weight_check = 0;
FP64 mg_weight_check_cnt = 0;
FP64 modify_mg_display_flag = 1;
                //===============================duckspg end
#endif



//1kg
#ifdef CAPA_1kg
void AnalyzeInit(void)
{
//#ifdef USE_COMPENSATION
	zeroTrackingEnterCount = (500*5)/10;
	zeroBuf = 0;
	zeroTrackingRange = 0;
	rezeroLimit = 0;

	RawW_zero = CalW_zero;
	RawW_50g = CalW_50g;
	RawW_100g = CalW_100g;
	RawW_200g = CalW_200g;
	RawW_500g = CalW_500g;
	RawW_max = CalW_max;
//value 1,5,10,20,50
	C_max = C_max_a + C_max_b + C_max_c;
	Cal_fact = 100000000/(RawW_max - RawW_zero);
	NormW_max = RawW_max * Cal_fact;//1kg -> Norm value
	NormW_zero = RawW_zero * Cal_fact;//0kg -> Norm value
	NormW_linear_50g = (RawW_50g - RawW_zero) * Cal_fact;
	NormW_linear_100g = (RawW_100g - RawW_zero) * Cal_fact;
	NormW_linear_200g = (RawW_200g - RawW_zero) * Cal_fact;
	NormW_linear_500g = (RawW_500g - RawW_zero) * Cal_fact;
//#endif
}
void AnalyzeReInit(void)
{

//	RawW_50g = RawW_zero+(CalW_50g-CalW_zero)*(RawW_max-RawW_zero)/(CalW_max-CalW_zero);
//	RawW_100g = RawW_zero+(CalW_100g-CalW_zero)*(RawW_max-RawW_zero)/(CalW_max-CalW_zero);
//	RawW_200g = RawW_zero+(CalW_200g-CalW_zero)*(RawW_max-RawW_zero)/(CalW_max-CalW_zero);
//	RawW_500g = RawW_zero+(CalW_500g-CalW_zero)*(RawW_max-RawW_zero)/(CalW_max-CalW_zero);
	RawW_50g = CalW_50g;
	RawW_100g = CalW_100g;
	RawW_200g = CalW_200g;
	RawW_500g = CalW_500g;
	RawW_max = CalW_max;
	//RawW_zero = CalW_zero;
//value
	C_max = C_max_a + C_max_b + C_max_c;
	Cal_fact = 100000000/(RawW_max - RawW_zero);
	NormW_max = RawW_max * Cal_fact;//1kg -> Norm value
	//NormW_zero = RawW_zero * Cal_fact;//0kg -> Norm value
	NormW_linear_50g = (RawW_50g - RawW_zero) * Cal_fact;
	NormW_linear_100g = (RawW_100g - RawW_zero) * Cal_fact;
	NormW_linear_200g = (RawW_200g - RawW_zero) * Cal_fact;
	NormW_linear_500g = (RawW_500g - RawW_zero) * Cal_fact;
}
#elif defined(CAPA_50g)
//50g
void AnalyzeInit(void)
{
//	zeroTrackingEnterCount = (500*5)/10;
//	zeroBuf = 0;
//	zeroTrackingRange = 0;
//	rezeroLimit = 0;

	RawW_zero = CalW_zero;
	RawW_1g = CalW_1g;
	RawW_5g = CalW_5g;
	RawW_10g = CalW_10g;
	RawW_20g = CalW_20g;
	RawW_max = CalW_max;
//value 1,5,10,20,50
	C_max = C_max_a + C_max_b + C_max_c;
	Cal_fact = 100000000/(RawW_max - RawW_zero);
	NormW_max = RawW_max * Cal_fact;//1kg -> Norm value
	NormW_zero = RawW_zero * Cal_fact;//0kg -> Norm value
    
	NormW_linear_1g = (RawW_1g - RawW_zero) * Cal_fact;
	NormW_linear_5g = (RawW_5g - RawW_zero) * Cal_fact;
	NormW_linear_10g = (RawW_10g - RawW_zero) * Cal_fact;
	NormW_linear_20g = (RawW_20g - RawW_zero) * Cal_fact;
#ifdef MG_CHECK_FILTER
    mg_check_threshold = mg_check_threshold - RawW_zero;//RawW_zero 는 Creep보상을 켠 상태에서의 값을 넣어야 한다.
#endif
}
//50g
void AnalyzeReInit(void)
{
	RawW_1g = CalW_1g;
	RawW_5g = CalW_5g;
	RawW_10g = CalW_10g;
	RawW_20g = CalW_20g;
	RawW_max = CalW_max;
	RawW_zero = CalW_zero;
//value
	C_max = C_max_a + C_max_b + C_max_c;
	Cal_fact = 100000000/(RawW_max - RawW_zero);
	NormW_max = RawW_max * Cal_fact;//1kg -> Norm value
	//NormW_zero = RawW_zero * Cal_fact;//0kg -> Norm value
	NormW_linear_1g = (RawW_1g - RawW_zero) * Cal_fact;
	NormW_linear_5g = (RawW_5g - RawW_zero) * Cal_fact;
	NormW_linear_10g = (RawW_10g - RawW_zero) * Cal_fact;
	NormW_linear_20g = (RawW_20g - RawW_zero) * Cal_fact;
    #ifdef MG_CHECK_FILTER
    mg_check_threshold = mg_check_threshold - RawW_zero;//RawW_zero 는 Creep보상을 켠 상태에서의 값을 넣어야 한다.
#endif
}
#endif

FP64 PhaseToDisplacement(INT64S indata)
{
	FP64 displacement = 0;
	
	displacement =(FP64)indata/DISP_SCALE_FACTOR;
	
	return displacement;
}

FP64 CreepCompProc(FP64 inData)
{
	INT16U i;
	INT32U bufIndex;
	INT32U currIndex;
	INT32U mafWinSize;

	FP64 NormW_curr;
	FP64 NormW_prev;
	FP64 NormW_delta;
	//FP64 CorrectedNormW;
	
	currIndex = MafCreepOrder.index;
	mafWinSize = MafCreepOrder.winSize;
	
	if(MafCreepOrder.mafOnFlag == OFF)
	{
		if (!inData) return inData;
		if (currIndex == mafWinSize) MafCreepOrder.mafOnFlag = ON;
		MafCreepOrder.buf[currIndex] = inData;
		MafCreepOrder.index++;
		if (MafCreepOrder.mafOnFlag != ON)
		{
			return 0;
		}
		else
		{
			return inData;
		}
	}

	MafCreepOrder.buf[currIndex] = inData;
	MafCreepOrder.index++;
	MafCreepOrder.index = MafCreepOrder.index % MAF_BUF_MAX_SIZE;


	NormW_curr = inData * Cal_fact;
	bufIndex = (currIndex + MAF_BUF_MAX_SIZE - (INT32U)(t_delta_thres*SampleingRate)) % MAF_BUF_MAX_SIZE;
	NormW_prev = MafCreepOrder.buf[bufIndex] * Cal_fact;
	
    NormW_delta = NormW_curr - NormW_prev;
    if (NormW_delta > NormW_delta_thres)
	{
        C_acc_a = C_curr_a;
        C_acc_b = C_curr_b;
        C_acc_c = C_curr_c;
        t_curr = 0.002;//= 1/SampleingRate, 500Hz 
        T_a = T_a_up;
        T_b = T_b_up;
        T_c = T_c_up;
	}
	else if (NormW_delta < -NormW_delta_thres)
	{
        C_acc_a = C_curr_a;
        C_acc_b = C_curr_b;
        C_acc_c = C_curr_c;
        t_curr = 0.002;//= 1/SampleingRate, 500Hz
        T_a = T_a_down;
        T_b = T_b_down;
        T_c = T_c_down;
	}
    else
	{
        t_curr = t_curr + 1/SampleingRate;
	}
        
    C_curr_a = ((((NormW_curr - NormW_zero)/100000000) * C_max_a)-C_acc_a)*(1-exp(-t_curr/T_a))+C_acc_a;
    C_curr_b = ((((NormW_curr - NormW_zero)/100000000) * C_max_b)-C_acc_b)*(1-exp(-t_curr/T_b))+C_acc_b;
    C_curr_c = ((((NormW_curr - NormW_zero)/100000000) * C_max_c)-C_acc_c)*(1-exp(-t_curr/T_c))+C_acc_c;
    C_curr = C_curr_a + C_curr_b + C_curr_c;
    
    //Reset Creep Comp
	if(ResetCreep_on_flag)
	{
        C_curr = 0;
        C_curr_a = 0;
        C_curr_b = 0;
        C_curr_c = 0;
        C_acc = 0;
        C_acc_a = 0;
        C_acc_b = 0;
        C_acc_c = 0;
        ResetCreep_on_flag = 0;
    }
	return NormW_curr - C_curr;
}
#ifdef MG_CHECK_FILTER
FP64 DC_Tracking_Filter(FP64 inData)
{
	INT32U currIndex;
	INT32U mafWinSize;

    FP64 calcBZero = 0;
    FP64 calcAOne = 0;

	currIndex = MafDCTrackingOrder.index;
	mafWinSize = MafDCTrackingOrder.winSize;
	
    calcBZero = B_ZERO * inData;
    calcAOne = A_ONE * DctFilterOut;
    DctFilterOut = (calcBZero + calcAOne)/Q_FIFTEEN;

	if(MafDCTrackingOrder.mafOnFlag == OFF)
	{
		if (currIndex == mafWinSize) MafDCTrackingOrder.mafOnFlag = ON;
		MafDCTrackingOrder.buf[currIndex] = DctFilterOut;
		MafDCTrackingOrder.index++;
		return;
	}

	MafDCTrackingOrder.buf[currIndex] = DctFilterOut;
	MafDCTrackingOrder.index++;
	MafDCTrackingOrder.index = MafDCTrackingOrder.index % MAF_BUF_MAX_SIZE;

}

void SlopeDiffMeasureProc()
{
    INT32U currIndex = 0;
    INT32U prevIndex = 0;
    static INT8U DiffCheckStatus = 0;


    if(MafCreepOrder.mafOnFlag == OFF) return;

    currIndex = MafCreepOrder.index;
    currIndex = (currIndex + MAF_BUF_MAX_SIZE - 1) % MAF_BUF_MAX_SIZE;

    differ_curr = MafCreepOrder.buf[currIndex];
    prevIndex = (currIndex + MAF_BUF_MAX_SIZE - DIFFER_X_DELTA) % MAF_BUF_MAX_SIZE;

    differ_prev = MafCreepOrder.buf[prevIndex];

    differ_value = (differ_curr - differ_prev)/DIFFER_X_DELTA;
    
    if (differ_value > differ_delta)
    {
        differ_conti = differ_conti + 1;

        if(differ_conti >= differ_conti_lenght)
        {
            differ_conti = differ_conti_lenght;
            mg_weight_check = mg_weight_check + differ_value;
        }
    }
    else
    {
        differ_conti = 0;
        DiffCheckStatus = SLOPE_DOWN_CHECK_STATUS;
    }


    currIndex = MafDCTrackingOrder.index;
    currIndex = (currIndex + MAF_BUF_MAX_SIZE - 1) % MAF_BUF_MAX_SIZE;

    differ_curr = MafDCTrackingOrder.buf[currIndex];
    prevIndex = (currIndex + MAF_BUF_MAX_SIZE - DIFFER_X_DELTA) % MAF_BUF_MAX_SIZE;

    differ_prev = MafDCTrackingOrder.buf[prevIndex];

    differ_value = (differ_curr - differ_prev)/DIFFER_X_DELTA;

    if (differ_value < -differ_delta)
    {
        mdiffer_conti = mdiffer_conti + 1;
        
        if (mdiffer_conti >= mdiffer_conti_lenght)
        {
            //                mdiffer_conti = mdiffer_conti_lenght;
            
            if ((mg_weight_check < differ_down_check) && (mg_weight_check !=0))
            {
                mg_weight_check = 0;
            }
            else
            {
                differ_down = differ_down + 1;
                //                if (differ_down > differ_down_length)
                if ((mg_weight_check > differ_down_check) && (mg_weight_check !=0))
                {
                    differ_down = differ_down_length;
                    mg_weight_check = 0;
                }
            }
        }
    }
    else
    {
        differ_down = 0;
        mdiffer_conti = 0;
    }
 }
/*
void SlopeDiffMeasureProc(FP64 inData)
{
    INT32U currIndex = 0;
    INT32U prevIndex = 0;
    
    if(MafCreepOrder.mafOnFlag == OFF) return;
    
    currIndex = MafCreepOrder.index;
    currIndex = (currIndex + MAF_BUF_MAX_SIZE - 1) % MAF_BUF_MAX_SIZE;
    
      //=========duckspg_differ=========================
    differ_curr = MafCreepOrder.buf[currIndex];
    //differ_prev = mean_data_creep_input(i-differ_x_delta);
    prevIndex = (currIndex + MAF_BUF_MAX_SIZE - DIFFER_X_DELTA) % MAF_BUF_MAX_SIZE;
    
    differ_prev = MafCreepOrder.buf[prevIndex];
             
    differ_value = (differ_curr - differ_prev)/DIFFER_X_DELTA;
    
    
    if (differ_value > differ_delta)
    {
        differ_conti = differ_conti + 1;
        
        if(differ_conti >= differ_conti_lenght)
        {
            differ_conti = differ_conti_lenght;
            mg_weight_check = mg_weight_check + differ_value;
        }
    }
    else
    {
        differ_conti = 0;
    }
    if (differ_value < -differ_delta)
    {
        mdiffer_conti = mdiffer_conti + 1;
        
        if (mdiffer_conti >= mdiffer_conti_lenght)
        {
            mdiffer_conti = mdiffer_conti_lenght;
            
            if (mg_weight_check < differ_down_check)
            {
                mg_weight_check = 0;
            }
            else
            {
                differ_down = differ_down + 1;
                if (differ_down > differ_down_length)
                {
                    differ_down = differ_down_length;
                    mg_weight_check = 0;
                }
            }
        }
    }
    else
    {
        differ_down = 0;
        mdiffer_conti = 0;
    }
    
    //======================== duckspg end   
}
*/
FP64 GetMgWeightCheck(void)
{
    return mg_weight_check;
}
#endif
FP64 ZeroCreepTrackingProc(FP64 inData)
{
	INT16U i;
	INT32U bufIndex;
	INT32U currIndex;
	INT32U mafWinSize;
	
	FP64 NormW_curr;
	FP64 NormW_prev;
	FP64 NormW_delta;	
    FP64 test = 0;
//	static FP64 NormW_zero_tracking = 0;

	currIndex = MafCreepTrackingOrder.index;
	mafWinSize = MafCreepTrackingOrder.winSize;
	
	if(MafCreepTrackingOrder.mafOnFlag == OFF)
	{
		if (!inData) return inData;
		if (currIndex == mafWinSize) MafCreepTrackingOrder.mafOnFlag = ON;
		MafCreepTrackingOrder.buf[currIndex] = inData;
		MafCreepTrackingOrder.index++;
		if (MafCreepTrackingOrder.mafOnFlag != ON)
		{
			return 0;
		}
		else
		{
			return inData;
		}
	}

	MafCreepTrackingOrder.buf[currIndex] = inData;
	MafCreepTrackingOrder.index++;
	MafCreepTrackingOrder.index = MafCreepTrackingOrder.index % MAF_BUF_MAX_SIZE;

    if(ZeroTracking_on_flag)
	{
		NormW_curr = inData;
		bufIndex = (currIndex + MAF_BUF_MAX_SIZE - (INT32U)(t_delta_thres*SampleingRate)) % MAF_BUF_MAX_SIZE;
		NormW_prev = MafCreepTrackingOrder.buf[bufIndex];
		NormW_delta = NormW_curr - NormW_prev;
		
		if ((NormW_delta < NormW_zero_trac_thres) && (NormW_delta > -NormW_zero_trac_thres))
		{
            NormW_zero_tracking = NormW_zero_tracking + (NormW_delta / (t_delta_thres*SampleingRate));
        }
	}
    else
	{
        NormW_zero_tracking = 0;
	}
	
    //Zero Key Proc 
    if(ZeroKey_on_flag)
	{
		NormW_zero = inData;
        NormW_zero_tracking = 0;
        ZeroKey_on_flag = 0;
#ifdef MG_CHECK_FILTER
        differ_conti = 0;  //duckspg2
        mg_weight_check = 0; //duckspg2
#endif
    }
	//
	
	//Auto Zero Proc
    if(AutoZero_on_flag)
	{
        test = inData - NormW_zero - NormW_zero_tracking;
        if (auto_zero_range > test)
//        if (auto_zero_range > inData - NormW_zero - NormW_zero_tracking)
		{
			if ((NormW_delta < NormW_zero_trac_thres) && (NormW_delta > -NormW_zero_trac_thres))				
			{
                NormW_zero = inData;
                NormW_zero_tracking = 0;
#ifdef MG_CHECK_FILTER
                autoZeroFlag = 1;
#endif
            }
#ifdef MG_CHECK_FILTER
            else //insert for mg check test filter 20160824 YYC
            {
                autoZeroFlag = 0;
            }
#endif
        }
    }		

	return inData - NormW_zero - NormW_zero_tracking;
}
//1kg
#ifdef CAPA_1kg
FP64 LinearCompProc(FP64 inData)
{
	FP64 NormW_linear_err;
	FP64 NormW_zeroed;
	
    if(Linearity_on_flag)
	{
        //NormW_zeroed = CorrectedNormW - NormW_zero - NormW_zero_tracking;
		NormW_zeroed = inData;
		
        if (NormW_zeroed <= 0)
		{
            NormW_linear_err = 0;
		}
        else if (NormW_zeroed > 0 && NormW_zeroed <= NormW_linear_50g) //50g
		{
            NormW_linear_err = (5000000 - NormW_linear_50g) * (NormW_zeroed / NormW_linear_50g);
		}		
        else if (NormW_zeroed > NormW_linear_50g && NormW_zeroed <= NormW_linear_100g) //100g
		{
            NormW_linear_err = 10000000 - NormW_linear_100g - (5000000 - NormW_linear_50g);
            NormW_linear_err = NormW_linear_err * ((NormW_zeroed - NormW_linear_50g) / (NormW_linear_100g - NormW_linear_50g));
            NormW_linear_err += (5000000 - NormW_linear_50g);
		}
        else if (NormW_zeroed > NormW_linear_100g && NormW_zeroed <= NormW_linear_200g) //200g
		{
            NormW_linear_err = 20000000 - NormW_linear_200g - (10000000 - NormW_linear_100g);
            NormW_linear_err = NormW_linear_err * ((NormW_zeroed - NormW_linear_100g) / (NormW_linear_200g - NormW_linear_100g));
            NormW_linear_err += (10000000 - NormW_linear_100g); 
		}
        else if (NormW_zeroed > NormW_linear_200g && NormW_zeroed <= NormW_linear_500g) //500g
		{
            NormW_linear_err = 50000000 - NormW_linear_500g - (20000000 - NormW_linear_200g);
            NormW_linear_err = NormW_linear_err * ((NormW_zeroed - NormW_linear_200g) / (NormW_linear_500g - NormW_linear_200g));
            NormW_linear_err += (20000000 - NormW_linear_200g);  
		}
        else if (NormW_zeroed > NormW_linear_500g && NormW_zeroed <= 100000000) //500g
		{
            NormW_linear_err = - (50000000 - NormW_linear_500g);
            NormW_linear_err = NormW_linear_err * ((NormW_zeroed - NormW_linear_500g) / (100000000 - NormW_linear_500g));
            NormW_linear_err += (50000000 - NormW_linear_500g);  
        }
		else
		{
            NormW_linear_err = 0;
        }
        //CorrectedNormW = CorrectedNormW + NormW_linear_err;
		//return CorrectedNormW;
		
		return inData + NormW_linear_err;
    }
	else
	{
		return inData;
	}
}
#elif defined(CAPA_50g)
//50g
#ifdef MG_CHECK_FILTER
FP64 LinearCompProc(FP64 inData)
{

    return inData;
}
#else
FP64 LinearCompProc(FP64 inData)
{
	FP64 NormW_linear_err;
	FP64 NormW_zeroed;
	
    if(Linearity_on_flag)
	{
        //NormW_zeroed = CorrectedNormW - NormW_zero - NormW_zero_tracking;
		NormW_zeroed = inData;
		
        if (NormW_zeroed <= 0)
		{
            NormW_linear_err = 0;
		}
        else if (NormW_zeroed > 0 && NormW_zeroed <= NormW_linear_1g) //1g = 2000000
		{
            NormW_linear_err = (2000000 - NormW_linear_1g) * (NormW_zeroed / NormW_linear_1g);
		}		
        else if (NormW_zeroed > NormW_linear_1g && NormW_zeroed <= NormW_linear_5g) //5g = 10000000
		{
            NormW_linear_err = 10000000 - NormW_linear_5g - (2000000 - NormW_linear_1g);
            NormW_linear_err = NormW_linear_err * ((NormW_zeroed - NormW_linear_1g) / (NormW_linear_5g - NormW_linear_1g));
            NormW_linear_err += (2000000 - NormW_linear_1g);
		}
        else if (NormW_zeroed > NormW_linear_5g && NormW_zeroed <= NormW_linear_10g) //10g = 20000000
		{
            NormW_linear_err = 20000000 - NormW_linear_10g - (10000000 - NormW_linear_5g);
            NormW_linear_err = NormW_linear_err * ((NormW_zeroed - NormW_linear_5g) / (NormW_linear_10g - NormW_linear_5g));
            NormW_linear_err += (10000000 - NormW_linear_5g); 
		}
        else if (NormW_zeroed > NormW_linear_10g && NormW_zeroed <= NormW_linear_20g) //20g = 40000000
		{
            NormW_linear_err = 40000000 - NormW_linear_20g - (20000000 - NormW_linear_10g);
            NormW_linear_err = NormW_linear_err * ((NormW_zeroed - NormW_linear_10g) / (NormW_linear_20g - NormW_linear_10g));
            NormW_linear_err += (20000000 - NormW_linear_10g);  
		}
        else if (NormW_zeroed > NormW_linear_20g && NormW_zeroed <= 100000000) //50g = 100000000
		{
            NormW_linear_err = - (40000000 - NormW_linear_20g);
            NormW_linear_err = NormW_linear_err * ((NormW_zeroed - NormW_linear_20g) / (100000000 - NormW_linear_20g));
            NormW_linear_err += (40000000 - NormW_linear_20g);  
        }
		else
		{
            NormW_linear_err = 0;
        }
        //CorrectedNormW = CorrectedNormW + NormW_linear_err;
		//return CorrectedNormW;
		
		return inData + NormW_linear_err;
    }
	else
	{
		return inData;
	}

}
#endif
#endif
FP64 WeightGetData(FP64 inData)
{
	FP64 weight;
    FP64 ld;
	
	weight = Weight_fact * inData * Weight_div;	
    
	return weight;
}

FP64 WeightGetDataDiv(FP64 inData)
{
    FP64 ld;
	long wdata;

    inData = inData + 0.5*Weight_div;
    wdata = (long)(inData / Weight_div);
    wdata = wdata * Weight_div;
            
	return wdata;
}
#ifdef MG_CHECK_FILTER
FP64 SlopeCompProc(FP64 inData)
{
     //=====================duckspg 
    FP64 inData3thPow;
    FP64 inData2ndPow;
    FP64 NewMgWCheck3thPow;
    FP64 NewMgWCheck2ndPow;
    static INT8U mg_check_flag = 0; // duckspg 
    static INT32U mg_down_display=0;
    
    inData3thPow = pow(inData,3);
    inData2ndPow = pow(inData,2);

    NewMgWCheck3thPow = pow(New_mg_weight_check,3);
    NewMgWCheck2ndPow = pow(New_mg_weight_check,2);
    
    
    Linearity_fitting_flag = 1;
    if(Linearity_fitting_flag)
    {
        if (LinFitNormW>7500000)
        {
            LinFitNormW = lip1*inData3thPow + lip2*inData2ndPow + lip3*inData + lip4;
        }
        else
        {
            LinFitNormW = lip5_1*inData3thPow + lip5_2*inData2ndPow + lip5_3*inData + lip5_4;
        }
    }
    
    if (modify_mg_display_flag)
    {
        if (autoZeroFlag)
        {
            if (mg_weight_check > 0)
            {
                mg_check_flag = 1;
                New_mg_weight_check = mg_weight_check;
                LinFitNormW = mg_weight_check; //20160901 ian test
//                if (mg_weight_check > 12000)
//                {
//                    LinFitNormW = mglip1*NewMgWCheck3thPow + mglip2*NewMgWCheck2ndPow + mglip3*New_mg_weight_check + mglip4;
//                }
//                else
//                {
//                    LinFitNormW = mg10lip1*NewMgWCheck3thPow + mg10lip2*NewMgWCheck2ndPow + mg10lip3*New_mg_weight_check;
//                }
                mg_down_display = (INT32U)(LinFitNormW);
            }
            else
            {
                if (mg_check_flag == 1)
                {
                    mg_down_display = mg_down_display / 10;
                    LinFitNormW = mg_down_display;
                    if (mg_down_display < 10)
                    {
                        mg_check_flag = 0;
                    }
                }
            }
            
            
            
            
            
            old_mg_weight_check = New_mg_weight_check;
        }
    }
    return LinFitNormW;
}

            //======================== duckspg end
#endif   

#ifdef CAPA_1kg
//#define WEIGHT_COMP_RATE 0.90//전체 보정량 90% 1kg
//#define WEIGHT_COMP_RANGE 10000 //1kg에서 1g단위 CRISS
#elif defined(CAPA_50g)
#define WEIGHT_COMP_RATE 0.97//전체 보정량 95% 50g
#define WEIGHT_COMP_RANGE 50000 //50g에서 0.05g CRISS
#endif
#define WEIGHT_COMP_RATE2 0.01//한번에 보정하는 양 1%(남은 보정값 중)

FP64 WeightCompProc(FP64 inData)
{
    long wdata;
    
    wdata = (long)inData;
    WeightLowerData = fmodf(wdata, WEIGHT_COMP_RANGE);//mod() 나머지 연산 50g 50000
    WeightUpperData = wdata - WeightLowerData;
    if (WeightUpperData == WeightUpperDataPoint)
    {
        if (WeightLowerData < WEIGHT_COMP_RANGE/2) //50g -> 25000
        {
            if(WeightDataCompValue != (WEIGHT_COMP_RATE*WeightLowerData))// 50% 보정
            {
                WeightDataCompValue = WeightDataCompValue - WEIGHT_COMP_RATE2*(WeightDataCompValue-WEIGHT_COMP_RATE*WeightLowerData);// 한번에 오차 보정 오차의 0.5% 보정
            }
        }
        else
        {
            WeightLowerData = WeightLowerData - WEIGHT_COMP_RANGE;//50g -> 50000
            if(WeightDataCompValue != (WEIGHT_COMP_RATE*WeightLowerData))
            {
                WeightDataCompValue = WeightDataCompValue - WEIGHT_COMP_RATE2*(WeightDataCompValue-WEIGHT_COMP_RATE*WeightLowerData);
            }
        }
    }
    else
    {
        WeightUpperDataPoint = WeightUpperData;
    }
    
    return inData - WeightDataCompValue;
}
    
    
INT8U CalModeKey = 0;

void Calibration_Mode(INT8U inKey,FP64 cTrackingData, FP64 CalFator)
{
	FP64 calData;
	static INT8U calTime = 1;
	calData = (cTrackingData/CalFator) + RawW_zero;
	if(TimerCheckDelay100ms(TIMER_DELAY_100MS_STOPWATCH))
	{
		DispTimer(calTime++);
		if(calTime == 4)
		{
			TimerSetDelay100ms(TIMER_DELAY_100MS_STOPWATCH,0);
			DispTimer(0);
			calTime = 1;
		}
		else
		{
			TimerSetDelay100ms(TIMER_DELAY_100MS_STOPWATCH,10);
		}
	}
	if(TimerCheckDelay100ms(TIMER_DELAY_100MS_CAL_TIME))
	{
		TimerSetDelay100ms(TIMER_DELAY_100MS_STOPWATCH,0);
#ifdef CAPA_1kg		
		switch(CalModeKey)
		{
			case KC_ZERO:
				CalW_zero = filteredData_Last;
                AnalyzeReInit();
				break;
			case KC_50:
				CalW_50g = calData;//1kg
				break;
			case KC_100:
				CalW_100g = calData;//1kg
				break;
			case KC_200:
				CalW_200g = calData;//1kg
				break;
			case KC_500:
				CalW_500g = calData;//1kg
				break;
			case KC_MAX:
				CalW_max = calData;
				break;
		}
#elif defined(CAPA_50g)
		switch(CalModeKey)
		{
			case KC_ZERO:
				CalW_zero = filteredData_Last;
                AnalyzeReInit();
				break;
			case KC_50:
                CalW_1g = calData;
				break;
			case KC_100:
                CalW_5g = calData;
				break;
			case KC_200:
                CalW_10g = calData;
				break;
			case KC_500:
                CalW_20g = calData;
				break;
			case KC_MAX:
				CalW_max = calData;
				break;
		}        
#endif
		DispCalComplete(CalModeKey);
		CalModeKey = 0;
	}
	if(TimerCheckDelay10ms(TIMER_DELAY_10MS_CAL_DIGIT))
	{
		TimerSetDelay10ms(TIMER_DELAY_10MS_CAL_DIGIT,0);
		updateCalValueDisplay((INT64S)WeightData);
		TimerSetDelay10ms(TIMER_DELAY_10MS_CAL_DIGIT,15);
	}
	
}

INT8U CalModeFlag = 0;
void AnalyzeProc(void)
{
	FP64 rawDispData = 0;
	INT8U key;

	key = getKey();

	switch(key)
	{
        default: break;
        case KD_CAL:
                CalModeFlag = 1;
                DispMode(CAL_MODE);
                TimerSetDelay10ms(TIMER_DELAY_10MS_CAL_DIGIT, 100);
                TimerSetDelay10ms(TIMER_DELAY_10MS_SEND_LCD, 0);
			break;
            
        case KC_CAL:
                TimerSetDelay10ms(TIMER_DELAY_10MS_CAL_DIGIT,0);
                CalModeFlag = 0;
                DispMode(NORM_MODE);
                AnalyzeReInit();
                TimerSetDelay10ms(TIMER_DELAY_10MS_SEND_LCD, 100);
			break;
            
		case KD_ZERO:
			ZeroKey_on_flag = 1;
			break;
		case KD_FUNC:
			ResetCreep_on_flag = 1;
			break;
		
		case KC_ZERO:
		case KC_50:
		case KC_100:
		case KC_200:
		case KC_500:
		case KC_MAX:
			CalModeKey = key;
			TimerSetDelay100ms(TIMER_DELAY_100MS_CAL_TIME, 30);
			TimerSetDelay100ms(TIMER_DELAY_100MS_STOPWATCH,10);
			break;
	
	}
	
	
	if (RingBufCheckBuf((RING_BUF *)RawPhaseRingBufPtr))
	{
        FPGA_STATUS_LOW;
		if (RingBufCheckBuf((RING_BUF *)TempRingBufPtr))
		{
			temperature =	RingBufGetInt32u((RING_BUF *)TempRingBufPtr);
			LCD_Temp_data = temperature;
            TEMP_STATUS_LOW;
		}
		rawPhaseData =	RingBufGetInt64s((RING_BUF *)RawPhaseRingBufPtr);
//		flData = WC300_KalmanFilter_1st(rawPhaseData);
//		flData = WC300_KalmanFilter_2nd(flData);
#ifdef USE_COMPENSATION
		if (CalModeFlag)
		{
			Linearity_on_flag = 0;
			ZeroTracking_on_flag = 1;
			AutoZero_on_flag = 0;
		}
		else
		{
			Linearity_on_flag = 1;
			ZeroTracking_on_flag = 1;
			AutoZero_on_flag = 1;
		}
		globalCounter++;
		rawDispData = PhaseToDisplacement(rawPhaseData);
//		rawDispData = rawDispData * 100;
		
		filteredData_1st = WC300_MovingAVG(&Maf1stOrder,rawDispData);
		filteredData_2nd = WC300_MovingAVG(&Maf2ndOrder,filteredData_1st);
		filteredData_3rd = WC300_MovingAVG(&Maf3rdOrder,filteredData_2nd);
		filteredData_4th = WC300_MovingAVG(&Maf4thOrder,filteredData_3rd);
		filteredData_5th = WC300_MovingAVG(&Maf5thOrder,filteredData_4th);
		filteredData_6th = WC300_MovingAVG(&Maf6thOrder,filteredData_5th);
//        filteredData_7th = WC300_MovingAVG(&Maf7thOrder,filteredData_6th);
		//filteredData_Last = filteredData_6th; //Raw Filtered Value For Calibration
        filteredData_Last = filteredData_6th; //Raw Filtered Value For Calibration
		//Calibration
		//1. set CalModeFlag = 1; 
		//2. Wait 3 sec.
		//2~7.  RawW_xxxx = creepTrackingData/Cal_fact;
		//       RawW_maxm, RawW_zero, RawW_500g, RawW_200g, RawW_100g, RawW_50g
		//8. set CalModeFlag = 0;
		//9. AnalyzeInit();)

        filteredData_Last = filteredData_Last * 100;
		creepCompData = CreepCompProc(filteredData_Last);//normalize
#ifdef MG_CHECK_FILTER
        DC_Tracking_Filter(filteredData_Last);
        SlopeDiffMeasureProc(creepCompData); //mg check slide Diff function.
#endif
		creepTrackingData = ZeroCreepTrackingProc(creepCompData);//zeroing
		LinearCompData = LinearCompProc(creepTrackingData);
		WeightData = WeightGetData(LinearCompData);
#ifndef MG_CHECK_FILTER
        WeightCompData = WeightCompProc(WeightData);
#endif
#ifdef MG_CHECK_FILTER
        WeightCompData = SlopeCompProc(WeightData);//insert for mg check function
#endif
#ifndef MG_CHECK_FILTER
        WeightCompData = WeightGetDataDiv(WeightCompData);//50g -> 0.005 mg div
#endif
        
        
        if (CalModeFlag)
		{
			Calibration_Mode(CalModeKey, creepTrackingData, Cal_fact);
		}
        updateMonitorData((INT64S)WeightCompData,temperature);
#else		
        updateMonitorData((INT64S)rawPhaseData,temperature);
#endif
        
        if (TimerCheckDelay10ms(TIMER_DELAY_10MS_SEND_LCD))
        {
            TimerSetDelay10ms(TIMER_DELAY_10MS_SEND_LCD, 0);
            updateDisplay((INT64S)WeightCompData,LCD_Temp_data);
            network_tx_proc();
            TimerSetDelay10ms(TIMER_DELAY_10MS_SEND_LCD, 10);
        }
	}

}
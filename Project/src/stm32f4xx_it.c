/**
  ******************************************************************************
  * @file    Project/STM32F4xx_StdPeriph_Templates/stm32f4xx_it.c 
  * @author  MCD Application Team
  * @version V1.7.1
  * @date    20-May-2016
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "globals.h"
#include "stm32f4xx_it.h"
#include "stm32f4xx_usart.h"
#include "main.h"
#include "commbuf.h"
#include "timer.h"
#include "buzzer.h"
#include "key.h"
/** @addtogroup Template_Project
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
//extern __IO uint32_t uwPeriodValue;
extern __IO uint32_t uwCaptureNumber;
uint16_t tmpCC4[2] = {0, 0};

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
  /* This interrupt is generated when HSE clock fails */

  if (RCC_GetITStatus(RCC_IT_CSS) != RESET)
  {
    /* At this stage: HSE, PLL are disabled (but no change on PLL config) and HSI
       is selected as system clock source */

    /* Enable HSE */
    RCC_HSEConfig(RCC_HSE_ON);

    /* Enable HSE Ready and PLL Ready interrupts */
    RCC_ITConfig(RCC_IT_HSERDY | RCC_IT_PLLRDY, ENABLE);

    /* Clear Clock Security System interrupt pending bit */
    RCC_ClearITPendingBit(RCC_IT_CSS);

    /* Once HSE clock recovers, the HSERDY interrupt is generated and in the RCC ISR
       routine the system clock will be reconfigured to its previous state (before
       HSE clock failure) */
  }
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
  //TimingDelay_Decrement();
}

void USART1_IRQHandler(void)
{
    unsigned char rxChar;
    unsigned char txChar;
  /* USART in Receiver mode */
  if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
  {
      rxChar = USART_ReceiveData(USART1);
      PutCharRxBuf(&CommBufUart1,rxChar);
    USART_ClearITPendingBit(USART1, USART_IT_RXNE);
  }
  //kevin : USART in Tx mode */
  if(USART_GetITStatus(USART1, USART_IT_TXE) != RESET)
  {
      if (CheckTxBuf(&CommBufUart1)) 
      {
          txChar = GetCharTxBuf(&CommBufUart1);
          //USART_SendData(USART1, 0xFF);       //kevin : 실제로 UART 로 보내지는 쪽
          USART_SendData(USART1, txChar);       //kevin : 실제로 UART 로 보내지는 쪽
          CommBufUart1.Txing = 1;
      } 
      else 
      {
          USART_ITConfig(USART1,USART_IT_TXE,DISABLE);
          CommBufUart1.Txing = 0;
      } 
      USART_ClearITPendingBit(USART1, USART_IT_TXE);
      
  }	

}


void USART3_IRQHandler(void)
{
    unsigned char rxChar;
    unsigned char txChar;
  /* USART in Receiver mode */
  if (USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
  {
      rxChar = USART_ReceiveData(USART3);
      PutCharRxBuf(&CommBufUart3,rxChar);
    USART_ClearITPendingBit(USART3, USART_IT_RXNE);
  }
  //kevin : USART in Tx mode */
  if(USART_GetITStatus(USART3, USART_IT_TXE) != RESET)
  {
      if (CheckTxBuf(&CommBufUart3)) 
      {
          txChar = GetCharTxBuf(&CommBufUart3);
          //USART_SendData(USART1, 0xFF);       //kevin : 실제로 UART 로 보내지는 쪽
          USART_SendData(USART3, txChar);       //kevin : 실제로 UART 로 보내지는 쪽
          CommBufUart3.Txing = 1;
      } 
      else 
      {
          USART_ITConfig(USART3,USART_IT_TXE,DISABLE);
          CommBufUart3.Txing = 0;
      } 
      USART_ClearITPendingBit(USART3, USART_IT_TXE);
      
  }	

}


void TIM2_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM2,TIM_IT_Update) != RESET)
    {
        /* Clear TIM2 Capture Compare1 interrupt pending bit*/
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
        SysTimer1ms++;
       
        //KEY_Scan();           // duckspg key scan disable
        if ((SysTimer1ms % 10) == 0) // 10 : 10msec
        {
            SysTimer10ms++;
        }
        if ((SysTimer1ms % 100) == 0)
        {
            SysTimer100ms++;
        }
        
        if ((SysTimer1ms % 50) == 0) // 50msec
        {
            if(Buzzer.Times && Buzzer.Status == 0)
            {
                BUZZER_LOW;
                Buzzer.Times--;
                Buzzer.Status = 1;
            }
            else if(Buzzer.Status)
            {
                BUZZER_HIGH;
                Buzzer.Status = 0;
            }
        }
    
//    if ((SysTimer1ms % 300) == 0) 
//    {
//GPIOA는 AD 컨트롤 SPI 라인과 같이 사용하는 포트이므로
//인터럽트에서 사용하지 않아야 한다. 꼭 어플리케이션(main loop에 있어야함.)        
//        GPIO_ToggleBits(GPIOA,GPIO_Pin_2);
//        GPIO_ToggleBits(GPIOA,GPIO_Pin_3);
//    }
        
    }    
}
/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f40xx.s/startup_stm32f427x.s).                         */
/******************************************************************************/

/**
  * @brief  This function handles RTC Wakeup global interrupt request.
  * @param  None
  * @retval None
  */
void RTC_WKUP_IRQHandler(void)
{
  if(RTC_GetITStatus(RTC_IT_WUT) != RESET)
  {
    /* Toggle on LED1 */
//    STM_EVAL_LEDToggle(LED1);
    RTC_ClearITPendingBit(RTC_IT_WUT);
    EXTI_ClearITPendingBit(EXTI_Line22);
  } 
}

/**
  * @brief  This function handles TIM5 global interrupt request.
  * @param  None
  * @retval None
  */
void TIM5_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM5, TIM_IT_CC4) != RESET)
  {    
    /* Get the Input Capture value */
    
   
    /* Clear CC4 Interrupt pending bit */
    TIM_ClearITPendingBit(TIM5, TIM_IT_CC4);

  }
}

/**
  * @brief  This function handles SPI interrupt request.
  * @param  None
  * @retval None
  */
//void SPI2_IRQHandler(void)
//{
////  /* SPI in Receiver mode */
////  if (SPI_I2S_GetITStatus(SPIx, SPI_I2S_IT_RXNE) == SET)
////  {
////    if (ubRxIndex < BUFFERSIZE)
////    {
////      /* Receive Transaction data */
////      aRxBuffer[ubRxIndex++] = SPI_I2S_ReceiveData(SPIx);
////    }
////    else
////    {
////      /* Disable the Rx buffer not empty interrupt */
////      SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_RXNE, DISABLE);
////    }
////  }
//  /* SPI in Transmitter mode */
//  if (SPI_I2S_GetITStatus(SPI2, SPI_I2S_IT_TXE) != RESET)
//  {
//    if (ubTxIndex < BUFFERSIZE)
//    {
//      /* Send Transaction data */
//      SPI_I2S_SendData(SPI2, DspBuf[ubTxIndex++]);
//    }
//    else
//    {
//      /* Disable the Tx buffer empty interrupt */
//      SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_TXE, DISABLE);
//    }
//  }
//}


/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/


/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

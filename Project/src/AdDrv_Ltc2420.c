#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "globals.h"
#include "AdDrv.h"
#include "ringbuf.h"
#include "timer.h"


/*
********************************************************************************
*                       INCLUDES
********************************************************************************
*/
/* Insert include files here */
/** @brief Loadcell AD 값을 넣을 Ring Buffer */
RING_BUF MagTempAdDrvRingBuf;
RING_BUF BlockTempAdDrvRingBuf;

/** @brief AD 값을 넣을 Ring Buffer */
RING_BUF *MagTempAdRingBufPtr;
RING_BUF *BlockTempAdRingBufPtr;
/*
********************************************************************************
*                       GLOBAL(EXPORTED) VARIABLES & TABLES
********************************************************************************
*/
/* Insert global variables & tables here */


/*
********************************************************************************
*                       LOCAL DEFINITIONS & MACROS
********************************************************************************
*/


/** @brief Channel 1(L/C) Ad 값을 넣을 buffer size */
#define MAG_TEMP_AD_BUF_SIZE		200
#define BLOCK_TEMP_AD_BUF_SIZE		200

/** @brief Channel 2(Battery) Ad 값을 넣을 buffer size */
//#define AD_BUF_SIZE_FOR_BATT	10

/*
********************************************************************************
*                       LOCAL DATA TYPES & STRUCTURES
********************************************************************************
*/
/* Insert local typedef & structure here */


/*
********************************************************************************
*                       GLOBAL(FILE SCOPE) VARIABLES & TABLES
********************************************************************************
*/
/* Insert file scope variable & tables here */
/** @brief Loadcell AD 값을 넣을 Ring Buffer에서 사용할 byte array */

INT32S magTempAdDrvData[MAG_TEMP_AD_BUF_SIZE];
INT32S blockTempAdDrvData[BLOCK_TEMP_AD_BUF_SIZE];
/*
********************************************************************************
*                       LOCAL FUNCTION PROTOTYPES
********************************************************************************
*/
//static void writeAdData(INT8U *temp_data);
static INT32U LTC2420ReadAdValue(void);
INT32S temp1232(void);

/*
********************************************************************************
*                       GLOBAL(EXPORTED) FUNCTIONS
********************************************************************************
*/
/* Insert global functions here */





/**
********************************************************************************
* @brief    AD Driver Init 함수\n
*           AD Ring Buffer Init\n
*           AD Chip Init
* @param    none
* @return   none
* @remark   초기에 실행 되는 함수
********************************************************************************
*/

void TempAdDrvInit(void)
{
	RingBufInit(&MagTempAdDrvRingBuf, (char *)(&magTempAdDrvData[0]), MAG_TEMP_AD_BUF_SIZE);
	MagTempAdRingBufPtr = &MagTempAdDrvRingBuf;

	RingBufInit(&BlockTempAdDrvRingBuf, (char *)(&blockTempAdDrvData[0]), BLOCK_TEMP_AD_BUF_SIZE);
	BlockTempAdRingBufPtr = &BlockTempAdDrvRingBuf;
}

/**
********************************************************************************
* @brief    AD Driver Procedure\n
*           AD Chip에서 읽은 값을 Ring Buffer에 저장
* @param    none
* @return   none
* @remark   일정 시간에 한번씩 계속 실행 되는 함수\n
*           AD Chip의 Sampling 시간 보다는 빨리 실행 되어야 함
********************************************************************************
*/

void TempAdDrvProc(void)
{
	INT32S adData = 0;

	adData = LTC2420ReadAdValue();
        if(adData) 
	{
		 RingBufPutInt32sForAdDrv(&MagTempAdDrvRingBuf, adData);
	}

#ifdef USE_BLOCK_TEMP_SENSOR    
        adData = 0;
        
        if (GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_1) == 0)
        {
             adData = temp1232();
        }

	if(adData) 
	{
		 RingBufPutInt32sForAdDrv(&BlockTempAdDrvRingBuf, adData);
	}
#endif
}


/*****************************************************************************/
/*
********************************************************************************
*                       LOCAL FUNCTIONS
********************************************************************************
*/
/* Insert local functions here */
/*
*
*
*
*
*/

INT32S temp1232(void)
{
  INT32U bData,i;
  bData = 0x00000000;
  for (i = 0; i < 25; i++)
  {
      bData <<= 1;
      GPIO_SetBits(GPIOB,GPIO_Pin_0);
      //delay4Clock();
      GPIO_ResetBits(GPIOB,GPIO_Pin_0);
      if (GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_1)) 
      {
              bData |= 0x00000001;
      }
      else
      {
              bData &= 0xfffffffe;
      }
      //delay4Clock();
  }

  return (INT32S)(bData>>1);
}




/*
******************************************************************************
* @brief    ad 컨버팅된 Data를 읽는 함수 \n
*
* @param    none
* @return   none
* @remark   연속적으로 AD 변환됨 
********************************************************************************
*/

//INT32U LTC2420ReadAdValue(void)
//{
//    INT8U  i;
//    INT8U eocFlag = 0;
//    INT32U  adsData;
//	
//	adsData = 0x00000000;
//    
//    
//    TEMP_AD_CLK_LOW;
//    
//    delay16Clock();
//    
//
//    TEMP_AD_CLK_HIGH;
//    delay16Clock();
//
//    if(TEMP_AD_DATA_OUT)
//    {
//        TEMP_AD_CLK_LOW;
//        
//        return 0;
//    }
//
//    
//
//    //dummy clock
//    TEMP_AD_CLK_LOW;
//    delay16Clock();
//    
//
//    TEMP_AD_CLK_HIGH;
//    delay16Clock();
//    
//
//    
//	for(i=0; i<22; i++)
//    {
//		adsData <<= 1;		
//        
//        TEMP_AD_CLK_LOW;
//        
//        delay16Clock();
//    
// 
//        TEMP_AD_CLK_HIGH;
//        
//        delay16Clock();
// 
//        if(TEMP_AD_DATA_OUT)
//		{
//			adsData++;
//		}
//    
//        
//	}
//    TEMP_AD_CLK_LOW;
//    adsData &= 0x000FFFFF;
//	return adsData;	
//    
//}

INT32U LTC2420ReadAdValue(void)
{
    INT8U  i;
    INT8U eocFlag = 0;
    INT32U  adsData;
	
	adsData = 0x00000000;
    
    
    if(TEMP_AD_DATA_OUT) return 0;
    
    TEMP_AD_CLK_LOW;
    
    delay16Clock();
    

    TEMP_AD_CLK_HIGH;
    delay16Clock();

    if(TEMP_AD_DATA_OUT)
    {
        TEMP_AD_CLK_LOW;
        
        return 0;
    }

    

    //dummy clock
    TEMP_AD_CLK_LOW;
    delay16Clock();
    

    TEMP_AD_CLK_HIGH;
    delay16Clock();
    

    
	for(i=0; i<22; i++)
    {
		adsData <<= 1;		
        
        TEMP_AD_CLK_LOW;
        
        delay16Clock();
    
 
        TEMP_AD_CLK_HIGH;
        
        delay16Clock();
 
        if(TEMP_AD_DATA_OUT)
		{
			adsData++;
		}
    
        
	}
    TEMP_AD_CLK_LOW;
    adsData &= 0x000FFFFF;
	return adsData;	
    
}

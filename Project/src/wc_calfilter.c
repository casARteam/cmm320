#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "globals.h"
#include "stm32f4xx_gpio.h"
#include "wc_calfilter.h"
#include "AdApi.h"
#include "filter.h"
#include "timer.h"
#include "commbuf.h"


/* extern으로 설정해서 바로 값을 변경할 수 있게 만든 구문 */

unsigned char TempCapaSetting = HIGHCAPA;           // 기본적으로 여기서 케파를 바꾸면 적용됨 나중에 지워야함


/*low capa kel filter 값 */
FP64 LowCapa_Pk_1st = 1.0;
FP64 LowCapa_varP_1st = 0.001;
FP64 LowCapa_R_1st = 7.5;
FP64 LowCapa_Kk_1st = 1.0;
FP64 LowCapa_Xk_1st = 0;

FP64 LowCapa_Pk_2nd = 1.0;
FP64 LowCapa_varP_2nd = 0.0001;
FP64 LowCapa_R_2nd = 15;
FP64 LowCapa_Kk_2nd = 1;
FP64 LowCapa_Xk_2nd = 0;

INT32U LowMoving1stwinsize = 100;

/*high capa kel filter 값 */
FP64 HighCapa_Pk_1st = 1.0;
FP64 HighCapa_varP_1st = 0.001;
FP64 HighCapa_R_1st = 7.5;
FP64 HighCapa_Kk_1st = 1.0;
FP64 HighCapa_Xk_1st = 0;

FP64 HighCapa_Pk_2nd = 1.0;
FP64 HighCapa_varP_2nd = 0.0001;                
FP64 HighCapa_R_2nd = 15;
FP64 HighCapa_Kk_2nd = 1;
FP64 HighCapa_Xk_2nd = 0;       

//INT32U HighMoving1stwinsize = 200;   //duckspg 
//INT32U HighMoving1stwinsize = 100;   //duckspg 
INT32U HighMoving1stwinsize = 30;
/* extern으로 설정해서 바로 값을 변경할 수 있게 만든 구문  end*/

#define FILTERVALUEADD          0x04
#define FILTERVALUEOFFSET    8

#ifdef WEIGHT_CAPA_10G 
#define RAWDATANUM  4 //kevin : zero 포함 캘리브레이션 갯수 
//const INT32U Disp_W_data[RAWDATANUM] = {0, 20000000, 50000000, 100000000};
const INT32U Disp_W_data[RAWDATANUM] = {0, 20000040, 50000090, 100000100};

//#define RAWDATANUM  3 ///test 200g cell
//const INT32U Disp_W_data[RAWDATANUM] = {0, 50000000, 100000000};//test 200g cell
#else
#define RAWDATANUM  5 //kevin : zero 포함 캘리브레이션 갯수 
//const INT32U Disp_W_data[RAWDATANUM] = {0, 10000000, 20000000, 50000000, 100000000};
const INT32U Disp_W_data[RAWDATANUM] = {0, 10000002, 20000005, 50000002, 100000004};//100g=100.000 017

//#define RAWDATANUM  3 ///test 200g cell
//const INT32U Disp_W_data[RAWDATANUM] = {0, 50000000, 100000000};//test 200g cell
#endif
FP64 RawZ_data[RAWDATANUM];
FP64 RawZ_Tdata[RAWDATANUM];
FP64 RawW_data[RAWDATANUM];
FP64 RawW_Tdata[RAWDATANUM];
FP64 linearA[RAWDATANUM];
FP64 linearB[RAWDATANUM];

struct CalFactorForm {
	FP64 Temp_a0;
	FP64 Temp_a1;
	FP64 Temp_a2;
	FP64 Temp_b0;
	FP64 Temp_b1;
	FP64 Temp_b2;
	FP64 Temp_Ws;
	FP64 Temp_Wz;
	FP64 NL_coverb;
	FP64 SpanCalV2WT;
    
    //ist order
    FP64 HighTemp, MidTemp, LowTemp;
    FP64 HighSpan, MidSpan, LowSpan;
    FP64 HighZero, MidZero, LowZero;
} CalFactor;

void CalLinearCoffSetting(INT8U calFlag);
void EepromCalDataSave(void);
INT32U AdTempComp(INT32U rawData, INT32U magTemp);
FP64 DoTempCompensation(FP64 Wmeasure, FP64 CurrentTemp);

extern INT8U MAFStableFlag;
extern INT8U StableSendDetactFlag;
extern INT8U StableCalDetactFlag;

//extern INT8U CalKeyCount;
INT8U NextKeyCount=0;
INT8U ZeroKeyCount=0;

//#define USE_CAL_MAFS_FILTER
void CalsettingInit(void)
{
    INT32U rawCalAdData = 0;
    INT32U rawCalMagTempAdData = 0;
    INT32U normCalMagTempAdData = 0;
    INT32U rawCalBlockTempAdData = 0;
    INT32U normCalBlockTempAdData = 0;
    //INT32U tempCompCalAdData = 0;
    INT8U capaFlag = 0;
    INT8U arrayDataFlag = 0;
    INT8U revChar;
    
    char rawAdDataASCII[20];
    
#ifdef USE_CAL_MAFS_FILTER
//void MAFSInit(INT8U winMinSize, INT8U winMaxSize, INT32S winIncThres, INT32S winBreakThres, INT8U shockWinSize, INT32S shockThres, INT32S thresIncThres)
    MAFSInit(10*MAFS_SRATE, 10*MAFS_SRATE, 250*14, 500*14, 1*MAFS_SRATE, 2*500*14, 50000000*14);//for calibration min filter size 10*10
#endif  
    
    EepromCalDataRead(); //Read No Temp Comp Cal data
    
    while(1)
    {
        GPIO_ResetBits(GPIOA,GPIO_Pin_3);
        
        while(1)
        {
            rawCalAdData = AdGetFilterdRawData(); 
            
            rawCalMagTempAdData = AdGetMagTempAd(); //kevin : 온도 AD값
            if(rawCalMagTempAdData)
            {
                normCalMagTempAdData = rawCalMagTempAdData;// / 10;
            }

#ifdef USE_BLOCK_TEMP_SENSOR            ////ext temp sensor (1232)        
            rawCalBlockTempAdData = AdGetBlockTempAd(); //kevin : 온도 AD값
            if(rawCalBlockTempAdData)
            {
                normCalBlockTempAdData = rawCalBlockTempAdData;
            } 
#endif         
            
            if (CheckRxBuf(&CommBufUart1)) 
            {
                revChar = GetCharRxBuf(&CommBufUart1);
            }
        
            if(rawCalAdData)
            {
                //tempCompCalAdData = AdTempComp(rawCalAdData, normCalMagTempAdData); //Temp. Comp Data
                
                //DispRawData(tempCompCalAdData);
                DispRawData(rawCalAdData);
                
                //Int32sToUnsignedDecStr(rawAdDataASCII, tempCompCalAdData, 12, 0, 0, 0, 0); 
                Int32sToUnsignedDecStr(rawAdDataASCII, rawCalAdData, 12, 0, 0, 0, 0); 
                CommSendData(rawAdDataASCII, 12,  0);
                if(arrayDataFlag%2 == 0)
                {
                    //DispTempData(Disp_W_data[0]);
                    DispCalWeightData(Disp_W_data[0]);
                    
                    Int32sToDecStr(rawAdDataASCII, Disp_W_data[0]/100, 10, 0, 0, 0, 0); 
                    CommSendData(rawAdDataASCII, 10,  1);
                }
                else
                {
                    //DispTempData(Disp_W_data[arrayDataFlag/2 + 1]);
                    DispCalWeightData(Disp_W_data[arrayDataFlag/2 + 1]);
                    
                    Int32sToDecStr(rawAdDataASCII, Disp_W_data[arrayDataFlag/2 + 1]/100, 10, 0, 0, 0, 0); 
                    CommSendData(rawAdDataASCII, 10,  1);
                }
                
                network_tx_proc();
                
                if (!GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_2) || revChar == 'z' || revChar == 'Z')         // ZERO
                {
                    ZeroKeyCount++;
                    if (ZeroKeyCount > KEY_COUNT_MAX  || revChar == 'z' || revChar == 'Z')
                    { 
                        ZeroKeyCount = 0;
                        revChar = 0;
#ifdef USE_INDI_CAL
                        arrayDataFlag++;
                        break;
#else                  
                        return;//exit
#endif                    
                    }                        
                } else ZeroKeyCount = 0;
                
                if (revChar == 'x' || revChar == 'X')        
                {
                    revChar = 0;
                    return;//exit
                }                
            
                if (!GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_1) || revChar == 'n' || revChar == 'N')
                {
                    NextKeyCount++;
                    if (NextKeyCount > KEY_COUNT_MAX  || revChar == 'n' || revChar == 'N')
                    {  
                        NextKeyCount = 0;
                        revChar = 0;
                        GPIO_SetBits(GPIOA,GPIO_Pin_3);    

                        while(1)
                        {
                            rawCalAdData = AdGetFilterdRawData(); 
                            
                            rawCalMagTempAdData = AdGetMagTempAd(); //kevin : 온도 AD값
                            if(rawCalMagTempAdData)
                            {
                                normCalMagTempAdData = rawCalMagTempAdData;// / 10;
                            }
#ifdef USE_BLOCK_TEMP_SENSOR            ////ext temp sensor (1232)        
                            rawCalBlockTempAdData = AdGetBlockTempAd(); //kevin : 온도 AD값
                            if(rawCalBlockTempAdData)
                            {
                                normCalBlockTempAdData = rawCalBlockTempAdData;
                            } 
#endif                             
                            if(rawCalAdData)
                            {
                                //tempCompCalAdData = AdTempComp(rawCalAdData, normCalMagTempAdData); //Temp. Comp Data
                                
                                //DispRawData(tempCompCalAdData);
                                DispRawData(rawCalAdData);
                                
                                //Int32sToUnsignedDecStr(rawAdDataASCII, tempCompCalAdData, 12, 0, 0, 0, 0); 
                                Int32sToUnsignedDecStr(rawAdDataASCII, rawCalAdData, 12, 0, 0, 0, 0); 
                                CommSendData(rawAdDataASCII, 12,  1);     
                                network_tx_proc();
                                //if (MAFStableFlag == 1)
                                if (StableCalDetactFlag == 1) //need test
                                //if(1)
                                {
                                    if(arrayDataFlag%2 == 0)
                                    {
                                        //RawZ_data[arrayDataFlag/2] = tempCompCalAdData;
                                        RawZ_data[arrayDataFlag/2] = rawCalAdData;
                                        RawZ_Tdata[arrayDataFlag/2] = normCalMagTempAdData;
                                        if(arrayDataFlag == 0)
                                        {
                                            //RawW_data[arrayDataFlag/2] = tempCompCalAdData;
                                            RawW_data[arrayDataFlag/2] = rawCalAdData;
                                            RawW_Tdata[arrayDataFlag/2] = normCalMagTempAdData;
                                        }
                                    }
                                    else
                                    {
                                        //RawW_data[arrayDataFlag/2 + 1] = tempCompCalAdData;
                                        RawW_data[arrayDataFlag/2 + 1] = rawCalAdData;
                                        RawW_Tdata[arrayDataFlag/2 + 1] = normCalMagTempAdData;
                                    }                                   
                                    //TimerSetDelay10ms(TIMER_DELAY_10MS_DELAY,100);
                                    //while(!TimerCheckDelay10ms(TIMER_DELAY_10MS_DELAY)); 
                                    arrayDataFlag++;
                                    break;
                                }
                            }
                        }
                        break;
                    }
                } else NextKeyCount = 0;
            }
        }
        
        if (arrayDataFlag == (2*RAWDATANUM - 1))
        {
            break;
        }
    }



    CalLinearCoffSetting(1);
    EepromCalDataSave();
    CalLinearCoffSetting(0);//Renew Linear coeff after save cal eeprom
   
    Eeprom_bwrite(SCALESELECTADD,HIGHCAPA);        // HIGHCAPA : global에 설정되어 있음  임시로 HIGHCAPA로 설정함
}

// FilterSettingInit();
FP64 DoNLCompensation(FP64 Wuncompensated)
{
   FP64 Wcompensated = 0;

#ifdef USE_LINEAR_2ND_ORDER_COMP
   Wuncompensated = Wuncompensated - RawW_data[0];

   Wcompensated = Wuncompensated * (1 + CalFactor.NL_coverb * Wuncompensated);

   Wcompensated = Wcompensated + RawW_data[0];

   return (Wcompensated);
#elif defined(USE_LINEAR_2ND_ORDER_COMP_NO_ZERO)
   Wcompensated = Wuncompensated * (1 + CalFactor.NL_coverb * Wuncompensated);
   return (Wcompensated);
#else
   return (Wuncompensated);
#endif
}

FP64 DoNLCompensationRev(FP64 Wcompensated)
{
   FP64 Wuncompensated = 0;

#ifdef USE_LINEAR_2ND_ORDER_COMP
   Wcompensated = Wcompensated - RawW_data[0];

   Wuncompensated = (-1 + sqrt(1 + 4 * CalFactor.NL_coverb  * Wcompensated)) / (2 * CalFactor.NL_coverb);

   Wuncompensated = Wuncompensated + RawW_data[0];

   return (Wuncompensated);
#elif defined(USE_LINEAR_2ND_ORDER_COMP_NO_ZERO)
   Wuncompensated = (-1 + sqrt(1 + 4 * CalFactor.NL_coverb  * Wcompensated)) / (2 * CalFactor.NL_coverb);
   return (Wuncompensated);
#else
   return (Wcompensated);
#endif
}


extern INT8U TempCompOnFlag;

#ifdef USE_INDI_CAL
void CalLinearCoffSetting(INT8U calFlag)
{
    INT8U i = 0;
    FP64 tempRawW_data[RAWDATANUM];

#ifdef USE_TEMP_COMP
    if  (TempCompOnFlag == 1 && calFlag == 0)
    {
        //Tmep comp
#ifdef WEIGHT_CAPA_10G        //check RAWDATANUM
        RawW_data[0] = DoTempCompensation(RawW_data[0], RawW_Tdata[0]);//2 zero
        RawW_data[1] = DoTempCompensation(RawW_data[1], RawW_Tdata[1]);//2
        RawW_data[2] = DoTempCompensation(RawW_data[2], RawW_Tdata[2]);//5  
        RawW_data[3] = DoTempCompensation(RawW_data[3], RawW_Tdata[3]);;//10
        RawZ_data[0] = RawW_data[0]; //2 zero
        RawZ_data[1] = DoTempCompensation(RawZ_data[1], RawZ_Tdata[1]);//5 zero 
        RawZ_data[2] = DoTempCompensation(RawZ_data[2], RawZ_Tdata[2]);;//10 zero
        RawZ_data[3] = RawZ_data[2];//no use
#else
        RawW_data[0] = DoTempCompensation(RawW_data[0], RawW_Tdata[0]);//100 zero
        RawW_data[1] = DoTempCompensation(RawW_data[1], RawW_Tdata[1]);//100
        RawW_data[2] = DoTempCompensation(RawW_data[2], RawW_Tdata[2]);//200  
        RawW_data[3] = DoTempCompensation(RawW_data[3], RawW_Tdata[3]);;//500
        RawW_data[4] = DoTempCompensation(RawW_data[4], RawW_Tdata[4]);//1000
        RawZ_data[0] = RawW_data[0]; //100 zero
        RawZ_data[1] = DoTempCompensation(RawZ_data[1], RawZ_Tdata[1]);//200 zero 
        RawZ_data[2] = DoTempCompensation(RawZ_data[2], RawZ_Tdata[2]);;//500 zero
        RawZ_data[3] = DoTempCompensation(RawZ_data[3], RawZ_Tdata[3]);//1000 zero
        RawZ_data[4] = RawZ_data[3];//no use
#endif
    }
#endif    
    
#if defined(USE_LINEAR_2ND_ORDER_COMP) || defined(USE_LINEAR_2ND_ORDER_COMP_NO_ZERO)
    if  (calFlag == 0)
    {
#ifdef WEIGHT_CAPA_10G        //check RAWDATANUM
        RawW_data[0] = DoNLCompensation(RawW_data[0]);//2 zero
        RawW_data[1] = DoNLCompensation(RawW_data[1]);//2
        RawW_data[2] = DoNLCompensation(RawW_data[2]);//5  
        RawW_data[3] = DoNLCompensation(RawW_data[3]);;//10
        RawZ_data[0] = RawW_data[0]; //2 zero
        RawZ_data[1] = DoNLCompensation(RawZ_data[1]);//5 zero 
        RawZ_data[2] = DoNLCompensation(RawZ_data[2]);;//10 zero
        RawZ_data[3] = RawZ_data[2];//no use
#else
        RawW_data[0] = DoNLCompensation(RawW_data[0]);//100 zero
        RawW_data[1] = DoNLCompensation(RawW_data[1]);//100
        RawW_data[2] = DoNLCompensation(RawW_data[2]);//200  
        RawW_data[3] = DoNLCompensation(RawW_data[3]);;//500
        RawW_data[4] = DoNLCompensation(RawW_data[4]);//1000
        RawZ_data[0] = RawW_data[0]; //100 zero
        RawZ_data[1] = DoNLCompensation(RawZ_data[1]);//200 zero 
        RawZ_data[2] = DoNLCompensation(RawZ_data[2]);;//500 zero
        RawZ_data[3] = DoNLCompensation(RawZ_data[3]);//1000 zero
        RawZ_data[4] = RawZ_data[3];//no use
#endif
    }
#endif    
        
    RawW_data[0] = RawZ_data[0];
    tempRawW_data[0] = RawW_data[0];
    for (i = 1 ; i < RAWDATANUM; i++)
    {
        tempRawW_data[i] = RawW_data[i] - (RawZ_data[i-1] - RawZ_data[0]);
        
    }

    for (i = 0 ; i < RAWDATANUM - 1; i++)
    {
        linearA[i] = (FP64)(Disp_W_data[i+1] - Disp_W_data[i])/(tempRawW_data[i+1]-tempRawW_data[i]);
        linearB[i] = Disp_W_data[i] - linearA[i]*tempRawW_data[i];    
    }
}
#else
void CalLinearCoffSetting(INT8U calFlag)
{
    INT8U i = 0;
    
// zero(after loading)
//    for (i = 0 ; i < RAWDATANUM; i++)
//    {
//        RawW_data[i] = RawW_data[i] - (RawZ_data[i] - RawZ_data[0]);
//    }

// zero(before loading)
    RawW_data[0] = RawZ_data[0];
    for (i = 1 ; i < RAWDATANUM; i++)
    {
        RawW_data[i] = RawW_data[i] - (RawZ_data[i-1] - RawZ_data[0]);
        
    }

    for (i = 0 ; i < RAWDATANUM - 1; i++)
    {
        linearA[i] = (FP64)(Disp_W_data[i+1] - Disp_W_data[i])/(RawW_data[i+1]-RawW_data[i]);
        linearB[i] = Disp_W_data[i] - linearA[i]*RawW_data[i];    
    }
}
#endif

#ifdef USE_LINEAR1_SET_ZERO 
INT32S AdLinearZeroRawData = 0;

INT8U AdLinearSetZero(INT32U rawData)
{
    //AdLinearZeroRawData = rawData - RawW_data[0];
    AdLinearZeroRawData = DoNLCompensation(rawData) - RawW_data[0];
}
#endif
        
INT32S AdLinearCompProc(INT32U rawData)
{
    INT8U i;
    INT32S normalData = 0;
    
    rawData = DoNLCompensation(rawData);//2nd Lin comp
    
#ifdef USE_LINEAR1_SET_ZERO     
    rawData = rawData - AdLinearZeroRawData;
#endif    
    
    for (i = 1 ; i < RAWDATANUM ; i++)
    {
        if (rawData < RawW_data[i])
        {
            break;
        }
    }
    
    if (i < RAWDATANUM)
    {
        normalData = (INT32S)(linearA[i-1]*rawData + linearB[i-1]);
    }
    else
    {
        normalData = (INT32S)(linearA[RAWDATANUM-2]*rawData + linearB[RAWDATANUM-2]);
    }
    
    return normalData;
}

INT32U AdLinearCompProcRev(FP64 normalData)
{
    INT8U i;
    FP64 rawDataRev = 0;
	FP64 RawWRev[RAWDATANUM];

	for (i = 1 ; i < RAWDATANUM ; i++)
    {
		RawWRev[i] = linearA[i]*RawW_data[i] + linearB[i];
	}

    for (i = 1 ; i < RAWDATANUM ; i++)
    {
        if (normalData < RawWRev[i])
        {
            break;
        }
    }
    
    if (i < RAWDATANUM)
    {
        rawDataRev = (normalData - linearB[i-1]) / linearA[i-1];
    }
    else
    {
        rawDataRev = (normalData - linearB[RAWDATANUM-2]) / linearA[RAWDATANUM-2];
    }
    
#ifdef USE_LINEAR1_SET_ZERO
    rawDataRev = rawDataRev + AdLinearZeroRawData;
#endif    

	rawDataRev = DoNLCompensationRev(rawDataRev);//2nd Lin comp

	return rawDataRev;
}


void EepromCalDataSave(void)
{
    INT8U i = 0;
    INT8U j = 0;
    for(i = 0 ; i<RAWDATANUM ; i++)
    {
        Eeprom_swrite(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&RawW_data[i],FILTERVALUEOFFSET);
        j++;
        Eeprom_swrite(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&linearA[i],FILTERVALUEOFFSET);
        j++;
        Eeprom_swrite(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&linearB[i],FILTERVALUEOFFSET);
        j++;
        Eeprom_swrite(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&RawZ_data[i],FILTERVALUEOFFSET);
        j++;
        Eeprom_swrite(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&RawW_Tdata[i],FILTERVALUEOFFSET);
        j++;
        Eeprom_swrite(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&RawZ_Tdata[i],FILTERVALUEOFFSET);
        j++;
    }
    Eeprom_swrite(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&RawW_data[i],FILTERVALUEOFFSET);
}

void EepromCalDataRead(void)
{
    INT8U i = 0;
    INT8U j = 0;
    for(i = 0 ; i<RAWDATANUM ; i++)
    {
        Eeprom_sread(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&RawW_data[i],FILTERVALUEOFFSET);
        j++;
        Eeprom_sread(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&linearA[i],FILTERVALUEOFFSET);
        j++;
        Eeprom_sread(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&linearB[i],FILTERVALUEOFFSET);
        j++;
        Eeprom_sread(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&RawZ_data[i],FILTERVALUEOFFSET);
        j++;
        Eeprom_sread(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&RawW_Tdata[i],FILTERVALUEOFFSET);
        j++;
        Eeprom_sread(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&RawZ_Tdata[i],FILTERVALUEOFFSET);
        j++;
    }
    Eeprom_sread(FILTERVALUEADD+FILTERVALUEOFFSET*j ,&RawW_data[i],FILTERVALUEOFFSET);
#ifdef WEIGHT_CAPA_10G 
    //10g 201705233 cell
	/*
    CalFactor.Temp_a0 = -0.000103595417235;
	CalFactor.Temp_a1 = 124.5361755338;
	CalFactor.Temp_a2 = 1878742698.81145;

	CalFactor.Temp_b0 = -0.000089144834915;
	CalFactor.Temp_b1 = 107.540484818388;
	CalFactor.Temp_b2 = 1851414050.40902;

	CalFactor.Temp_Ws = 1915410011;//span
	CalFactor.Temp_Wz = 1883160392;//zero

	CalFactor.NL_coverb = 5.8536E-13;//USE_LINEAR_2ND_ORDER_COMP
    
    CalFactor.HighTemp = 509128;
    CalFactor.HighSpan = 1915294486;
    CalFactor.HighZero = 1883058572;

    CalFactor.MidTemp = 515410;
    CalFactor.MidSpan = 1915410011;
    CalFactor.MidZero = 1883160392;

    CalFactor.LowTemp = 527976;
    CalFactor.LowSpan = 1915616661;
    CalFactor.LowZero = 1883342951;
    */
    //10g 20171114
	CalFactor.Temp_a0 = 0.000790983477601;
	CalFactor.Temp_a1 = -804.746412430432;
	CalFactor.Temp_a2 = 2446136335.37572;

	CalFactor.Temp_b0 = 0.0007686482644728;
	CalFactor.Temp_b1 = -781.530561076041;
	CalFactor.Temp_b2 = 2369796568.40915;

	CalFactor.Temp_Ws = 2241574311;//span
	CalFactor.Temp_Wz = 2171267312;//zero

	CalFactor.NL_coverb = 1.0E-15;//USE_LINEAR_2ND_ORDER_COMP_NO_ZERO

    CalFactor.HighTemp = 510274;
    CalFactor.HighSpan = 2241451088;
    CalFactor.HighZero = 2171142136;

    CalFactor.MidTemp = 521281;
    CalFactor.MidSpan = 2241574311;
    CalFactor.MidZero = 2171267312;

    CalFactor.LowTemp = 527108;
    CalFactor.LowSpan = 2241717133;
    CalFactor.LowZero = 2171408977;    
#else //1kg
    //1kg 20170926 
    /*
	CalFactor.Temp_a0 = 0.000276727255327;
	CalFactor.Temp_a1 = -281.510741560641;
	CalFactor.Temp_a2 = 4086837420.64146;

	CalFactor.Temp_b0 = 0.000272781613293;
	CalFactor.Temp_b1 = -266.428463885016000;
	CalFactor.Temp_b2 = 2606896188.80849;

	CalFactor.Temp_Ws = 4015329898;//span
	CalFactor.Temp_Wz = 2542234019;//zero

#ifdef USE_LINEAR_2ND_ORDER_COMP    
	CalFactor.NL_coverb = 5.4348E-13;//
#elif defined(USE_LINEAR_2ND_ORDER_COMP_NO_ZERO)
    CalFactor.NL_coverb = 5.4239E-13;//
#endif

    CalFactor.HighTemp = 513490;
    CalFactor.HighSpan = 4015249696;
    CalFactor.HighZero = 2542012705;

    CalFactor.MidTemp = 526343;
    CalFactor.MidSpan = 4015329898;
    CalFactor.MidZero = 2542234019;

    CalFactor.LowTemp = 535622;
    CalFactor.LowSpan = 4015444613; 
    CalFactor.LowZero = 2542449812;
    */
//1kg 20171109
	CalFactor.Temp_a0 = 0.000082181933184;
	CalFactor.Temp_a1 = -77.607515600944500;
	CalFactor.Temp_a2 = 4034975786.44935;

	CalFactor.Temp_b0 = 0.000086143802035;
	CalFactor.Temp_b1 = -70.312371171416200;
	CalFactor.Temp_b2 = 2555513337.75387;

	CalFactor.Temp_Ws = 4016779761;//span
	CalFactor.Temp_Wz = 2542083122;//zero

	CalFactor.NL_coverb = 5.09E-13;//USE_LINEAR_2ND_ORDER_COMP_NO_ZERO

    CalFactor.HighTemp = 499651;
    CalFactor.HighSpan = 4016715930;
    CalFactor.HighZero = 2541887588;

    CalFactor.MidTemp = 511307;
    CalFactor.MidSpan = 4016779761;
    CalFactor.MidZero = 2542083122;

    CalFactor.LowTemp = 522190;
    CalFactor.LowSpan = 4016859509; 
    CalFactor.LowZero = 2542286819;    
#endif    
}

extern INT8U StableSendDetactFlag;
INT8U HoldFlag = 0;
INT32S HoldWeight = 0;
INT32S AdWeightHoldProc(INT32S weight_data)
{
#ifdef USE_HOLD
    static INT32S prev_weight_data;
    INT32S err;
        
    err = weight_data - HoldWeight;

    if (StableSendDetactFlag == 1) //1mg
    {
        HoldFlag = 1;
        HoldWeight = weight_data;
    }
        
    if ((HoldFlag == 1) && (err > -HOLD_COMP_RANGE) && (err < HOLD_COMP_RANGE)) //10g
    {
        return prev_weight_data;
    }
    else
    {
        HoldFlag = 0;
    }
    
    prev_weight_data = weight_data;
    return weight_data;
#else
    return weight_data;
#endif  
}

#define WCOMP_MAX_WS_DATA       10
INT32S Disp_WStable_data[WCOMP_MAX_WS_DATA];
#define WCOMP_MAX_COUNT         10  //for smooth comp. disp

INT32S AdWeightCompProc(INT32S weight_data)
{
#ifdef USE_WEIGHT_COMP
    INT32S err;
    INT32S cal_weight_data;
    INT16U i;
    static INT8U wCompCnt = 0;
    static INT8U wCompWSCnt = 0;
    static INT8U wCompWSInitflag = 0;
    
    //////////////////////////////////////////////////////////
//    err = weight_data - (weight_data / WEIGHT_COMP_RANGE) * WEIGHT_COMP_RANGE;
//        
//    if ((err > -WEIGHT_COMP_RANGE) && (err < WEIGHT_COMP_RANGE)) //10g
//    {
//        //err = (err * WEIGHT_COMP_PERCENT) / 100;
//        wCompCnt++;
//        if (wCompCnt > WCOMP_MAX_COUNT) wCompCnt = WCOMP_MAX_COUNT;
//        err = (err * (WEIGHT_COMP_PERCENT * wCompCnt / WCOMP_MAX_COUNT)) / 100;
//        return  weight_data = ((weight_data - err) / 10) * 10 + (weight_data % 10);
//    }
//    else
//    {
//        wCompCnt = 0;
//        return weight_data;
//    }      
    ////////////////////////////////////////////////////////  
      
        
    for (i = 0; i < RAWDATANUM; i++)
    {
        cal_weight_data = Disp_W_data[RAWDATANUM - i - 1];
        if (weight_data >= (cal_weight_data - WEIGHT_COMP_RANGE)) break;
    }
    
    err = weight_data - cal_weight_data;
        
    if ((err > -WEIGHT_COMP_RANGE) && (err < WEIGHT_COMP_RANGE)) //10g
    {
        //err = (err * WEIGHT_COMP_PERCENT) / 100;
        wCompCnt++;
        if (wCompCnt > WCOMP_MAX_COUNT) wCompCnt = WCOMP_MAX_COUNT;
        err = (err * (WEIGHT_COMP_PERCENT * wCompCnt / WCOMP_MAX_COUNT)) / 100;
        return  weight_data = ((weight_data - err) / 10) * 10 + (weight_data % 10);
    }
    else
    {
/////////////////////////////////////////////////////
#ifdef USE_STABLE_WEIGHT_COMP     
        if (wCompWSInitflag == 0)
        {
            for (i = 0; i < WCOMP_MAX_WS_DATA; i++)
            {
                Disp_WStable_data[i] = 0;
            }
            wCompWSInitflag = 1;
        }
        
        for (i = 0; i < WCOMP_MAX_WS_DATA; i++)
        {
            cal_weight_data = Disp_WStable_data[i];
            if (weight_data >= (cal_weight_data - WEIGHT_COMP_RANGE) && weight_data <= (cal_weight_data + WEIGHT_COMP_RANGE)) break;
        }
        
        err = weight_data - cal_weight_data;
            
        if ((err > -WEIGHT_COMP_RANGE) && (err < WEIGHT_COMP_RANGE)) //10g
        {
            //err = (err * WEIGHT_COMP_PERCENT) / 100;
            wCompCnt++;
            if (wCompCnt > WCOMP_MAX_COUNT) wCompCnt = WCOMP_MAX_COUNT;
            err = (err * (WEIGHT_COMP_PERCENT * wCompCnt / WCOMP_MAX_COUNT)) / 100;
            return  weight_data = ((weight_data - err) / 10) * 10 + (weight_data % 10);
        }
        
        if (MAFStableFlag == 1)
        {
            for (i = 0; i < WCOMP_MAX_WS_DATA; i++)
            {
                if (weight_data/WEIGHT_COMP_RANGE == Disp_WStable_data[i]/WEIGHT_COMP_RANGE) break;
            }
            
            if (i == WCOMP_MAX_WS_DATA)
            {              
                wCompWSCnt = (wCompWSCnt + WCOMP_MAX_WS_DATA + 1) % WCOMP_MAX_WS_DATA;
                Disp_WStable_data[wCompWSCnt] = weight_data;
            }
        }
#endif        
/////////////////////////////////////////////////////      
        wCompCnt = 0;        
        return weight_data;
    }
#else
    return weight_data;
#endif    
}

////////////////////////////////////////////////////////////////////////////////////////////
//Temp comp. Start
////////////////////////////////////////////////////////////////////////////////////////////
FP64 DoTempCompensation(FP64 Wmeasure, FP64 CurrentTemp)
{
   FP64 Ws;
   FP64 Wz;
   FP64 ret;
   static INT8U IniFlag = 0;

#ifndef USE_TEMP_COMP
   return Wmeasure;
#endif
   
#ifdef USE_TEMP_COMP_RECAL
#ifdef WEIGHT_CAPA_10G 
   //ReCalibration
   FP64 ReCalTemp = 527822;//522549;    //mag temp
   FP64 ReCalWz = 2522536075;//2522428847;//1000g zero
   FP64 ReCalWs = 3995362704;//3995306901;//1000g zpan
#else
   //ReCalibration
   FP64 ReCalTemp = 527822;//522549;    //mag temp
   FP64 ReCalWz = 2522536075;//2522428847;//1000g zero
   FP64 ReCalWs = 3995362704;//3995306901;//1000g zpan
#endif

   FP64 dWs = (CalFactor.Temp_a0 * ReCalTemp + CalFactor.Temp_a1) * ReCalTemp + CalFactor.Temp_a2;
   dWs = dWs - ReCalWs;
   FP64 dWz = (CalFactor.Temp_b0 * ReCalTemp + CalFactor.Temp_b1) * ReCalTemp + CalFactor.Temp_b2; 
   dWz = dWz - ReCalWz;

   Ws =(CalFactor.Temp_a0 * CurrentTemp + CalFactor.Temp_a1) * CurrentTemp + CalFactor.Temp_a2 - dWs; 
   // span estimation @ current temp.
   Wz =(CalFactor.Temp_b0 * CurrentTemp + CalFactor.Temp_b1) * CurrentTemp + CalFactor.Temp_b2 - dWz; 
   // zero estimation @ current temp.
   
#else //#ifdef USE_TEMP_COMP_RECAL
   
#ifdef USE_TEMP_COMP_1ST_ORDER
   if ( CurrentTemp >= CalFactor.MidTemp )
   {
        Ws = (CurrentTemp - CalFactor.MidTemp)  * (CalFactor.HighSpan - CalFactor.MidSpan) /  (CalFactor.HighTemp - CalFactor.MidTemp) + CalFactor.MidSpan; 
        Wz = (CurrentTemp - CalFactor.MidTemp)  * (CalFactor.HighZero - CalFactor.MidZero) / (CalFactor.HighTemp - CalFactor.MidTemp) + CalFactor.MidZero;
    }
    else
    {
        Ws = (CurrentTemp - CalFactor.MidTemp)  * (CalFactor.LowSpan - CalFactor.MidSpan) /  (CalFactor.LowTemp - CalFactor.MidTemp) + CalFactor.MidSpan; 
        Wz = (CurrentTemp - CalFactor.MidTemp)  * (CalFactor.LowZero - CalFactor.MidZero) / (CalFactor.LowTemp - CalFactor.MidTemp) + CalFactor.MidZero;
    }
#else   
   Ws =(CalFactor.Temp_a0 * CurrentTemp + CalFactor.Temp_a1) * CurrentTemp + CalFactor.Temp_a2; 
   // span estimation @ current temp.
   Wz =(CalFactor.Temp_b0 * CurrentTemp + CalFactor.Temp_b1) * CurrentTemp + CalFactor.Temp_b2; 
   // zero estimation @ current temp.
   
   Ws = CalFactor.Temp_Ws + (Ws - CalFactor.Temp_Ws) * TEMP_COMP_RATE_SPAN;
   Wz = CalFactor.Temp_Wz + (Wz - CalFactor.Temp_Wz) * TEMP_COMP_RATE_ZERO;
#endif   
   
#endif //#ifdef USE_TEMP_COMP_RECAL
   ret = CalFactor.Temp_Wz + (Wmeasure - Wz)*(CalFactor.Temp_Ws - CalFactor.Temp_Wz)/(Ws -Wz);
    
   //ret = Wmeasure + (ret - Wmeasure) * TEMP_COMP_RATE;//보상 수준
      
   if(Ws!=Wz) return ret; 
   else return 0;     // error : divide by zero
}

INT8U TempCompOnFlag = 1;//Default on //off for test

INT32U AdTempComp(INT32U rawData, INT32U magTemp)
{
	FP64 ret;

#ifdef USE_TEMP_COMP
    if  (TempCompOnFlag == 1)
    {
        ret = DoTempCompensation(rawData, magTemp);
    }
    else ret = rawData;
#else
    ret = rawData;
#endif
	return ret;
	//
//	FP64 tempCompdZeroAd;
//
//	FP64 magTempHigh = 493834;
//	FP64 magTempHighZeroAd = 1410070311;
//
//	FP64 magTempMid = 527968;
//	FP64 magTempMidZeroAd = 1409850292;
//	FP64 magTempLow = 567525;
//	FP64 magTempLowZeroAd = 1409678472;
//
//	FP64 magdTempM2H = magTempMid - magTempHigh;
//	FP64 magTempdZeroAdM2H = magTempMidZeroAd - magTempHighZeroAd;
//
//	FP64 magdTempM2L = magTempMid - magTempLow;
//	FP64 magTempdZeroAdM2L = magTempMidZeroAd - magTempLowZeroAd;
//
//	FP64 magdTempM2Curr = magTempMid - (FP64)magTemp;
//	
//	if (magTemp > magTempHigh && magTemp <= magTempMid)
//	{
//		tempCompdZeroAd = magdTempM2Curr * (magTempdZeroAdM2H/magdTempM2H);
//	}
//	else
//	{
//		tempCompdZeroAd = magdTempM2Curr * (magTempdZeroAdM2L/magdTempM2L);
//	}
//
//#ifdef USE_TEMP_COMP
//	return rawData + tempCompdZeroAd;
//#else
//	return rawData;
//#endif
}
////////////////////////////////////////////////////////////////////////////////////////////
//Temp comp. End
////////////////////////////////////////////////////////////////////////////////////////////

INT32U SpanCalWeightZeroRawAdData=0;
INT32U SpanCalWeightSpanRawAdData=0;
INT32U SpanCalWeightSpanMagTempAdData=0;
INT32U SpanCalWeightZeroMagTempAdData=0;
INT8U SpanCalWeightCompOnFlag=0;
FP64 CalIntWeightCompRatio = 1;
FP64 CalIntWeightCompZero;

INT8U IntCalWeightCompSetWeight(INT8U mode)
{
#ifdef USE_INT_WEIGHT_SPAN_CAL            
    FP64 calWeightSpan;
    FP64 rawDatacomped;
	FP64 calWeightTempCompedSpan;
	FP64 calWeightTempCompedZero;
	FP64 calWeightLinCompedSpan;
	FP64 calWeightLinCompedZero;
	FP64 calWeightMaxCapaSpan;
	FP64 calSpan;
	FP64 calMax;
	FP64 calZero;

	if (SpanCalWeightSpanRawAdData != 0 && SpanCalWeightZeroRawAdData != 0)
    {
            if  (TempCompOnFlag == 1) 
            {
                calWeightTempCompedSpan = DoTempCompensation(SpanCalWeightSpanRawAdData , SpanCalWeightSpanMagTempAdData);
            }
            else
            {
                calWeightTempCompedSpan = SpanCalWeightSpanRawAdData;
            }
			calWeightLinCompedSpan = AdLinearCompProc(calWeightTempCompedSpan);
            if  (TempCompOnFlag == 1) 
            {
                calWeightTempCompedZero = DoTempCompensation(SpanCalWeightZeroRawAdData , SpanCalWeightZeroMagTempAdData);
            }
            else
            {
                calWeightTempCompedZero = SpanCalWeightZeroRawAdData;
            }
			calWeightLinCompedZero = AdLinearCompProc(calWeightTempCompedZero);
            calWeightSpan = calWeightLinCompedSpan - calWeightLinCompedZero;

			calWeightMaxCapaSpan = (calWeightSpan * Disp_W_data[RAWDATANUM - 1]) / INT_WEIGHT_VALUE;
			calWeightMaxCapaSpan = AdLinearCompProcRev(calWeightMaxCapaSpan + calWeightLinCompedZero);
			calMax = AdLinearCompProcRev(Disp_W_data[RAWDATANUM - 1] + calWeightLinCompedZero);
			calZero = AdLinearCompProcRev(Disp_W_data[0] + calWeightLinCompedZero);
			calSpan = calMax - calZero;
			CalIntWeightCompRatio = calSpan / (calWeightMaxCapaSpan - calZero);

			CalIntWeightCompZero = calWeightTempCompedZero;
			SpanCalWeightCompOnFlag = 1;//on
	}
    return 0;
#else
    return 0;
#endif
}

INT32U IntCalWeightCompProc(INT32U rawData)
{
#ifdef USE_INT_WEIGHT_SPAN_CAL    
    FP64 rawDatacomped;

    if (SpanCalWeightCompOnFlag == 1 && CalIntWeightCompRatio != 0)
    {
			rawDatacomped = rawData;
			rawDatacomped = (rawDatacomped - CalIntWeightCompZero) * CalIntWeightCompRatio;
            rawData = rawDatacomped + CalIntWeightCompZero;
    }
    return rawData;
#else
    return rawData;
#endif   
}
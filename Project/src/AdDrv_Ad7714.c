#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "globals.h"
#include "AdDrv.h"
#include "ringbuf.h"
#include "timer.h"

#ifdef AD_DRV_AD7714


/*
********************************************************************************
*                       INCLUDES
********************************************************************************
*/
/* Insert include files here */
/** @brief Loadcell AD 값을 넣을 Ring Buffer */
RING_BUF AdDrvRingBuf;

/** @brief AD 값을 넣을 Ring Buffer */
RING_BUF *adRingBufPtr;
/*
********************************************************************************
*                       GLOBAL(EXPORTED) VARIABLES & TABLES
********************************************************************************
*/
/* Insert global variables & tables here */


/*
********************************************************************************
*                       LOCAL DEFINITIONS & MACROS
********************************************************************************
*/

//#define AD_READ_COMMAND 0x5C
#define AD_READ_COMMAND 0x58
/** @brief Channel 1(L/C) Ad 값을 넣을 buffer size */
#define AD_BUF_SIZE		200

/** @brief Channel 2(Battery) Ad 값을 넣을 buffer size */
//#define AD_BUF_SIZE_FOR_BATT	10

#define AD7714_AD_FREQ		10L

#define	FCLKAD			2457600	// AD Clock Freq is 2.4576 MHz
#define	ADCUTOFF		10

/*
********************************************************************************
*                       LOCAL DATA TYPES & STRUCTURES
********************************************************************************
*/
/* Insert local typedef & structure here */


/*
********************************************************************************
*                       GLOBAL(FILE SCOPE) VARIABLES & TABLES
********************************************************************************
*/
/* Insert file scope variable & tables here */
/** @brief Loadcell AD 값을 넣을 Ring Buffer에서 사용할 byte array */

INT32U adDrvData[AD_BUF_SIZE];
/*
********************************************************************************
*                       LOCAL FUNCTION PROTOTYPES
********************************************************************************
*/
//static void writeAdData(INT8U *temp_data);
static INT32U ad7714ReadAdValue(void);

/*
********************************************************************************
*                       GLOBAL(EXPORTED) FUNCTIONS
********************************************************************************
*/
/* Insert global functions here */
/**
********************************************************************************
* @brief    AD Sampling Rate 반환 함수\n
* @param    none
* @return   Sampling rate
* @remark   
********************************************************************************
*/
INT16U AdDrvGetSamplingRate(void)
{
	return (INT16U)AD7714_AD_FREQ;
}

/**
********************************************************************************
* @brief    AD Driver command send 함수\n
* @param    none
* @return   none
* @remark   
********************************************************************************
*/
void ad7714_WriteByte(INT8U data) 
{ 
   INT8U i; 

 
   for(i=0; i<8 ;i++)
	{
        AD_CLK_LOW;
        delay16Clock();
        delay16Clock();
        delay16Clock();
        if (data & (0x80))
            AD_DATA_IN_HIGH;
		else
            AD_DATA_IN_LOW;
        data <<=1;
        
        AD_CLK_HIGH;
        delay16Clock();
        delay16Clock();
        delay16Clock();
	}
   AD_CLK_LOW;
} 


void ADSelfCal(void)
{
   ad7714_WriteByte(0x14);	// write com reg
   ad7714_WriteByte(0x20);	// selfcal, gain 1, BO current No, Fiter start

}

void ADBackGroundCal(void)
{
   ad7714_WriteByte(0x14);	// write com reg
   ad7714_WriteByte(0xA0);	// selfcal, gain 4, BO current No, Fiter start

}

void ADSystemCal(void)
{
   ad7714_WriteByte(0x14);	// write com reg
   ad7714_WriteByte(0x60);	// OffsetCal, gain 4, BO current No, Fiter start
}
void ADZeroCal(void)
{
   ad7714_WriteByte(0x14);	// write com reg
   ad7714_WriteByte(0x40);	// OffsetCal, gain 4, BO current No, Fiter start
}

void ADZeroSelfCal(void)
{
   ad7714_WriteByte(0x14);	// write com reg
   ad7714_WriteByte(0xC0);	// OffsetCal, gain 4, BO current No, Fiter start
}

void ADoffsetCal(void)
{
   ad7714_WriteByte(0x14);	// write com reg
   ad7714_WriteByte(0x80);	// OffsetCal, gain 4, BO current No, Fiter start
}

/**
********************************************************************************
* @brief    AD7714 filter setting 함수\n
* @param    none
* @return   none
* @remark   초기에 실행 되는 함수
********************************************************************************
*/

void AD7714_FilterSet(void)
{
   INT32U wCode;	// 2457600/128/10 = 0111 10000000B 19 < wCode < 4000
   INT8U wCodeLow;
   INT8U wCodeHigh;
   
   wCode = (INT32U) (FCLKAD / 128 / ADCUTOFF);	// calculate reg value for cutoff frq
   
   if (wCode <= 19 || wCode >= 4000)
   {
       return;
   }
   wCodeLow  = (INT8U)(wCode & 0x000000FF);
   wCodeHigh = (INT8U)((wCode & 0x00000F00)>>8);


   ad7714_WriteByte(0x24);	// write com reg
   ad7714_WriteByte(wCodeHigh|0xC0);	// unipolar, 24bit word, BST reduce, A version,Filter high 0111
   
   ad7714_WriteByte(0x34);	// write com reg  next is filter low and channel is ch
   ad7714_WriteByte(wCodeLow);	// write Filter Low reg


   
   
}

/**
********************************************************************************
* @brief    AD Driver Init 함수\n
*           AD Ring Buffer Init\n
*           AD Chip Init
* @param    none
* @return   none
* @remark   초기에 실행 되는 함수
********************************************************************************
*/
void Ad7714DrvInit(void)
{
    
    AD_RESET_LOW;
    delay4Clock();
    delay4Clock();
    
    AD_RESET_HIGH;
    delay4Clock();
    delay4Clock();
    delay4Clock();
    delay4Clock();
    
    TimerSetDelay10ms(TIMER_DELAY_10MS_DELAY,15);
    AD_CLK_HIGH;
    while(!TimerCheckDelay10ms(TIMER_DELAY_10MS_DELAY));
    AD_CLK_LOW;
    TimerSetDelay10ms(TIMER_DELAY_10MS_DELAY,50);
    while(!TimerCheckDelay10ms(TIMER_DELAY_10MS_DELAY));
    
    ad7714_WriteByte(0xFF);
    ad7714_WriteByte(0xFF);
    ad7714_WriteByte(0xFF);
    ad7714_WriteByte(0xFF);
    ad7714_WriteByte(0xFF);

    ad7714_WriteByte(0x10);	//Mode Register set, Ch AIN1(+) AIN6(-) Pseudo Differential
    delay16Clock();
    delay16Clock();
    
    ad7714_WriteByte(0x00);	//Normal mode Gain 1 Burnout Current set 0 Filter Sync 0.
    delay16Clock();
    delay16Clock();
    ad7714_WriteByte(0x20);	//Filter High Register set, Ch AIN1(+) AIN6(-) Pseudo Differential
    delay16Clock();
    delay16Clock();
    ad7714_WriteByte(0xC7);	//Unipolar, Word Length 24 bit,
    delay16Clock();
    delay16Clock();
    ad7714_WriteByte(0x30);	//Filter Low Register set, Ch AIN1(+) AIN6(-) Pseudo Differential
    delay16Clock();
    delay16Clock();
    ad7714_WriteByte(0xA1);	//Filter Low Register value.
    delay16Clock();
    delay16Clock();
    ad7714_WriteByte(0x60);	//Zero-Scale Calibration set, Ch AIN1(+) AIN6(-) Pseudo Differential   ZS Cal on AIN
    delay16Clock();
    delay16Clock();
    ad7714_WriteByte(0x37);	//Filter Low Register, Ch AIN6(+) AIN6(-) TEST MODE
    delay16Clock();
    delay16Clock();
    ad7714_WriteByte(0x84);	//Filter Low Register value.
    delay16Clock();
    delay16Clock();
    ad7714_WriteByte(0x27);	//Filter High Register set, Ch AIN6(+) AIN6(-) TEST MODE
    delay16Clock();
    delay16Clock();
    ad7714_WriteByte(0x70);	//Word Length 24 bit Current Boost 1, Zero 1 (why? datasheet must be set 0)
    delay16Clock();
    delay16Clock();
    ad7714_WriteByte(0x62);	//Zero-Scale Calibration Register set, Ch AIN3(+) AIN6(-) Pseudo Differential   ZS Cal on AIN 
    delay16Clock();
    delay16Clock();
    ad7714_WriteByte(0x9B);	//??????
    delay16Clock();
    delay16Clock();
    ad7714_WriteByte(0x04);	//???????
    delay16Clock();
    delay16Clock();
    while(AD_DRDY);
    ad7714_WriteByte(AD_READ_COMMAND);
    
}

void AdDrvInit(void)
{
	RingBufInit(&AdDrvRingBuf, (char *)(&adDrvData[0]), AD_BUF_SIZE);
	adRingBufPtr = &AdDrvRingBuf;
	
	Ad7714DrvInit();
}

/**
********************************************************************************
* @brief    AD Driver Procedure\n
*           AD Chip에서 읽은 값을 Ring Buffer에 저장
* @param    none
* @return   none
* @remark   일정 시간에 한번씩 계속 실행 되는 함수\n
*           AD Chip의 Sampling 시간 보다는 빨리 실행 되어야 함
********************************************************************************
*/

void AdDrvProc(void)
{
	INT32U adData = 0;
        
	adData = ad7714ReadAdValue();

	if(adData) 
	{
		 RingBufPutInt32sForAdDrv(&AdDrvRingBuf, adData);
	}

}



/*****************************************************************************/
/*
********************************************************************************
*                       LOCAL FUNCTIONS
********************************************************************************
*/
/* Insert local functions here */
/*
*
*
*
*
*/

/*
******************************************************************************
* @brief    ad 컨버팅된 Data를 읽는 함수 \n
*
* @param    none
* @return   none
* @remark   연속적으로 AD 변환됨 
********************************************************************************
*/

INT32U ad7714ReadAdValue(void)
{
    INT8U  i;
    
    INT32U  adsData;
	
	adsData = 0x00000000;
    
    

	if(AD_DRDY)	return 0;
    
	ad7714_WriteByte(AD_READ_COMMAND);
    AD_CLK_LOW;
    delay16Clock();
    delay16Clock();
    delay16Clock();
    delay16Clock();
    delay16Clock();
    delay16Clock();
    delay16Clock();
    delay16Clock();
	for(i=0; i<24; i++)
    {
		adsData <<= 1;
        AD_CLK_LOW;
		delay16Clock();
        delay16Clock();
         AD_CLK_HIGH;
        delay16Clock();
        delay16Clock();
        delay16Clock();
       
        if(AD_DATA_OUT)
		{
			adsData++;
		}
        
        delay16Clock();
        delay16Clock();
	}
    AD_CLK_LOW;

	return adsData;	

}

#endif //AD_DRV_AD7714

#include "globals.h"
#include "AdApi.h"
#include "Adm_def.h"
#include "Adm_var.h"
#include "AdDrv.h"
#include "RingBuf.h"
#include "filter.h"
/*filter setting and cal setting 
   각 FILTER를 비활성화 하기 위해서는 define 을 disable한다 */ 
//#define KELFILTER_1ST_ENABLE          
//#define KELFILTER_2ST_ENABLE
//#define MOVINGAVG_1ST_ENABLE
//#define ZEROTRACKING_ENABLE

/*filter setting and cal setting  END*/


/* moving avarage의 윈도우 싸이즈를 정하는 define 
    moving avarage 2차이후를 정하기 위해서는 Wc_filterInit함수에 init을 꼭 하고 함수를 만들어야 한다.  */





long RawAd;

long AdBuf[NUM_ADBUFFER];

void AdcInit(void)
{

	AdDrvInit();
    TempAdDrvInit();
    
#ifdef USE_MAFS_FILTER
#ifdef WEIGHT_CAPA_10G  
//    MAFSInit(10*MAFS_SRATE, 20*MAFS_SRATE, 5000*MAFS_CAL_COFF, 10000*MAFS_CAL_COFF, 1*MAFS_SRATE, 2*50000000*MAFS_CAL_COFF, 50000000*MAFS_CAL_COFF);  //shock off
    MAFSInit(10*MAFS_SRATE, 20*MAFS_SRATE, 5000*MAFS_CAL_COFF, 10000*MAFS_CAL_COFF, 1*MAFS_SRATE, 2*50000000*MAFS_CAL_COFF, 50000000*MAFS_CAL_COFF);  //shock off
#else    
//void MAFSInit(INT8U winMinSize, INT8U winMaxSize, INT32S winIncThres, INT32S winBreakThres, INT8U shockWinSize, INT32S shockThres, INT32S thresIncThres)
    MAFSInit(1*MAFS_SRATE, 20*MAFS_SRATE, 300*MAFS_CAL_COFF, 600*MAFS_CAL_COFF, 1*MAFS_SRATE, 2*50000000*MAFS_CAL_COFF, 50000000*MAFS_CAL_COFF);  //shock off
    //MAFSInit(10*MAFS_SRATE, 20*MAFS_SRATE, 5000*MAFS_CAL_COFF, 10000*MAFS_CAL_COFF, 1*MAFS_SRATE, 2*50000000*MAFS_CAL_COFF, 50000000*MAFS_CAL_COFF);  //shock off, KRISS test
#endif
#endif        
}


INT32U AdGetRawData(void)
{
    INT32U rawData;
    
    rawData = 0;
    
    AdDrvProc();

    /*
    if(RingBufCheckBuf(adRingBufPtr)) //kevin : 따라서 32비트 신호가 나오기 시작하면 adRingBufPtr 값이 무언가가 있기에 여기로 진입한다.
    {
        rawData = RingBufGetInt32u(adRingBufPtr); //kevin : 위에까지 컨버팅되고, 버퍼에 저장되어있는 데이터를 rawData 에 넣어준다.
    }
    */
    
    return rawData; //kevin : 저장된 rawData 를 리턴
}

INT32U AdNoFilteredRawData;

INT32U AdGetFilterdRawData(void)
{

    INT32U rawData;
    
    rawData = 0;
    
    AdDrvProc(); //kevin : 최종 변환된 AD 32비트 AD 신호가 &AdDrvRingBuf 주소값에 넣어진다. 참고로 이니셜 할 때 adRingBufPtr = &AdDrvRingBuf; 였음

    if(RingBufCheckBuf(adRingBufPtr)) //kevin : 따라서 32비트 신호가 나오기 시작하면 adRingBufPtr 값이 무언가가 있기에 여기로 진입한다.
    {
        rawData = RingBufGetInt32u(adRingBufPtr); //kevin : 위에까지 컨버팅되고, 버퍼에 저장되어있는 데이터를 rawData 에 넣어준다.
#ifdef AD_DRV_AD7714     
        rawData =  rawData << 7; //8 bit shift
#endif
        //GPIO_ToggleBits(GPIOA,GPIO_Pin_2); // kevin : 하드웨어상 LED 와 연결되어있다. (컨버젼할때마다 LED를 깜빡이고 싶었나보다.

        AdNoFilteredRawData = rawData;//For debug


#ifdef KELFILTER_1ST_ENABLE
        rawData = (INT32U)WC300_KalmanFilter_1st(rawData);
#endif

#ifdef KELFILTER_2ST_ENABLE
        rawData = (INT32U)WC300_KalmanFilter_2nd(rawData);
#endif        

#ifdef MOVINGAVG_1ST_ENABLE
        rawData = WC300_MovingAVG(&Maf1stOrder, rawData);
#endif         

#ifdef USE_MAFS_FILTER
        rawData = MAFSProc(rawData);
#endif        
    }
    
    return rawData; //kevin : 저장된 rawData 를 리턴
}

extern INT32U IntCalWeightCompProc(INT32U rawData);

INT32S AdNormProc(INT32U rawData)
{
    INT32S normData = 0;
        
    if (rawData)
    {
#ifdef USE_INT_WEIGHT_SPAN_CAL            
        rawData = IntCalWeightCompProc(rawData);
#endif
        
#ifdef USE_LINEAR_COMP
        normData = AdLinearCompProc(rawData);
#else
        normData = rawData;
#endif
    }

    //normData = (INT32U)WC300_MovingAVG(&Maf1stOrder,normData);
    
    return normData;
}

#define USE_TEMP_SENSOR_AVG
#define TEMP_SENSOR_AVG_COUNT   20
INT32U AdGetMagTempAd (void)
{
    INT16U i;
    INT32U rawSumData;
    INT32U rawData;
    
    rawData = 0;
    TempAdDrvProc();
    
    if(RingBufCheckBuf(MagTempAdRingBufPtr))
    {
        
#ifdef USE_TEMP_SENSOR_AVG      
        rawSumData = TEMP_SENSOR_AVG_COUNT / 2;
        for ( i = 0; i < TEMP_SENSOR_AVG_COUNT; i++)
        {
              rawSumData = rawSumData + RingBufGetInt32sPrevData(MagTempAdRingBufPtr, i);
        }
        rawData = rawSumData / TEMP_SENSOR_AVG_COUNT;
#else
        rawData = RingBufGetInt32u(MagTempAdRingBufPtr);
#endif        
    }
    
    return rawData;

}

INT32U AdGetBlockTempAd (void)
{

    INT32U rawData;
    rawData = 0;
    TempAdDrvProc();
    
    if(RingBufCheckBuf(BlockTempAdRingBufPtr))
    {
        rawData = RingBufGetInt32u(BlockTempAdRingBufPtr);
    }
    
    return rawData;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/** @brief MAF 버퍼 사이즈*/
#define MAF_BUF_SIZE	6000//600

/** @brief MAF 버퍼*/
 INT32U MAFBuf[MAF_BUF_SIZE];
/** @brief MAF 버퍼 인덱스*/
 INT16U MAFBufIndex;
/** @brief MAF 윈도우 사이즈*/
 INT16U MAFWinSize;
/** @brief MAF 윈도우 max 사이즈*/
 INT16U MAFWinMaxSize;
/** @brief MAF 윈도우 min 사이즈*/
 INT16U MAFWinMinSize;
/** @brief MAF 윈도우를 Break하는 경계 수치*/
 INT32U MAFWinBreakThres;
/** @brief MAF 윈도우를 증가시키는 경계 수치*/
 INT32U MAFWinIncThres;
/** @brief MAF output 값*/
 INT32U MAFfilteredData;
/** @brief MAF 원도우 증가, Break 기준 값*/
 INT32U MAFfilterRefData;
/** @brief MAF shock 판정 기준값 */
 INT32U MAFshockRefData;
/** @brief MAF shock 판정 크기 기준값 */
 INT32U MAFshockThres;
/** @brief MAF shock 판정 크기, MAF window break 기준값 증가의 기준값*/
 INT32U MAFThresIncThres;
/** @brief MAF shock 판정 윈도우 크기 */
 INT16U MAFshockWinSize;
/** @brief MAF shock 개수 변수 */
 INT16U MAFshockCount;

 INT8U MAFStableFlag;
 INT8U StableSendDetactFlag;
 INT8U StableCalDetactFlag;
/*
********************************************************************************
* @brief    INT32S 절대값 반환\n
* @param    num : 입력값
* @return   절대값
* @remark   
********************************************************************************
*/
FP64 AbsFp64(FP64 num)
{
	if (num > 0)
	{
		return (num);
	}
	else
	{
		return (-num);
	}	
}


/**
********************************************************************************
* @brief    Moving Average Filter with removing Shock Init 함수\n
* @param    winMinSize : window 최소 크기
*           winMaxSize : window 최대 크기 <= MAF_BUF_SIZE
*           winIncThres : window를 증가 시킬 조건
*           winBreakThres : window size를 최소화할 조건
*           shockWinSize : shock 판정 window 크기
*           shockThres : shock 판정 크기
*			thresIncThres : winIncThres, winBreakThres, shockThres를 2배로 키울 경계
* @return   none
* @remark   초기에 한번 실행
********************************************************************************
*/
void MAFSInit(INT16U winMinSize, INT16U winMaxSize, INT32U winIncThres, INT32U winBreakThres, INT16U shockWinSize, INT32U shockThres, INT32U thresIncThres)
{
	INT16U i;

	MAFWinMinSize = winMinSize;
	MAFWinMaxSize = winMaxSize;
	MAFWinIncThres = winIncThres;
	MAFWinBreakThres = winBreakThres;

	MAFBufIndex = 0;
	MAFWinSize = MAFWinMinSize;
	MAFfilteredData = 0;
	MAFfilterRefData = 0;
	MAFshockRefData = 0;
	MAFshockCount = 0;
	MAFshockWinSize = shockWinSize;
	MAFshockThres = shockThres;
	MAFThresIncThres = thresIncThres;

	for (i = 0; i < MAF_BUF_SIZE; i++)
	{
		MAFBuf[i] = 0;
	}
    
    MAFStableFlag = 0;
    StableSendDetactFlag = 0;
    StableCalDetactFlag = 0;
}

/**
********************************************************************************
* @brief    Moving Average Filter with removing Shock 에 데이터를 입력하는 함수\n
* @param    inData : AD 입력값
* @return   Filering된 값
* @remark   
********************************************************************************
*/
//#define USE_MAFS_SHOCK_FILTER
INT32U MAFSProc(INT32U inData)
{
	INT16U i;
	FP64 sum;
	INT16U bufIndex, currIndex;
	INT32U minFilteredData;
	INT32U prevMinFilteredData;
	FP64 diffData;
	INT8S sign;
	static INT8S oldSign;
	INT32U shockThres;
	INT32U winBreakThres;
	INT32U winIncThres;
        INT32U prevStableSendSum;
#ifdef FAST_BREAK_SHOCK_FILTER
	INT32U shockBreakThres;//shock filter를 적용하지 않을 경계치(값 즉시 반영됨)
#endif

	MAFBufIndex++;
	MAFBufIndex = MAFBufIndex % MAF_BUF_SIZE;

	diffData = (FP64)inData - (FP64)MAFshockRefData; 

	if (inData < MAFThresIncThres) //3가지 경계값 증가 경계치 내이면 그대로
	{
		shockThres = MAFshockThres;
		winBreakThres = MAFWinBreakThres;
		winIncThres = MAFWinIncThres;
	}
	else //밖이면 3가지 경계치 증가 (질량이 클수록 노이즈도 증가 방어)
	{
		shockThres = MAFshockThres + MAFshockThres;
		winBreakThres = MAFWinBreakThres + MAFWinBreakThres;
		winIncThres = MAFWinIncThres + MAFWinIncThres;
	}

//--------------------------------------//
// Shock Filter
//--------------------------------------//
#ifdef FAST_BREAK_SHOCK_FILTER
	shockBreakThres = 128 * shockThres;//about 200d	-> Magic Number 변경할 것
#endif

#ifdef USE_MAFS_SHOCK_FILTER        
	if (AbsFp64(diffData) > shockThres)//AD 변화량이 Shock 경계치 밖이면(Shock 이면)
#else
	if (0)            
#endif//#ifdef USE_MAFS_SHOCK_FILTER                    
	{
#ifdef FAST_BREAK_SHOCK_FILTER
		if (AbsFp64(diffData) > shockBreakThres)//AD 변화량이 상당히 크면(무게 변화로 감지)
		{
			MAFshockCount = MAFshockWinSize;//Shock 필터 중지
		}
#endif
		if (diffData > 0) sign = 1;
		else if (diffData < 0) sign = -1;
		else sign = 0;

		MAFshockCount++;

		if (MAFshockCount > MAFshockWinSize)//Shock 개수가 shock window보다 크면(실제 질량이면)
		{
			MAFshockCount = MAFshockWinSize;
			currIndex = MAFBufIndex;//Shock 필터 중지
		}
		else
		{
			if (MAFshockCount != 1 && oldSign != sign) //Shock 필터 동작 중 AD 변화량의 부호가 바뀌면 Shock 필터 계속 동작
			{
				MAFBufIndex = (MAFBufIndex + MAF_BUF_SIZE - (MAFshockCount - 1)) % MAF_BUF_SIZE;
				MAFshockCount = 1;
				currIndex = (MAFBufIndex + MAF_BUF_SIZE - 1) % MAF_BUF_SIZE;
			}
			else //AD 변화량의 부호 변경이 없고
			{
				if (MAFshockCount == MAFshockWinSize) //Shock 개수가 shock window에 도달하면 Shock 필터 중지
				{
					MAFBufIndex = (MAFBufIndex + MAF_BUF_SIZE - MAFshockCount) % MAF_BUF_SIZE;
					currIndex = MAFBufIndex;
				}
				else //Shock 개수가 shock window보다 작으면 Shock 필터 동작
				{
					currIndex = (MAFBufIndex + MAF_BUF_SIZE - MAFshockCount) % MAF_BUF_SIZE;
				}
			}
		}
		oldSign = sign;
	}
	else //AD 변화량이 Shock 경계치 안이면(Shock이 아니면)
	{
		if (MAFshockCount > 0 && MAFshockCount < MAFshockWinSize) //바로전 shock AD 값이 있으면 skip
		{
		 	MAFBufIndex = (MAFBufIndex + MAF_BUF_SIZE - MAFshockCount) % MAF_BUF_SIZE;
		}
		MAFshockCount = 0;
		currIndex = MAFBufIndex;
	}

	MAFBuf[MAFBufIndex] = inData;

 
//--------------------------------------//
// Moving Average Filter
//--------------------------------------//
	sum = MAFWinMinSize / 2;
	for (i = 0; i < MAFWinMinSize; i++)
	{
		bufIndex = (currIndex + MAF_BUF_SIZE - i) % MAF_BUF_SIZE;
		sum = sum + MAFBuf[bufIndex];
	}
	minFilteredData = sum / MAFWinMinSize; //현재 최소 범위 평균값

	sum = MAFWinMinSize / 2;
	for (i = 0; i < MAFWinMinSize; i++)
	{
		bufIndex = (currIndex + MAF_BUF_SIZE - MAFWinMinSize - i) % MAF_BUF_SIZE;
		sum = sum + MAFBuf[bufIndex];
	}
	prevMinFilteredData = sum / MAFWinMinSize; //전 최소 범위 평균값

	diffData = AbsFp64((FP64)minFilteredData - (FP64)prevMinFilteredData);

	if (diffData < winBreakThres) //AD 변화량이 break 범위 안이면 평균 버퍼 크기 유지
	{
		if (diffData < winIncThres)	//AD 변화량이 increase 범위 안이면 평균 버퍼 크기 증가
		{
			if (MAFshockCount == 0) 
			{
				MAFWinSize++;
			}
			if (MAFWinSize > MAFWinMaxSize)
			{
				MAFWinSize = MAFWinMaxSize;
			}
			MAFshockRefData = MAFfilteredData; //Shock 판정 기준값 설정
		}
	}
	else //AD 변화량이 break 범위 밖이면 평균 버퍼 크기 최소화
	{
		MAFWinSize = MAFWinMinSize/2 + 1;
#ifdef FAST_AVERAGE_IN_LOW_AD //빠른 응답을 위햐 버퍼 크기 더 줄임(무게에 따라)
		if (inData < MAFThresIncThres)
		{
			if (inData < (MAFThresIncThres/2))
			{
				MAFWinSize = MAFWinSize - 2;
			}
			else
			{
				MAFWinSize = MAFWinSize - 1;
			}
			if ((MAFWinSize == 0) || (MAFWinSize > MAF_BUF_SIZE)) MAFWinSize = 1;
		}
#endif
		MAFshockRefData = 0; //Shock 판정 기준값 리셋
	}

	sum = MAFWinSize / 2;
	for (i = 0; i < MAFWinSize; i++)
	{
		bufIndex = (currIndex + MAF_BUF_SIZE - i) % MAF_BUF_SIZE;
		sum = sum + MAFBuf[bufIndex];
	}
	MAFfilteredData = sum / MAFWinSize;  //MAF 에서 정해전 버퍼 크기 만큼 평균한 값 반영


    //for external stable flag
    if (MAFWinSize > MAFWinMinSize) MAFStableFlag = 1;
    else MAFStableFlag = 0;
    /////////////////////////

    //for external stable send & cal flag
	sum = MAFWinSize / 2;
	for (i = 0; i < MAFWinSize; i++)
	{
		bufIndex = (currIndex + MAF_BUF_SIZE - STABLE_SEND_DETACT_TIME - i) % MAF_BUF_SIZE;
		sum = sum + MAFBuf[bufIndex];
	}
	prevStableSendSum = sum / MAFWinSize;  //MAF 에서 정해전 버퍼 크기 만큼 평균한 값 반영
    
	diffData = AbsFp64((FP64)MAFfilteredData - (FP64)prevStableSendSum);

	if (diffData < STABLE_SEND_DETACT_RANGE) 
	{
            StableSendDetactFlag = 1;
    }
    else
    {
            StableSendDetactFlag = 0;
    }

  	if (diffData < STABLE_CAL_DETACT_RANGE) 
	{
            StableCalDetactFlag = 1;
    }
    else
    {
            StableCalDetactFlag = 0;
    }

    ///////////////////////////
    
	return MAFfilteredData;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
INT32S NormAdZeroTrackingData = 0;
extern INT32S normAdAutoZeroTrackingData;

RING_BUF WeightDataRingBuf;
RING_BUF *WeightDataRingBufPtr;
#define ZERO_TRACKING_AD_BUF_SIZE		1000
INT32S weightDataBuf[ZERO_TRACKING_AD_BUF_SIZE];

INT32S AdAutoZeroTrackingProc(INT32S weight_data)
{
    static INT32S prev_weight_data = 0;
    INT32S diff_data;
	INT32S auto_zero_data;

    diff_data = weight_data - prev_weight_data;    
    prev_weight_data = weight_data;
	auto_zero_data = 0;
	if (diff_data < AUTO_ZERO_TRACKING_RANGE && diff_data > -AUTO_ZERO_TRACKING_RANGE)
	{
		if ((weight_data - normAdAutoZeroTrackingData) < AUTO_ZERO_TRACKING_WEIGHT && (weight_data - normAdAutoZeroTrackingData) > -AUTO_ZERO_TRACKING_WEIGHT)
		{
			//auto_zero_data = (AUTO_ZERO_TRACKING_SPEED * (weight_data - normAdAutoZeroTrackingData)) / AUTO_ZERO_TRACKING_RANGE; 
            
            //auto_zero_data = (weight_data - normAdAutoZeroTrackingData)/2;
			//if (auto_zero_data > AUTO_ZERO_TRACKING_RANGE || auto_zero_data < -AUTO_ZERO_TRACKING_RANGE)
			//{
			//	auto_zero_data = (AUTO_ZERO_TRACKING_SPEED * auto_zero_data) / AUTO_ZERO_TRACKING_RANGE; 
			//}
                        
            auto_zero_data = weight_data - normAdAutoZeroTrackingData;
			if (auto_zero_data == 1 || auto_zero_data == -1)
            {
                //auto_zero_data = auto_zero_data;
            }
			else if (auto_zero_data > AUTO_ZERO_TRACKING_RANGE || auto_zero_data < -AUTO_ZERO_TRACKING_RANGE)
			{
				auto_zero_data = (AUTO_ZERO_TRACKING_SPEED * auto_zero_data) / AUTO_ZERO_TRACKING_RANGE; 
			}
            else
            {
                auto_zero_data = auto_zero_data / 2;
            }
		}
	}
#ifdef USE_AUTO_ZERO_TRACKING_PROC
	return auto_zero_data;
#else
	return 0;
#endif
}

INT32S AdZeroTrackingProc(INT32S weight_data)
{
    static INT32S prev_weight_data = 0;
    static INT32S prev_stable_diff_data = 0;
    INT32S diff_data;
	INT32S auto_zero_data;
	static INT8U initFlag = 0;

	if (initFlag == 0)
	{
		initFlag = 1;
		RingBufInit(&WeightDataRingBuf, (char *)(&weightDataBuf[0]), ZERO_TRACKING_AD_BUF_SIZE);
		WeightDataRingBufPtr = &WeightDataRingBuf;
	}

#ifdef USE_ZERO_TRACKING   

	//if(RingBufCheckBuf(WeightDataRingBufPtr) == 0)  return 0;

 //   diff_data = weight_data - prev_weight_data;    
 //   prev_weight_data = weight_data;;
	//auto_zero_data = 0;
	//if (diff_data < ZERO_TRACKING_RANGE && diff_data > -ZERO_TRACKING_RANGE)
	//{
	//	if ((weight_data - NormAdZeroTrackingData) < 1000 && (weight_data - NormAdZeroTrackingData) > -1000)
	//	{
	//		auto_zero_data = (weight_data - NormAdZeroTrackingData) / 5; 
	//	}
	//}
	//return auto_zero_data;
	
	RingBufPutInt32s(WeightDataRingBufPtr, weight_data);

	//diff_data = weight_data - RingBufGetInt32sPrevData(WeightDataRingBufPtr, ZERO_TRACKING_LONG_RANGE_TIME);
	//if (diff_data < -ZERO_TRACKING_LONG_RANGE || diff_data > ZERO_TRACKING_LONG_RANGE) return 0;

	diff_data = weight_data - RingBufGetInt32sPrevData(WeightDataRingBufPtr, 1);
    if (diff_data < ZERO_TRACKING_RANGE && diff_data > -ZERO_TRACKING_RANGE)
    {
        //return (ZERO_TRACKING_SPEED * diff_data) / ZERO_TRACKING_RANGE;
		if (diff_data < ZERO_TRACKING_RANGE/2 && diff_data > -ZERO_TRACKING_RANGE/2)
		{
			return diff_data;
		}
		return diff_data/2;
    }
    return 0;

	/////////////////////////////////
    //diff_data = weight_data - prev_weight_data;    
    //prev_weight_data = weight_data;

    //if (diff_data < ZERO_TRACKING_RANGE && diff_data > -ZERO_TRACKING_RANGE)
    //{
    //    return ZERO_TRACKING_SPEED * (diff_data / ZERO_TRACKING_RANGE);
    //}
    //return 0;
    
//////////////////////////////////

//    
//    if(diff_data > -5 && diff_data < 5)
//    {
//        return diff_data;
//    }
//    return 0;
    
    
//    if (MAFStableFlag == 1)
//    {
//        return diff_data;
//    }
//    else
//    {
//        return 0;
//    }    
    
    if (MAFStableFlag == 1)
    {
        if (diff_data < 3 || diff_data > -3)
        {
            if (prev_stable_diff_data < 3 && prev_stable_diff_data > -3) 
            {
                prev_stable_diff_data += diff_data;
                return 0;
            }
        }
        return diff_data;
    }
    else
    {
//        diff_data += prev_stable_diff_data;
        prev_stable_diff_data = 0;
        return 0;
    }        
    
//    diff_data = weight_data - prev_weight_data;
//    prev_weight_data = weight_data;
//    if(diff_data > -STABLE_RANGE && diff_data < STABLE_RANGE)
//    {
//        return diff_data;
//    }
//    else if(diff_data > -ZERO_TRACKING_RANGE && diff_data < ZERO_TRACKING_RANGE)
//    {
//        return diff_data/2;
//    }
//    else
//    {
//        return 0;    
//    }
#else
    return 0;
#endif
}

INT8U AdAutoZeroKeyProc(INT32S weight_data)
{
#ifdef USE_AUTO_ZERO_KEY            
    static INT8U weightOnFlag = 0;
    
    if (weight_data > AUTO_ZERO_WEIGHT)//10g
    {
        weightOnFlag = 1;
        return 0;
    }
    
    if (weight_data < AUTO_ZERO_WEIGHT && weightOnFlag == 1)//10g
    {
        weightOnFlag = 0;
        return 1;//5;
    }
    else
    {
        return 0;
    }
#else
    return 0;
#endif
}

INT32S  AdZeroDispProc(INT32S weight_data)
{
#ifdef USE_ZERO_DISP_FILTER
    if ((weight_data > -ZERO_DISP_FILT_RANGE) && (weight_data < ZERO_DISP_FILT_RANGE)) //5g
    {
        return  0;
    }
    else
    {
        return weight_data;
    }
#else            
    return weight_data;
#endif
}
  


/*
********************************************************************************
* Copyright (c) 2006 CAS
* @brief   Scale Main Source File\n
* @file    main.c
* @version 1.0
* @date    2018/07/10
* @author  SeungHo Jeong
********************************************************************************
* @remark  [Version History]\n
*          v1.0 2018/07/10 created by HYP \n
********************************************************************************
*/

/*
********************************************************************************
*                       INCLUDES
********************************************************************************
*/
/* Insert include files here */
//kevin20180710 : #include (파일 처리를 위한 전처리문)

#include <stdio.h>
#include <string.h>
#include "globals.h"
#include "arm_math.h"
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_rtc.h"
#include "main.h"
#include "lcd.h"
#include "RingBuf.h"
#include "commbuf.h"
#include "comm_app.h"
#include "timer.h"
#include "eeprom.h"
#include "buzzer.h"
#include "key.h"
#include "AdApi.h"
#include "filter.h"
#include "wc_calfilter.h"
#include "common.h"
#include "function.h"
#include "HardwareConfig.h"


#include "AdDrv.h" //kevin 20180712 :    AD_7177_DATA_IN_HIGH; main 에서 넣어서 테스트 하기위해 잠시 넣어둠

//#include "led.h" //kevin 20180726 : PWM 최초 소스코드를 테스트로 돌려보기위한 임시 헤더파일

/*
********************************************************************************
*                       GLOBAL(EXPORTED) VARIABLES & TABLES
********************************************************************************
*/
/* Insert global variables & tables here */
//kevin20180710 : 사용안함

/*
********************************************************************************
*                       GLOBAL(FILE SCOPE) VARIABLES & TABLES
********************************************************************************
*/
/* Insert file scope variable & tables here */
//kevin20180710 : static (전역 정적 변수)

/*
********************************************************************************
*                       GLOBAL(EXPORTED) FUNCTIONS
********************************************************************************
*/
/* Insert global functions here */
//kevin20180710 : 글로벌 함수의 원형

/*
********************************************************************************
*                       LOCAL DEFINITIONS & MACROS
********************************************************************************
*/
/* Insert #define here */
//kevin20180710 : #define (형태 정의를 위한 전처리문)

//#define TIMER_PRESCALER_FREQ        1000000             // timer 입력 클력 1MHz
#define TIMER_PRESCALER_FREQ        42000000
#define PWM_FREQ                    10000              // PWM 반복 주기 1ms = 1kHz
#define PWM_WIDTH                   (TIMER_PRESCALER_FREQ / PWM_FREQ)

#define DUTY_IDLE                   (PWM_WIDTH / 2)     // 50% duty
#define DUTY_MAX                    (PWM_WIDTH - 1)     // 최대 duty 값.

#define PWM 8400



/*
********************************************************************************
*                       LOCAL DATA TYPES & STRUCTURES
********************************************************************************
*/
/* Insert local typedef & structure here */
//kevin20180710 : typedef (형식 선언)

    //사용중
    static INT32U photodiodeUpAdRawData;
    static INT32S photodiodeUpAd;
    static INT32S photodiodeUpAdLimit;
    
    static INT32U photodiodeDownAdRawData;
    static INT32S photodiodeDownAd;
    static INT32S photodiodeDownAdLimit;
    
    
    static INT32S photodiodeAdSpan;
    static INT32S photodiodeAdSpanBenchmark = 10000000; //kevin : 실제 측정 스판은 18620000 정도
    static FP32 photodiodeAdSpanRate; //kevin : 실제 측정 스판은 18620000 정도
    static INT32S photodiodeCenterAd;
    static INT32S photodiodeCenterErr;
    static INT32S photodiodeLimitMargin;
    static INT32S photodiodeAdLimitSpan;
    //사용중
    
    
    static INT16U eMagnetVoltage;
    static FP32 eMagnetVoltageFloat;
        
    static INT32U slitTargetPosition = 0;//사용중
    static INT32U slitCurrentPosition = 0;
    static INT32S slitCurrentPositionRawData = 0;
        
    static INT32S currentPositionError = 0;
    static INT32S currentPositionError_MAVR = 0;
    static INT32S currentPositionError_MAVRpp = 0;
    static INT32S currentPositionError_MAVRpp_last = 0;
    static INT32S currentPositionError_MAVRpp_u = 0;
    static INT32S currentPositionError_MAVRpp_d = 0;
    static INT32S currentPositionError_MAVRpp_u_last = 0;
    static INT32S currentPositionError_MAVRpp_d_last = 0;
    static INT32S lastPositionError = 0;
    static INT32S lastPositionError_MAVR = 0;
    static INT32S current_MAVR = 0;
    static INT32S last_MAVR = 0;
    static INT64S integralPid = 0;
    static INT32S derivativePid = 0;
    static INT32S pidValue = 0;
    static FP32 ku = 0;
    static FP32 tu = 0;
    static FP32 kp = 0;
    static FP32 ki = 0;
    static FP32 kd = 0;
    static FP32 u = 0;
    static FP32 pwm = 0;
    static INT32U pidCalCount = 0;
    
    static INT32U adRawWeightData = 0;
    static INT32U adAvrWeightData = 0;
    static INT32U adAvrCount = 0;
    
    
    static INT32U testcount = 0; //keivn20180712 : PID 테스트용 카운트

    static INT32S MovAvrCurrnetBufIndex = 0;
    static INT32S MovAvrFilterLength = 100; //무빙 에버러지 겟수
    static INT32S MovAvrFilterBuf[100] = 0;
    
    static INT32S MovAvrCurrnetBufIndex_s = 0;
    static INT32S MovAvrFilterLength_s = 500; //무빙 에버러지 겟수
    static INT32S MovAvrFilterBuf_s[500] = 0;
    
    static INT8U revChar = 0;

/*
********************************************************************************
*                       LOCAL FUNCTION PROTOTYPES
********************************************************************************
*/
/* Insert static function prototypes here */
//kevin20180710 : 사용안함
    INT32S movingAverageFilter(INT32S rawAdDataIn);

/*
********************************************************************************
*                       MAIN  ROUTINE
********************************************************************************
*/
/* Insert main routine here */

char rawAdDataASCII[20];

 int main(void)
{
    // SysTick end of count event each 10ms
    SystemCoreClockUpdate();
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);
    
    // Enable Clock Security System(CSS): this will generate an NMI exception when HSE clock fails
    RCC_ClockSecuritySystemCmd(ENABLE);
    RCC_GetClocksFreq(&RCC_Clocks);
    Rcc_clock_init();
    
    //진짜 해야할것
    Gpio_init();  // 하드웨어
    Timer2_init(); // 타이머
    Usart1_init();  // 커뮤니케이션 
    Usart3_init();  // 커뮤니케이션
    Nvic_init(); //하드웨어
    Commbuf_Init();
    SPI_Config(); //커뮤니케이션 
    TimerInit();
    AdcInit();
    EepromInit();
    EepromCalDataRead();

    //PID 이니셜 (포토다이오드 Up/Down 값 측정) >> Pid.h 와 Pid.c 를 만드는 편이 좋겠다
    //함수 이름은 PidIitial();


//(1-1Start)FullDigitalPID with PWM 하면서 생긴 main() 소스
    uint16_t PrescalerValue;
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_OCInitTypeDef TIM_OCInitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    unsigned short duty;
    char buff[128];
    
    //LED_Init();
    //LED_R_ON();
    //LED_G_ON();
    
        /* TIM3, GPIOB clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
    //RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

    //GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    //GPIO_Init(GPIOB, &GPIO_InitStructure);
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    //GPIO_PinAFConfig(GPIOB, GPIO_PinSource1, GPIO_AF_TIM3);
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource9, GPIO_AF_TIM3);


    /* Compute the prescaler value */
    SystemCoreClockUpdate();
    PrescalerValue = (uint16_t) (SystemCoreClock / 2 / TIMER_PRESCALER_FREQ) - 1;   // timer base counter에 1MHz 입력

    /* Time base configuration */
    TIM_TimeBaseStructure.TIM_Period = PWM - 1;     // 1kHz timer
    TIM_TimeBaseStructure.TIM_Prescaler = 0;//0(8400)or1(4200)
    //TIM_TimeBaseStructure.TIM_Period = 5000; //kevin20180727 : 작을수록 주기가 길어짐 Hz 인가봄
    //TIM_TimeBaseStructure.TIM_Prescaler = 1; //kevin20180727 : 작을수록 분해능 up! TIM3 프리스케일러는 1 to 65536 가능
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PWM / 2;       // default 50%
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OC4Init(TIM3, &TIM_OCInitStructure);
    TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Disable);

    /* TIM3 enable counter */
    TIM_Cmd(TIM3, ENABLE);
//(1-1End)FullDigitalPID with PWM 하면서 생긴 main() 소스


/*
    while (1)
    {
        
        INT32U i = 0;
        
        
        //kevin_20180830 : CPU 입장에서는 PWM 신호가모두 Low 가 나오지만 인버터에 의해 반전되 정전류 회로로는 10mA 출력 
        //kevin_20180830 : 신형 블럭  기준에서, 수간부 (슬릿) 이 아래쪽 리미트에 걸리는 상태
        TIM_SetCompare4(TIM3, 1);
        for (i=0;i<4000000;i++) 
        {
            delay16Clock();
        }
        
        //kevin_20180830 : CPU 입장에서는 PWM 신호가모두 High 가 나오지만 인버터에 의해 반전되 정전류 회로로는 0mA 출력
        //kevin_20180830 : 신형 블럭  기준에서, 수간부 (슬릿) 이 위쪽 리미트에 걸리는 상태
        TIM_SetCompare4(TIM3, 4199);
        for (i=0;i<4000000;i++) 
        {
            delay16Clock();
        }
    }
*/

//(2-6Start)FullDigitalPID with PWM (without Photodiode center calibration)

    slitTargetPosition = 3435973836;
    INT32S i = 0;
    INT32S span = 0;
    INT32S offset = 0;
    INT32S ku_flag = 1;
    INT32S tu_flag = 1;
    INT32S count = 0;
    INT32S fp = 0;
    INT32S sp = 0;

    //pwm을 0으로 설정하여 전자석의 출력을 최대로 만든 후 슬릿이 하한에 있을 때의 에러값을 확인
    pwm = 0;
    TIM_SetCompare4(TIM3, (INT32U)pwm);
    for(i=0; i<9999999; i++)
    {
        delay16Clock();
    }
    AdGetRawData();
    if(RingBufCheckBuf(adRingBufPtr))
    {
        photodiodeDownAd = abs(RingBufGetInt32s(adRingBufPtr) - slitTargetPosition) / 1000;
    }
    //pwm을 9000으로 설정하여 전자석의 출력을 최소로 만든 후 슬릿이 상한에 있을 때의 에러값을 확인
    pwm = PWM;
    TIM_SetCompare4(TIM3, (INT32U)pwm);
    for(i=0; i<9999999; i++)
    {
        delay16Clock();
    }
    AdGetRawData();
    if(RingBufCheckBuf(adRingBufPtr))
    {
        photodiodeUpAd = abs(RingBufGetInt32s(adRingBufPtr) - slitTargetPosition) / 1000;
    }
    //슬릿이 하한과 상한에 있을 때의 에러값의 평균을 span으로 설정
    span = (photodiodeDownAd + photodiodeUpAd) / 2;
    
    //슬릿을 중앙에 위치시키기 위한 pwm을 탐색
    while(currentPositionError >= 0 || currentPositionError == -slitTargetPosition)
    {
        TIM_SetCompare4(TIM3, (INT32U)pwm);
        for(i=0; i<999999; i++)
        {
            delay16Clock();
        }
        AdGetRawData();
        if(RingBufCheckBuf(adRingBufPtr))
        {
            currentPositionError = RingBufGetInt32s(adRingBufPtr) - slitTargetPosition;
        }
        pwm--;
    }
    //슬릿을 중앙에 위치시키기 위한 pwm을 offset으로 설정
    offset = pwm + 1 - PWM / 2;
    
    //Ziegler-Nichols Closed-Loop Tuning을 위한 Ku를 탐색
    count = 0;
    while(ku_flag)
    {
        if(count < 99999)
        {
            AdGetRawData();
            if(RingBufCheckBuf(adRingBufPtr))
            {            
                slitCurrentPosition = RingBufGetInt32s(adRingBufPtr);
                currentPositionError = slitCurrentPosition - slitTargetPosition;
                if(currentPositionError > 0)
                {
                    currentPositionError = (currentPositionError / 1000);
                }
                else if(currentPositionError < 0)
                {
                    currentPositionError = -1 * (-1 * currentPositionError / 1000);
                }
                else
                {
                    currentPositionError = 0;
                }
                if(currentPositionError < -photodiodeDownAd / 100 * 90 || currentPositionError > photodiodeUpAd / 100 * 90)
                {
                    ku_flag = 0;
                }
                u = (INT32S)(kp * (FP32)currentPositionError);
                if(u > span)
                {
                    u = span;
                }
                else if(u < -span)
                {
                    u = -span;
                }
                pwm = (span - u) / (span * 2) * PWM;
                pwm = pwm + offset;
                if(pwm > PWM)
                {
                    pwm = PWM;
                }
                if(pwm == 0 || pwm == PWM)
                {
                    ku_flag = 0;
                }
                if(count < 9 && currentPositionError - lastPositionError < 0)
                {
                    pwm = (PWM / 2 + offset) / 100 * 90;
                }
                else if(count < 9 && currentPositionError - lastPositionError > 0)
                {
                    count--;
                }
                TIM_SetCompare4(TIM3, (INT32S)pwm);
                lastPositionError = currentPositionError;
                Int32sToDecStr(rawAdDataASCII, currentPositionError, 7, 0, 0, 0, 0);
                CommSendData3(rawAdDataASCII, 7, 1);
                network_tx_proc();
                count++;
            }
        }
        else
        {
            count = 0;
            kp += 0.001;
        }
    }
    ku = kp - 0.001;

    //Ziegler-Nichols Closed-Loop Tuning을 위한 tu를 탐색
    count = 0;
    while(tu_flag)
    {
        if(count < 99999)
        {
            AdGetRawData();
            if(RingBufCheckBuf(adRingBufPtr))
            {            
                slitCurrentPosition = RingBufGetInt32s(adRingBufPtr);
                currentPositionError = slitCurrentPosition - slitTargetPosition;
                if(currentPositionError > 0)
                {
                    currentPositionError = (currentPositionError / 1000);
                }
                else if(currentPositionError < 0)
                {
                    currentPositionError = -1 * (-1 * currentPositionError / 1000);
                }
                else
                {
                    currentPositionError = 0;
                }
                u = (INT32S)(ku * (FP32)currentPositionError);
                if(u > span)
                {
                    u = span;
                }
                else if(u < -span)
                {
                    u = -span;
                }
                pwm = (span - u) / (span * 2) * PWM;
                pwm = pwm + offset;
                if(pwm > PWM)
                {
                    pwm = PWM;
                }
                TIM_SetCompare4(TIM3, (INT32U)pwm);
                Int32sToDecStr(rawAdDataASCII, currentPositionError, 7, 0, 0, 0, 0);
                CommSendData3(rawAdDataASCII, 7, 1);
                network_tx_proc();
                count++;
            }
        }else
        {
            AdGetRawData();
            if(RingBufCheckBuf(adRingBufPtr))
            {            
                slitCurrentPosition = RingBufGetInt32s(adRingBufPtr);
                currentPositionError = slitCurrentPosition - slitTargetPosition;
                if(currentPositionError > 0)
                {
                    currentPositionError = (currentPositionError / 1000);
                }
                else if(currentPositionError < 0)
                {
                    currentPositionError = -1 * (-1 * currentPositionError / 1000);
                }
                else
                {
                    currentPositionError = 0;
                }
                currentPositionError_MAVR = movingAverageFilter(currentPositionError);
                current_MAVR = currentPositionError_MAVR - lastPositionError_MAVR;
                if(tu_flag == 1 && last_MAVR >= 0 && current_MAVR < 0)
                {
                    fp = count;
                    tu_flag = 2;
                }
                else if(tu_flag == 2 && last_MAVR >= 0 && current_MAVR < 0)
                {
                    sp = count;
                    tu_flag = 0;
                }
                u = (INT32S)(ku * (FP32)(currentPositionError));
                if(u > span)
                {
                    u = span;
                }
                else if(u < -span)
                {
                    u = -span;
                }
                pwm = (span - u) / (span * 2) * PWM;
                pwm = pwm + offset;
                if(pwm > PWM)
                {
                    pwm = PWM;
                }
                TIM_SetCompare4(TIM3, (INT32U)pwm);
                lastPositionError_MAVR = currentPositionError_MAVR;
                last_MAVR = current_MAVR;
                count++;
            }
        }
    }
    tu = sp - fp;
    
//    //Block_B PCB_B(4200)
//    photodiodeDownAd = 362039;
//    photodiodeUpAd = 505839;
//    span = (photodiodeDownAd + photodiodeUpAd) / 2;
//    ku = 1.40000013E-2;
//    tu = 1.894E+3;
    
//    //Block_B PCB_B(8400)
//    photodiodeDownAd = ;
//    photodiodeUpAd = ;
//    span = (photodiodeDownAd + photodiodeUpAd) / 2;
//    ku = ;
//    tu = ;
    
    //Ziegler-Nichols Closed-Loop Tuning
    kp = 0.6 * ku;
    ki = 2 * kp / tu;
    kd = kp * tu / 8;
    
    //Fine Tuning
    span /= 100;
    kp /=2;
    ki /=1;
    kd /=1;
    
    //PID Controller
    while(1)
    {
        AdGetRawData();
        if(RingBufCheckBuf(adRingBufPtr))
        {            
            slitCurrentPosition = RingBufGetInt32s(adRingBufPtr);
            currentPositionError = slitCurrentPosition - slitTargetPosition;
            if(currentPositionError > 0)
            {
                currentPositionError = (currentPositionError / 1000);
            }
            else if(currentPositionError < 0)
            {
                currentPositionError = -1 * (-1 * currentPositionError / 1000);
            }
            else
            {
                currentPositionError = 0;
            }
            integralPid = integralPid + (currentPositionError + lastPositionError) / 2;
            derivativePid = currentPositionError - lastPositionError;
            u = (INT32S)((kp * (FP32)(currentPositionError)) + (ki * (FP32)(integralPid)) + (kd * (FP32)(derivativePid)));
            if(u > span)
            {
                u = span;
            }
            else if(u < -span)
            {
                u = -span;
            }
            pwm = (span - u) / (span * 2) * PWM;
            if(pwm > PWM)
            {
                pwm = PWM;
            }
            TIM_SetCompare4(TIM3, (INT32U)8399);
            lastPositionError = currentPositionError;
//            Int32sToDecStr(rawAdDataASCII, currentPositionError, 7, 0, 0, 0, 0);
//            CommSendData3(rawAdDataASCII, 7, 1);
//            network_tx_proc();
        }
    }
    
//(2-6End)FullDigitalPID with PWM (without Photodiode center calibration)
    
//(2-7Start)PhotoDiode Test
    
         while(1) // PID 연산부
        {
            static INT32U uartTxCount = 0;
            static INT32U photodiodeDevider = 1000;
            AdGetRawData();
            
            if(RingBufCheckBuf(adRingBufPtr)) //kevin : 따라서 32비트 신호가 나오기 시작하면 adRingBufPtr 값이 무언가가 있기에 여기로 진입한다.
            {
            
                slitCurrentPositionRawData = RingBufGetInt32s(adRingBufPtr)/photodiodeDevider; //kevin : 위에까지 컨버팅되고, 버퍼에 저장되어있는 데이터를 rawData 에 넣어준다.

                
                
               // eMagnetVoltage = (INT32U)((65536.0/(float)(photodiodeAdLimitSpan))*(float)(pidValue));
                //DAC8811_Write_send(eMagnetVoltage);
                TIM_SetCompare4(TIM3, eMagnetVoltage);
                lastPositionError = currentPositionError;
                
                GPIO_SetBits(GPIOA,GPIO_Pin_2); //Moving Average 하는데 걸리는 연산시간 체크용
                GPIO_ResetBits(GPIOA,GPIO_Pin_3); //Moving Average 하는데 걸리는 연산시간 체크용
                
                currentPositionError_MAVR = movingAverageFilter_s(slitCurrentPositionRawData);
                
                GPIO_SetBits(GPIOA,GPIO_Pin_3); //Moving Average 하는데 걸리는 연산시간 체크용
                GPIO_ResetBits(GPIOA,GPIO_Pin_2); //Moving Average 하는데 걸리는 연산시간 체크용
                
                    
                if(uartTxCount==1000)
                {
//                    Int32sToDecStr(rawAdDataASCII, slitCurrentPositionRawData, 12, 0, 0, 0, 0);
//                    CommSendData3(rawAdDataASCII, 12,  0);
                    Int32sToDecStr(rawAdDataASCII, currentPositionError_MAVR, 12, 0, 0, 0, 0);
                    CommSendData3(rawAdDataASCII, 12,  1);
                    uartTxCount=0;
                }
                uartTxCount++;
            }//kevin20180712 : PID 연산 -end
            network_tx_proc(); //kevin20180903:CommBufUartx.Txing 를 체크해서 데이터를 내보낼지 말지 정함                
            
        }//kevin20180712 : Main program -end
//(2-7End)PhotoDiode Test
    
    
//(2-3Start)FullDigitalPID with PWM
    
    while (1)
    {
        INT32U i = 0;
        
        //kevin_20180830 : RingBufGetInt32s(adRingBufPtr) 에서 얻은 값의 자릿수가 너무 높아서 버리기 위한 부분
        INT32U photodiodeDevider = 100; //kevin_20180830 : 녗으로 나눌 것인가 (defualt : 1)
        
        /////움직임 에이징 (측정하지는 않고 예비로 한번 움직여보는 행동)
        //kevin_20180830 : CPU 입장에서는 PWM 신호가모두 Low 가 나오지만 인버터에 의해 반전되 정전류 회로로는 10mA 출력 
        //kevin_20180830 : 신형 블럭  기준에서, 수간부 (슬릿) 이 아래쪽 리미트에 걸리는 상태
        TIM_SetCompare4(TIM3, 1);
        for (i=0;i<4000000;i++) 
        {
            delay16Clock();
        }
        
        //kevin_20180830 : CPU 입장에서는 PWM 신호가모두 High 가 나오지만 인버터에 의해 반전되 정전류 회로로는 0mA 출력
        //kevin_20180830 : 신형 블럭  기준에서, 수간부 (슬릿) 이 위쪽 리미트에 걸리는 상태
        TIM_SetCompare4(TIM3, 4199);
        for (i=0;i<4000000;i++) 
        {
            delay16Clock();
        }
        /////움직임 에이징 (측정하지는 않고 예비로 한번 움직여보는 행동)
        
        
        //kevin_20180830 : CPU 입장에서는 PWM 신호가모두 High 가 나오지만 인버터에 의해 반전되 정전류 회로로는 0mA 출력
        //kevin_20180830 : 신형 블럭  기준에서, 수간부 (슬릿) 이 위쪽 리미트에 걸리는 상태
        TIM_SetCompare4(TIM3, 4199);
        for (i=0;i<8000000;i++)
        {
            delay16Clock();            
        }
        while(1)
        {
            AdGetRawData();
            
            if(RingBufCheckBuf(adRingBufPtr)) //kevin : 따라서 32비트 신호가 나오기 시작하면 adRingBufPtr 값이 무언가가 있기에 여기로 진입한다.
            {
                photodiodeUpAdRawData = RingBufGetInt32s(adRingBufPtr)/photodiodeDevider; //kevin : 위에까지 컨버팅되고, 버퍼에 저장되어있는 데이터를 rawData 에 넣어준다.
                //photodiodeUpAdRawData = photodiodeUpAdRawData + 2141279653;
                break;
            }    
        }
        
        

        /////포토다이오드 센터 캘리브레이션
        //kevin_20180830 : CPU 입장에서는 PWM 신호가모두 Low 가 나오지만 인버터에 의해 반전되 정전류 회로로는 10mA 출력 
        //kevin_20180830 : 신형 블럭  기준에서, 수간부 (슬릿) 이 아래쪽 리미트에 걸리는 상태
        TIM_SetCompare4(TIM3, 1);
        for (i=0;i<8000000;i++)
        {
            delay16Clock();            
        }
        while(1)
        {  
            AdGetRawData();
            
            if(RingBufCheckBuf(adRingBufPtr)) //kevin : 따라서 32비트 신호가 나오기 시작하면 adRingBufPtr 값이 무언가가 있기에 여기로 진입한다.
            {
                photodiodeDownAdRawData = RingBufGetInt32s(adRingBufPtr)/photodiodeDevider; //kevin : 위에까지 컨버팅되고, 버퍼에 저장되어있는 데이터를 rawData 에 넣어준다.
                 //photodiodeDownAdRawData = photodiodeDownAdRawData + 2141279653;
                break;
            }    
        }
        
        
        
        ////포토다이오드에 의한 변위량 측정
        if(photodiodeUpAdRawData > photodiodeDownAdRawData)
        {
            photodiodeAdSpan = photodiodeUpAdRawData - photodiodeDownAdRawData;
        }
        else photodiodeAdSpan = photodiodeDownAdRawData- photodiodeUpAdRawData; //포토다이오드가 뒤집혔을경우 출력이 바뀔때 사용
        
        
        //kevin20180621
        //포토다이오드Up/Down 값에서 변위가 없는 쓸모없는 부분 제거
        //예를들어 Up : 2200 / Down:2100 으로 측정되면
        //Up : 100 / Down : 0 으로 값을 변경해 준다.
        //뒤에서 계산할때 쓸데없이 자릿수가 늘어나는 것을 방지하기 위해 만들었다
        //나중에 포토다이오드값을 충분히 증폭해서 받을수 있다면 부정확한 측정값인 하위비트를 바림을 수행하여 동일한 기능을 할 수 있을 것이다.
        photodiodeUpAd = photodiodeAdSpan;
        photodiodeDownAd = 0;
        
        
        //kevin20180621 : 측정된 포토다이오드 상단리미트 값과 하단리미트값으로 발란스를 위한 포토센서 중점위치 계산
        photodiodeCenterAd = (photodiodeUpAd + photodiodeDownAd)/2; 
        slitTargetPosition = photodiodeCenterAd;        
        //slitTargetPosition = slitTargetPosition - 6500000 ; //발란스 위치 디버깅용 (더할수록 발란스 위치를 위로)
        
        
            
        //kevin20180621 
        //포토다이오드 움직이는 범위 에대한 리미트를 소프트웨어적으로 적용
        //위에서 전자석의 힘으로 강제로 상단/하단 리미트에 위치하게 하여 측정한것은 강한 힘으로 밀어내서 리미트 값을 측정한 것이기때문에
        //실제론 해당 리미트 값에 도달하기 전에 리미트에 도달하였다고 소프트웨어적으로 제어를 해주는 편이 좀더 좋을거라 판단
        photodiodeLimitMargin = (photodiodeAdSpan)/100*2; //kevin20180621 : 실제 측정된 포토 다이오드 스판에서 얼마만큼 줄일것인지 정해줌
        photodiodeUpAdLimit = photodiodeUpAd - photodiodeLimitMargin; // keinv20160621 : 실제 리미트에 닿기 전을 실제 리미트라고 정해준다
        photodiodeDownAdLimit = photodiodeDownAd + photodiodeLimitMargin; // keinv20160621 :  실제 리미트에 닿기 전을 실제 리미트라고 정해준다.
        photodiodeAdLimitSpan = photodiodeUpAdLimit - photodiodeDownAdLimit; // keinv20160621 : 마진이 적용된 포토다이오드 스판값
        
        
         while(1) // PID 연산부
        {
                
            //DAC PID 계수 마지막 버전
            //kp = 0.7;
            //ki = 0.003;
            //kd = 1.6;
                                    
            //PWM PID 계수 마지막 버전 (@로우패스 1kHz)
            //kp = 0.3;
            //ki = 0.006;
            //kd = 15; //10~15정도가 적당해 보임
            
            //PWM PID 계수 마지막 버전 (@로우패스 60Hz)
            //kp = 0.3;
            //ki = 0.006;
            //kd = 20; //20~30정도가 적당해 보임
            
            
            //2차 PCB, 단전원용 PID 계수 -2
            kp = 0.3;
            ki = 0.007; //0.006 은 0g 과 1kg 계량에서 오차가 생겼음
            kd = 100; //20~30정도가 적당해 보임
            
            //2차 PCB, 단전원용 PID 계수 > 0kg 1kg 계량에서 포토다이오드 편차는 2mV 정도로 관찰됨
            kp = 0.3;
            ki = 0.0075; //0.006 에서 0.0075로 변경되면서 많이 좋아짐
            kd = 100; 
            
            
            //2차 PCB, 단전원용 PID 계수 > 포토 다이오드 Gain 이 1에서 0.75로 줄어들면서 변경
            kp = 0.3;
            ki = 0.0077;
            kd = 120; 
            

            static INT32U uartTxCount = 0;
           
            
            
            AdGetRawData();
            
            if(RingBufCheckBuf(adRingBufPtr)) //kevin : 따라서 32비트 신호가 나오기 시작하면 adRingBufPtr 값이 무언가가 있기에 여기로 진입한다.
            {
            
                slitCurrentPositionRawData = RingBufGetInt32s(adRingBufPtr)/photodiodeDevider; //kevin : 위에까지 컨버팅되고, 버퍼에 저장되어있는 데이터를 rawData 에 넣어준다.
                slitCurrentPosition = photodiodeUpAdRawData - slitCurrentPositionRawData; //kevin20180621 : 이거 문제 있을듯.어떻게 처리하는게 좋을까.위에 값을 낮춘것땜에 같이 낮춘다고 했긴했는데... 임시방편이다..
                
                
                
                //kevin20180620:슬릿의 편차
                currentPositionError = slitTargetPosition - slitCurrentPosition;
                
                //kevin20180620:적분 계산
                integralPid = integralPid + currentPositionError;
                //kevin20180621 : 무분별하게 적분값이 늘어나는것을 방지
                //kevin20180621 : 적분값이 아무리 늘어나도 전자석이 낼수 있는 값에는 한계가 있기도하고, 계속 적분하면 int 형의 값이 변질되기때문
                
                
                //kevin20180625 : 적분 최대치 제한버전 1
                /*
                //if(integralPid>photodiodeAdSpan)
                if(integralPid>photodiodeAdSpan)
                    integralPid = photodiodeAdSpan;
                //else if(integralPid<-photodiodeAdSpan)
                else if(integralPid<-photodiodeAdSpan)
                    integralPid = -photodiodeAdSpan;
                */

                //kevin20180625 : 적분 최대치 제한버전 2 - int32 signed 형 최대치까지만 허용
                //kevin20180712 : 
                //if(integralPid>photodiodeAdSpan)
                if(integralPid>2000000000)
                    integralPid = 2000000000;
                
                //else if(integralPid<-photodiodeAdSpan)
                else if(integralPid<-2000000000)
                    integralPid = -2000000000;
                
                
                //kevin20180620:미분계산
                derivativePid = currentPositionError - lastPositionError;
                //kevin20180621 : 무분별하게 미분값이 늘어나는것을 방지
                //kevin20180621 : 순간적으로 잘못 측정되거나 노이즈 등으로 발생할수 있는 미분값등이 너무 크게 반영되는것을 미연에 방지하기위해
                //kevin20180621 : 이 제한적 사항은 차후 디버깅에 의해 삭제 가능할 수 있다.(보완하거나)
                
                
                if(derivativePid>photodiodeAdSpan)
                    derivativePid = photodiodeAdSpan;
                else if(derivativePid<-photodiodeAdSpan)
                    derivativePid = -photodiodeAdSpan;
                
                                
                //kevin20180620: 최종 PID 합산 제어량
                //pidValue = kp * (currentPositionError + (ki * integralPid) + (kd * derivativePid));
                pidValue = (kp * currentPositionError) + (ki * integralPid) + (kd * derivativePid);
                
                
                //kevin20180620
                //도출된 PID 값은 전자석의 세기를 정하는 척도가 된다. PID 값이 무한히 커져서도 안되지만
                //현재 위치가 센터로부터 가장멀리 떨어져있고, 그 상태가 계속유지되었을때(적분된 값)에 가장큰 
                
                //kevin20180620:제어 리미트 설정, 측정된 리미트는 실제 리미트에 닿아버리는 리미트기때문에 좋지않은 값이다.
                //if (pidValue > photodiodeAdLimitSpan) pidValue = photodiodeAdLimitSpan;
                //else if (pidValue < -photodiodeAdLimitSpan) pidValue = -photodiodeAdLimitSpan;
                if (pidValue > photodiodeAdLimitSpan) 
                {
                    pidValue = photodiodeAdLimitSpan; // kevin20180621 : 나중에 값을 넌것을 자동으로 채워지게 바꿔야할듯
                }
                else if (pidValue < -photodiodeAdLimitSpan) 
                {
                    pidValue = -photodiodeAdLimitSpan;
                }
                else pidValue=pidValue;
            
                
                pidValue=-pidValue; //kevin20180719 : 프로토타입 2차PCB부터 단전원 + 인버터 사용 PWM PID 로 오면서 극성이 반대로 되었기 때문에 최종 값이 뒤집어줌
                
                //PID 용 차등입력전압 계산식
                //65536=16bit ADC Max값  //0x0000 = -9.369V  //0xFFFF = +9.293V
                //eMagnetVoltage = (-pidValue + photodiodeAdLimitSpan) / ((photodiodeAdLimitSpan - (-photodiodeAdLimitSpan)) * 65536);
                //kevin20180621 : 소수점 처리문제때문에 계산을 쪼개서 했음
                eMagnetVoltageFloat = (float)(photodiodeAdLimitSpan+pidValue) / (float)(photodiodeAdLimitSpan+photodiodeAdLimitSpan);
                
                //eMagnetVoltage = (int)(eMagnetVoltageFloat * (65536-1));
                //eMagnetVoltage = (int)(eMagnetVoltageFloat * (65536-1)); //kevin20180719 : 이수치를 낮추면 아래위로 구간이 작아지는게 아니라 중점도 흐트러짐
                //-1를 해준 이유는 오버플로우 방지책으로 임시로 넣어둔 것
                eMagnetVoltage = (int)(eMagnetVoltageFloat * (4200-1)); //kevin20180719 : 이수치를 낮추면 아래위로 구간이 작아지는게 아니라 중점도 흐트러짐
                
                

                
                
               // eMagnetVoltage = (INT32U)((65536.0/(float)(photodiodeAdLimitSpan))*(float)(pidValue));
                //DAC8811_Write_send(eMagnetVoltage);
                TIM_SetCompare4(TIM3, eMagnetVoltage);
                lastPositionError = currentPositionError;
                
                GPIO_SetBits(GPIOA,GPIO_Pin_2); //Moving Average 하는데 걸리는 연산시간 체크용
                GPIO_ResetBits(GPIOA,GPIO_Pin_3); //Moving Average 하는데 걸리는 연산시간 체크용
                
                currentPositionError_MAVR = movingAverageFilter_s(currentPositionError);
                
                GPIO_SetBits(GPIOA,GPIO_Pin_3); //Moving Average 하는데 걸리는 연산시간 체크용
                GPIO_ResetBits(GPIOA,GPIO_Pin_2); //Moving Average 하는데 걸리는 연산시간 체크용
                
                    
                if(uartTxCount==1000)
                {
                    Int32sToDecStr(rawAdDataASCII, currentPositionError_MAVR, 12, 0, 0, 0, 0);
                    CommSendData3(rawAdDataASCII, 12,  1);
                    uartTxCount=0;
                }
                uartTxCount++;
            }//kevin20180712 : PID 연산 -end
            network_tx_proc(); //kevin20180903:CommBufUartx.Txing 를 체크해서 데이터를 내보낼지 말지 정함                
            
        }//kevin20180712 : Main program -end
    }//kevin20180712 : Main while (1) -end
//(2-3End)FullDigitalPID with PWM
    
    
//(2-5Start)2ch Uart Test

    INT32U h = 0;
    
    while (1)
    {
        AdGetRawData();

        if(RingBufCheckBuf(adRingBufPtr)) 
        {                           
            photodiodeUpAdRawData = RingBufGetInt32s(adRingBufPtr); //kevin : 위에까지 컨버팅되고, 버퍼에 저장되어있는 데이터를 rawData 에 넣어준다.
            photodiodeUpAdRawData = movingAverageFilter(photodiodeUpAdRawData);
            
            if(h==10)
            {     
                Int32sToUnsignedDecStr(rawAdDataASCII, photodiodeUpAdRawData, 12, 0, 0, 0, 0);
                CommSendData(rawAdDataASCII, 12,  0);
                CommSendData3(rawAdDataASCII, 12,  1);
                h=0;
            }
            h++;
        }
       network_tx_proc(); //kevin20180903:CommBufUartx.Txing 를 체크해서 데이터를 내보낼지 말지 정함                
    }

//(2-5End)2ch Uart Test
    
    
//(2-4Start)FullDigitalPID with PWM
    INT32U pwmLevel=0;; //pwmLevel = 0~300 까지
    while (1)
    {
        volatile INT32U i = 0;
        volatile INT32U k = 0;
        
        INT32U dutyHigh;
        INT32U dutyLow;
        
        duty = 100;
        TIM_SetCompare4(TIM3, (uint16_t)duty);
    }
//(2-4End)FullDigitalPID with PWM   
    
    
    
//(2-1Start)FullDigitalPID with PWM

    while (1)
    {
        volatile INT32U i = 0;  
        
        //duty = 200; //0~200 가능 <<2^12 인 4096 개 정도로는 쪼갤 수 있으면 좋을텐데
/*
        for (duty=0;duty<4500;duty++)
        {
            //기다려주시는 시간은 PWM 프리퀀시에서 뭔가를 받아와서 카운팅하는게 좋을듯.
            //주기를 알아내고 주기가 들어올때마다 출력을 내보낸다.
            TIM_SetCompare4(TIM3, (uint16_t)duty);
            for(i=0;i<5000;i++)            {}//delay
            
        }
   */     
    
    
        duty = 1;
        TIM_SetCompare4(TIM3, (uint16_t)duty);
        for(i=0;i<30000;i++)            {}//delay
        
        duty = 4199;
        TIM_SetCompare4(TIM3, (uint16_t)duty);
        for(i=0;i<30000;i++)            {}//delay
    

    }
//(2-1Start)FullDigitalPID with PWM   
    
    

//(2-2Start)FullDigitalPID with DAC
    while (1)
    {
        
        INT32U i = 0;
        
        /////움직임 에이징 (측정하지는 않고 예비로 한번 움직여보는 행동)
        DAC8811_Write_send(0x0100); //kevin_20180618 : 자석을 밀때 (-) 전압
        for (i=0;i<4000000;i++) 
        {
            delay16Clock();
        }
        
        DAC8811_Write_send(0xFEFF); //kevin_20180618 : 자석을 당길때 (+) 전압
        for (i=0;i<4000000;i++) 
        {
            delay16Clock();
        }
        /////움직임 에이징 (측정하지는 않고 예비로 한번 움직여보는 행동)
        
        
        /////포토다이오드 센터 캘리브레이션
        DAC8811_Write_send(0x0000); //kevin_20180618 : 자석을 밀때 (-) 전압
        for (i=0;i<4000000;i++)
        {
            delay16Clock();            
        }
        while(1)
        {
            AdGetRawData();
            
            if(RingBufCheckBuf(adRingBufPtr)) //kevin : 따라서 32비트 신호가 나오기 시작하면 adRingBufPtr 값이 무언가가 있기에 여기로 진입한다.
            {
                photodiodeUpAdRawData = RingBufGetInt32s(adRingBufPtr); //kevin : 위에까지 컨버팅되고, 버퍼에 저장되어있는 데이터를 rawData 에 넣어준다.
                //photodiodeUpAdRawData = photodiodeUpAdRawData + 2141279653;
                break;
            }    
        }
        
        
        
        DAC8811_Write_send(0xFFFF); //kevin_20180618 : 자석을 당길때 (+) 전압   
        for (i=0;i<4000000;i++)
        {
            delay16Clock();            
        }
        while(1)
        {  
            AdGetRawData();
            
            if(RingBufCheckBuf(adRingBufPtr)) //kevin : 따라서 32비트 신호가 나오기 시작하면 adRingBufPtr 값이 무언가가 있기에 여기로 진입한다.
            {
                photodiodeDownAdRawData = RingBufGetInt32s(adRingBufPtr); //kevin : 위에까지 컨버팅되고, 버퍼에 저장되어있는 데이터를 rawData 에 넣어준다.
                 //photodiodeDownAdRawData = photodiodeDownAdRawData + 2141279653;
                break;
            }    
        }
        
        
        
        ////포토다이오드에 의한 변위량 측정
        if(photodiodeUpAdRawData > photodiodeDownAdRawData)
        {
            photodiodeAdSpan = photodiodeUpAdRawData - photodiodeDownAdRawData;
        }
        else photodiodeAdSpan = photodiodeDownAdRawData- photodiodeUpAdRawData; //포토다이오드가 뒤집혔을경우 출력이 바뀔때 사용
        
        
        //kevin20180621
        //포토다이오드Up/Down 값에서 변위가 없는 쓸모없는 부분 제거
        //예를들어 Up : 2200 / Down:2100 으로 측정되면
        //Up : 100 / Down : 0 으로 값을 변경해 준다.
        //뒤에서 계산할때 쓸데없이 자릿수가 늘어나는 것을 방지하기 위해 만들었다
        //나중에 포토다이오드값을 충분히 증폭해서 받을수 있다면 부정확한 측정값인 하위비트를 바림을 수행하여 동일한 기능을 할 수 있을 것이다.
        photodiodeUpAd = photodiodeAdSpan;
        photodiodeDownAd = 0;
        
        
        //kevin20180621 : 측정된 포토다이오드 상단리미트 값과 하단리미트값으로 발란스를 위한 포토센서 중점위치 계산
        photodiodeCenterAd = (photodiodeUpAd + photodiodeDownAd)/2; 
        slitTargetPosition = photodiodeCenterAd;        
        //slitTargetPosition = slitTargetPosition - 6500000 ; //발란스 위치 디버깅용 (더할수록 발란스 위치를 위로)
        
        
        //kevin20180621 
        //포토다이오드 움직이는 범위 에대한 리미트를 소프트웨어적으로 적용
        //위에서 전자석의 힘으로 강제로 상단/하단 리미트에 위치하게 하여 측정한것은 강한 힘으로 밀어내서 리미트 값을 측정한 것이기때문에
        //실제론 해당 리미트 값에 도달하기 전에 리미트에 도달하였다고 소프트웨어적으로 제어를 해주는 편이 좀더 좋을거라 판단
        photodiodeLimitMargin = (photodiodeAdSpan)/100*2; //kevin20180621 : 실제 측정된 포토 다이오드 스판에서 얼마만큼 줄일것인지 정해줌
        photodiodeUpAdLimit = photodiodeUpAd - photodiodeLimitMargin; // keinv20160621 : 실제 리미트에 닿기 전을 실제 리미트라고 정해준다
        photodiodeDownAdLimit = photodiodeDownAd + photodiodeLimitMargin; // keinv20160621 :  실제 리미트에 닿기 전을 실제 리미트라고 정해준다.
        photodiodeAdLimitSpan = photodiodeUpAdLimit - photodiodeDownAdLimit; // keinv20160621 : 마진이 적용된 포토다이오드 스판값
        
        
         while(1) // PID 연산부
        {
                TEST_1_H; //kevin20180713 : 디버깅용
            //kp = 0.3;
            //ki = 0.005;
            //kd = 0.125;
            
            
                //1kg
                //kp = 0.4;
            //ki = 0.003;
            //kd = 1.6;
                
            kp = 0.7;
            ki = 0.0003;
            kd = 1.6;

            
            
            
            AdGetRawData();
            
            if(RingBufCheckBuf(adRingBufPtr)) //kevin : 따라서 32비트 신호가 나오기 시작하면 adRingBufPtr 값이 무언가가 있기에 여기로 진입한다.
            {
            TEST_1_L; //kevin20180713 : 디버깅용    
                slitCurrentPositionRawData = RingBufGetInt32s(adRingBufPtr); //kevin : 위에까지 컨버팅되고, 버퍼에 저장되어있는 데이터를 rawData 에 넣어준다.
                slitCurrentPosition = photodiodeUpAdRawData - slitCurrentPositionRawData; //kevin20180621 : 이거 문제 있을듯.어떻게 처리하는게 좋을까.위에 값을 낮춘것땜에 같이 낮춘다고 했긴했는데... 임시방편이다..
                
                
                
                //kevin20180620:슬릿의 편차
                currentPositionError = slitTargetPosition - slitCurrentPosition;
                
                //kevin20180620:적분 계산
                integralPid = integralPid + currentPositionError;
                //kevin20180621 : 무분별하게 적분값이 늘어나는것을 방지
                //kevin20180621 : 적분값이 아무리 늘어나도 전자석이 낼수 있는 값에는 한계가 있기도하고, 계속 적분하면 int 형의 값이 변질되기때문
                
                
                //kevin20180625 : 적분 최대치 제한버전 1
                /*
                //if(integralPid>photodiodeAdSpan)
                if(integralPid>photodiodeAdSpan)
                    integralPid = photodiodeAdSpan;
                //else if(integralPid<-photodiodeAdSpan)
                else if(integralPid<-photodiodeAdSpan)
                    integralPid = -photodiodeAdSpan;
                */

                //kevin20180625 : 적분 최대치 제한버전 2 - int32 signed 형 최대치까지만 허용
                //kevin20180712 : 
                //if(integralPid>photodiodeAdSpan)
                if(integralPid>2000000000)
                    integralPid = 2000000000;
                
                //else if(integralPid<-photodiodeAdSpan)
                else if(integralPid<-2000000000)
                    integralPid = -2000000000;
                
                
                //kevin20180620:미분계산
                derivativePid = currentPositionError - lastPositionError;
                //kevin20180621 : 무분별하게 미분값이 늘어나는것을 방지
                //kevin20180621 : 순간적으로 잘못 측정되거나 노이즈 등으로 발생할수 있는 미분값등이 너무 크게 반영되는것을 미연에 방지하기위해
                //kevin20180621 : 이 제한적 사항은 차후 디버깅에 의해 삭제 가능할 수 있다.(보완하거나)
                
                
                if(derivativePid>photodiodeAdSpan)
                    derivativePid = photodiodeAdSpan;
                else if(derivativePid<-photodiodeAdSpan)
                    derivativePid = -photodiodeAdSpan;
                
                                
                //kevin20180620: 최종 PID 합산 제어량
                //pidValue = kp * (currentPositionError + (ki * integralPid) + (kd * derivativePid));
                pidValue = (kp * currentPositionError) + (ki * integralPid) + (kd * derivativePid);
                
                
                //kevin20180620
                //도출된 PID 값은 전자석의 세기를 정하는 척도가 된다. PID 값이 무한히 커져서도 안되지만
                //현재 위치가 센터로부터 가장멀리 떨어져있고, 그 상태가 계속유지되었을때(적분된 값)에 가장큰 
                
                //kevin20180620:제어 리미트 설정, 측정된 리미트는 실제 리미트에 닿아버리는 리미트기때문에 좋지않은 값이다.
                //if (pidValue > photodiodeAdLimitSpan) pidValue = photodiodeAdLimitSpan;
                //else if (pidValue < -photodiodeAdLimitSpan) pidValue = -photodiodeAdLimitSpan;
                if (pidValue > photodiodeAdLimitSpan) 
                {
                    pidValue = photodiodeAdLimitSpan; // kevin20180621 : 나중에 값을 넌것을 자동으로 채워지게 바꿔야할듯
                }
                else if (pidValue < -photodiodeAdLimitSpan) 
                {
                    pidValue = -photodiodeAdLimitSpan;
                }
                else pidValue=pidValue;
            
                
                
                //PID 용 차등입력전압 계산식
                //65536=16bit ADC Max값  //0x0000 = -9.369V  //0xFFFF = +9.293V
                //eMagnetVoltage = (-pidValue + photodiodeAdLimitSpan) / ((photodiodeAdLimitSpan - (-photodiodeAdLimitSpan)) * 65536);
                //kevin20180621 : 소수점 처리문제때문에 계산을 쪼개서 했음
                eMagnetVoltageFloat = (float)(photodiodeAdLimitSpan+pidValue) / (float)(photodiodeAdLimitSpan+photodiodeAdLimitSpan);
                
                //eMagnetVoltage = (int)(eMagnetVoltageFloat * 65536);
                eMagnetVoltage = (int)(eMagnetVoltageFloat * 65536); //kevin20180719 : 이수치를 낮추면 아래위로 구간이 작아지는게 아니라 중점도 흐트러짐
                
                

                
                TEST_1_H; //kevin20180713 : 디버깅용
               // eMagnetVoltage = (INT32U)((65536.0/(float)(photodiodeAdLimitSpan))*(float)(pidValue));
                DAC8811_Write_send(eMagnetVoltage);
                lastPositionError = currentPositionError;
                TEST_1_L; //kevin20180713 : 디버깅용
            }//kevin20180712 : PID 연산 -end
            
        }//kevin20180712 : Main program -end
    }//kevin20180712 : Main while (1) -end
//(2-2End)FullDigitalPID with DAC
    
}//kevin20180712 : int main(void) -end

//kevin20180712 : delay16Clock(); >> 제대로 수정해놓기



/*
********************************************************************************
*                       LOCAL FUNCTIONS
********************************************************************************
*/
/* Insert local functions here */
//kevin20180710 : 지역 함수의 원형

INT32S movingAverageFilter(INT32S rawAdDataIn) //kevin20190227: 무빙 에버러지 필터 수정
{        
    INT32S filteredData = 0;
    INT32U i;
    
    MovAvrFilterBuf[MovAvrCurrnetBufIndex] = rawAdDataIn; //무빙에버러지를 위한 취득값을 원하는 에버러지 갯수에 맞게 스케일을 줄여준다.

    for (i=0; i<MovAvrFilterLength; i++)
    {
            filteredData = filteredData + MovAvrFilterBuf[i]; //스케일 다운된 모든 요소값을 합한다 (자동으로 평균값이 취해짐)
    }
    filteredData = filteredData / MovAvrFilterLength;
    MovAvrCurrnetBufIndex++; 
    if (MovAvrCurrnetBufIndex >= MovAvrFilterLength)
    {
        MovAvrCurrnetBufIndex = 0;
    }
    return filteredData;
}

INT32S movingAverageFilter_s(INT32S rawAdDataIn) //kevin20190227: 무빙 에버러지 필터 수정
{        
    INT32S filteredData = 0;
    INT32U i;
    
    MovAvrFilterBuf_s[MovAvrCurrnetBufIndex_s] = rawAdDataIn ;

    for (i=0; i<MovAvrFilterLength_s; i++)
    {
            filteredData = filteredData + MovAvrFilterBuf_s[i];
    }
    
    filteredData = filteredData / MovAvrFilterLength_s;
    MovAvrCurrnetBufIndex_s++; 
    if (MovAvrCurrnetBufIndex_s >= MovAvrFilterLength_s)
    {
        MovAvrCurrnetBufIndex_s = 0;
    }
    return filteredData;
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif






///*****************************************************************************
/*
|*  Copyright       :   (c) 2011 CAS
|*  Filename        :   driver_eeprom.c
|*  Version         :   0.1
|*  Programmer(s)   :   Tang Qian Kun(TQK)
|*  Created         :   2011/5/9
|*  Modified        :
|*  Description     :   CT100 eeprom driver function
*/
//****************************************************************************/
#include "globals.h"
#include "eeprom.h"

static EEPROM_WAIT e2rom_wait;

void EepromInit(void)
{
//static INT8U testdata = 0;
    M25LC1024_Powerup(0x00);
}

/******************************************************************************************************/
/* Send_Receive_byte	:	          		      					      */												
/******************************************************************************************************/
static INT8U Send_Receive_byte(INT8U data)
{
    INT8U spi3RxData = 0;
    
	SPI_I2S_SendData(SPI3,data);
    while(SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_TXE) == RESET);
/////////////////////////////////////// question-Please write code ///////////////////////////////////////
// mission -5:  M25P40(4Mbyte) write 및 read를 하여 값을 하이퍼터머널 windows에 표시하십시요!!
//             : format: motorola, master mode          
// Header File : ../include/91x_ssp.h 
//             : ../module/91x_ssp.c 
// SPI Clock Line -> GPIO5.4 , MOSI Line -> GPIO5.5 , MISO Line -> GPIO5.6 , NSS Line -> GPIO5.7 
  /* Loop while Receive FIFO is empty */
	while(SPI_I2S_GetFlagStatus(SPI3, SPI_I2S_FLAG_RXNE) == RESET);	
   spi3RxData = SPI_I2S_ReceiveData(SPI3);
  /* Return the byte read from the SSP bus */
	return spi3RxData; 
}

/******************************************************************************************************/
/* WaitForWriteEnd	:	  not used    	          					      */												
/******************************************************************************************************/
static void WaitForWriteEnd(INT32U Addr)
{
	INT8U Status = 0;
	
	if(Addr < EROM_1_START_ADDR) M25LCxx_CS0_ENABLE;            // CS Enable
	else M25LCxx_CS1_ENABLE;

  /* Send "Read Status Register" instruction */
	Send_Receive_byte(RDSR); 
  /* Loop as long as the memory is busy with a write cycle */ 		
	do{
    /* Send a dummy byte to generate the clock needed by the FLASH
    and put the value of the status register in FLASH_Status variable */
		Status = Send_Receive_byte(Dummy_Byte);
	}while((Status & WIP_Flag) == SET); /* Write in progress */  
  
	if(Addr < EROM_1_START_ADDR){
		M25LCxx_CS0_DISABLE;            // CS Disable 
		e2rom_wait.wait0 = FALSE;
	}else{
		M25LCxx_CS1_DISABLE;
		e2rom_wait.wait1 = FALSE;
	}
}

/******************************************************************************************************/
/* M25LC1024_ChipErase	:	          							      */												
/******************************************************************************************************/
void M25LC1024_ChipErase(INT32U Addr)
{
	if(Addr < EROM_1_START_ADDR){
		if(e2rom_wait.wait0){
			WaitForWriteEnd(EROM_0_START_ADDR);
		}
		M25LCxx_CS0_ENABLE;            // CS Enable
	}else{
		if(e2rom_wait.wait1){
			WaitForWriteEnd(EROM_1_START_ADDR);
		}
		M25LCxx_CS1_ENABLE;
	}
	Send_Receive_byte(WREN);    // Write enable instruction 
	if(Addr < EROM_1_START_ADDR) M25LCxx_CS0_DISABLE;            // CS Disable 
	else M25LCxx_CS1_DISABLE;
  
	if(Addr < EROM_1_START_ADDR) M25LCxx_CS0_ENABLE;            // CS Enable
	else M25LCxx_CS1_ENABLE;

	Send_Receive_byte(CERASE);      // Sector Erase instruction  
	if(Addr < EROM_1_START_ADDR){
		M25LCxx_CS0_DISABLE;            // CS Disable 
		e2rom_wait.wait0 = TRUE;
	}else{
		M25LCxx_CS1_DISABLE;
		e2rom_wait.wait1 = TRUE;
	}
}

/******************************************************************************************************/
/* M25LC1024_SectorErase	:	          							      */												
/******************************************************************************************************/
void M25LC1024_SectorErase(INT32U Sector_Addr)
{
	INT32U address;

	if(Sector_Addr < EROM_1_START_ADDR){
		if(e2rom_wait.wait0){
			WaitForWriteEnd(EROM_0_START_ADDR);
		}
		M25LCxx_CS0_ENABLE;            // CS Enable
		address = Sector_Addr - EROM_0_START_ADDR;
	}else{
		if(e2rom_wait.wait1){
			WaitForWriteEnd(EROM_1_START_ADDR);
		}
		M25LCxx_CS1_ENABLE;
		address = Sector_Addr - EROM_1_START_ADDR;
	}
	Send_Receive_byte(WREN);    // Write enable instruction 
	if(Sector_Addr < EROM_1_START_ADDR) M25LCxx_CS0_DISABLE;            // CS Disable 
	else M25LCxx_CS1_DISABLE;

	if(Sector_Addr < EROM_1_START_ADDR) M25LCxx_CS0_ENABLE;            // CS Enable
	else M25LCxx_CS1_ENABLE;

	Send_Receive_byte(SERASE);      // Sector Erase instruction 

    /* Send SectorAddr high nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x00FF0000) >> 16);
    /* Send SectorAddr medium nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x0000FF00) >> 8);
    /* Send SectorAddr low nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x000000FF));
  /* Deselect the FLASH: Chip Select high */  

	if(Sector_Addr < EROM_1_START_ADDR){
		M25LCxx_CS0_DISABLE;            // CS Disable 
		e2rom_wait.wait0 = TRUE;
	}else{
		M25LCxx_CS1_DISABLE;
		e2rom_wait.wait1 = TRUE;
	}
}

/******************************************************************************************************/
/* M25LC1024_PageErase	:	          							      */												
/******************************************************************************************************/
void M25LC1024_PageErase(INT32U Page_Addr)
{
	INT32U address;

	if(Page_Addr < EROM_1_START_ADDR){
		if(e2rom_wait.wait0){
			WaitForWriteEnd(EROM_0_START_ADDR);
		}
		M25LCxx_CS0_ENABLE;            // CS Enable
		address = Page_Addr - EROM_0_START_ADDR;
	}else{
		if(e2rom_wait.wait1){
			WaitForWriteEnd(EROM_1_START_ADDR);
		}
		M25LCxx_CS1_ENABLE;
		address = Page_Addr - EROM_1_START_ADDR;
	}
	Send_Receive_byte(WREN);    // Write enable instruction 
	if(Page_Addr < EROM_1_START_ADDR) M25LCxx_CS0_DISABLE;            // CS Disable 
	else M25LCxx_CS1_DISABLE;

	if(Page_Addr < EROM_1_START_ADDR) M25LCxx_CS0_ENABLE;            // CS Enable
	else M25LCxx_CS1_ENABLE;

	Send_Receive_byte(PERASE);      // Sector Erase instruction 

    /* Send SectorAddr high nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x00FF0000) >> 16);
    /* Send SectorAddr medium nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x0000FF00) >> 8);
    /* Send SectorAddr low nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x000000FF));
  /* Deselect the FLASH: Chip Select high */  

	if(Page_Addr < EROM_1_START_ADDR){
		M25LCxx_CS0_DISABLE;            // CS Disable 
		e2rom_wait.wait0 = TRUE;
	}else{
		M25LCxx_CS1_DISABLE;
		e2rom_wait.wait1 = TRUE;
	}
}

/******************************************************************************************************/
/* M25LC1024_Bytewrite	:	          							      */
/******************************************************************************************************/
static void M25LC1024_Byteswrite_fix(INT32U write_addr, INT8U src, INT32U size)
{
	INT32U address, i;
	
	if(write_addr < EROM_1_START_ADDR){
		if(e2rom_wait.wait0){
			WaitForWriteEnd(EROM_0_START_ADDR);
		}
		M25LCxx_CS0_ENABLE;            // CS Enable
		address = write_addr - EROM_0_START_ADDR;
	}else{
		if(e2rom_wait.wait1){
			WaitForWriteEnd(EROM_1_START_ADDR);
		}
		M25LCxx_CS1_ENABLE;
		address = write_addr - EROM_1_START_ADDR;
	}
	Send_Receive_byte(WREN);        // Write enable instruction 
	if(write_addr < EROM_1_START_ADDR) M25LCxx_CS0_DISABLE;            // CS Disable 
	else M25LCxx_CS1_DISABLE;

	if(write_addr < EROM_1_START_ADDR) M25LCxx_CS0_ENABLE;            // CS Enable
	else M25LCxx_CS1_ENABLE;

	Send_Receive_byte(WRITE);       // Write to Memory instruction
    /* Send SectorAddr high nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x00FF0000) >> 16);
    /* Send SectorAddr medium nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x0000FF00) >> 8);
    /* Send SectorAddr low nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x000000FF));
  /* send write data */
	for(i=0;i<size;i++){
		Send_Receive_byte(src);
	}

	if(write_addr < EROM_1_START_ADDR){
		M25LCxx_CS0_DISABLE;            // CS Disable 
		e2rom_wait.wait0 = TRUE;
	}else{
		M25LCxx_CS1_DISABLE;
		e2rom_wait.wait1 = TRUE;
	}
}

/******************************************************************************************************/
/* M25LC1024_Bytewrite	:	          							      */												
/******************************************************************************************************/
static void M25LC1024_Byteswrite(INT32U write_addr, const INT8U* pData, INT32U size)
{
	INT32U address, i;
	INT8U* src;
	
	src = (INT8U*)pData;

	if(write_addr < EROM_1_START_ADDR){
		if(e2rom_wait.wait0){
			WaitForWriteEnd(EROM_0_START_ADDR);
		}
		M25LCxx_CS0_ENABLE;            // CS Enable
		address = write_addr - EROM_0_START_ADDR;
	}else{
		if(e2rom_wait.wait1){
			WaitForWriteEnd(EROM_1_START_ADDR);
		}
		M25LCxx_CS1_ENABLE;
		address = write_addr - EROM_1_START_ADDR;
	}
	Send_Receive_byte(WREN);        // Write enable instruction 
	if(write_addr < EROM_1_START_ADDR) M25LCxx_CS0_DISABLE;            // CS Disable 
	else M25LCxx_CS1_DISABLE;

	if(write_addr < EROM_1_START_ADDR) M25LCxx_CS0_ENABLE;            // CS Enable
	else M25LCxx_CS1_ENABLE;

	Send_Receive_byte(WRITE);       // Write to Memory instruction
    /* Send SectorAddr high nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x00FF0000) >> 16);
    /* Send SectorAddr medium nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x0000FF00) >> 8);
    /* Send SectorAddr low nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x000000FF));
  /* send write data */
	for(i=0;i<size;i++){
		Send_Receive_byte(*(src+i));
	}

	if(write_addr < EROM_1_START_ADDR){
		M25LCxx_CS0_DISABLE;            // CS Disable 
		e2rom_wait.wait0 = TRUE;
	}else{
		M25LCxx_CS1_DISABLE;
		e2rom_wait.wait1 = TRUE;
	}
}

/******************************************************************************************************/
/* Eeprom_clear	:	          							      */
/******************************************************************************************************/
void Eeprom_clear(INT32U write_addr, INT8U src, INT32U size)
{
	INT32U page, pStart, count;
	
	while(1){
		page = write_addr/EROM_PAGE_SIZE;
		pStart = write_addr%EROM_PAGE_SIZE;
		count = (size > (EROM_PAGE_SIZE-pStart))?(EROM_PAGE_SIZE-pStart):size;
		M25LC1024_Byteswrite_fix(page*EROM_PAGE_SIZE + pStart, src, count);
		size -= count;
		if(size > 0){
			write_addr += count;
		}else break;
	}
}

void M25LC1024_Arrywrite(INT32U write_addr, INT8U* pData, INT32U size)
{
	INT32U page, pStart, count;
	
	while(1){
		page = write_addr/EROM_PAGE_SIZE;
		pStart = write_addr%EROM_PAGE_SIZE;
		count = (size > (EROM_PAGE_SIZE-pStart))?(EROM_PAGE_SIZE-pStart):size;
		M25LC1024_Byteswrite(page*EROM_PAGE_SIZE + pStart, pData, count);
		size -= count;
		if(size > 0){
			pData += count;
			write_addr += count;
		}else break;
	}
}
void Eeprom_swrite(INT32U write_addr, INT8U* pData, INT32U size)
{
    M25LC1024_Arrywrite(write_addr, pData, size);
}
void Eeprom_lwrite(INT32U write_addr, INT8U* pData)
{
    M25LC1024_Arrywrite(write_addr, pData, 4);
}
void Eeprom_wwrite(INT32U write_addr, INT8U* pData)
{
    M25LC1024_Arrywrite(write_addr, pData, 2);
}
void Eeprom_bwrite(INT32U write_addr, INT8U* pData)
{
    M25LC1024_Arrywrite(write_addr, pData, 1);
}

/******************************************************************************************************/
/* M25LC1024_Byteread	:	          							      */												
/******************************************************************************************************/
void M25LC1024_Arryread(INT32U read_addr, INT8U* pData, INT32U size)
{
	INT32U address, i;
	
	if(read_addr < EROM_1_START_ADDR){
		if(e2rom_wait.wait0){
			WaitForWriteEnd(0);
		}
		M25LCxx_CS0_ENABLE;            // CS Enable
		address = read_addr - EROM_0_START_ADDR;
	}else{
		if(e2rom_wait.wait1){
			WaitForWriteEnd(EROM_1_START_ADDR);
		}
		M25LCxx_CS1_ENABLE;
		address = read_addr - EROM_1_START_ADDR;
	}
	Send_Receive_byte(READ);        // Write enable instruction 

  /* Send SectorAddr high nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x00FF0000) >> 16);
  /* Send SectorAddr medium nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x0000FF00) >> 8);
  /* Send SectorAddr low nibble address byte */
	Send_Receive_byte((INT8U)(address & 0x000000FF));
  /* read data */
	for(i=0;i<size;i++){
		*(pData+i) = Send_Receive_byte(Dummy_Byte);        // dummy dat for read data
	}
  
	if(read_addr < EROM_1_START_ADDR) M25LCxx_CS0_DISABLE;            // CS Disable 
	else M25LCxx_CS1_DISABLE;

}
void Eeprom_sread(INT32U read_addr, INT8U* pData, INT32U size)
{
    M25LC1024_Arryread(read_addr, pData, size);
}

INT32U Eeprom_lread(INT32U read_addr)
{
    INT32U val;

    M25LC1024_Arryread(read_addr, (INT8U *)&val, 4);
    return val;
}

INT16U Eeprom_wread(INT32U read_addr)
{
    INT16U val;

    M25LC1024_Arryread(read_addr, (INT8U *)&val, 2);
    return val;
}

INT8U Eeprom_bread(INT32U read_addr)
{
    INT8U val;

    M25LC1024_Arryread(read_addr, (INT8U *)&val, 1);
    return val;
}
/******************************************************************************************************/
/* M25LC1024_Powerdown	:	          							      */												
/******************************************************************************************************/
void M25LC1024_Powerdown(INT32U Addr)
{
	if(Addr < EROM_1_START_ADDR) M25LCxx_CS0_ENABLE;            // CS Enable
	else M25LCxx_CS1_ENABLE;
	Send_Receive_byte(PWDOWN);        // Write enable instruction 
	if(Addr < EROM_1_START_ADDR){
		M25LCxx_CS0_DISABLE;            // CS Disable 
		e2rom_wait.wait0 = FALSE;
	}else{
		M25LCxx_CS1_DISABLE;
		e2rom_wait.wait1 = FALSE;
	}
}

/******************************************************************************************************/
/* M25LC1024_Byteread	:	          							      */												
/******************************************************************************************************/
INT8U M25LC1024_Powerup(INT32U Addr)
{
	INT8U data;
	
	if(Addr < EROM_1_START_ADDR) M25LCxx_CS0_ENABLE;            // CS Enable
	else M25LCxx_CS1_ENABLE;
	
	Send_Receive_byte(PWUP);        // Write enable instruction 

  /* Send SectorAddr high nibble address byte */
	Send_Receive_byte(Dummy_Byte);
  /* Send SectorAddr medium nibble address byte */
	Send_Receive_byte(Dummy_Byte);
  /* Send SectorAddr low nibble address byte */
	Send_Receive_byte(Dummy_Byte);
  /* read data */
	data = Send_Receive_byte(Dummy_Byte);        // dummy dat for read data
  
	if(Addr < EROM_1_START_ADDR){
		M25LCxx_CS0_DISABLE;            // CS Disable 
		e2rom_wait.wait0 = FALSE;
	}else{
		M25LCxx_CS1_DISABLE;
		e2rom_wait.wait1 = FALSE;
	}
	return data;
}

/******************* (C) COPYRIGHT 2007 INSEM Inc ***************************************END OF FILE****/

#ifndef __HARDCONFIG_STM32F405RGT6_H
#define __HARDCONFIG_STM32F405RGT6_H

RCC_ClocksTypeDef RCC_Clocks;
RTC_InitTypeDef RTC_InitStructure;

extern void RCC_Init(void);
extern void Rcc_clock_init(void);

#endif
///*****************************************************************************
/*
|*  Copyright       :   (c) 2011 CAS
|*  Filename        :   driver_key.h
|*  Version         :   0.1
|*  Programmer(s)   :   QI ZHY ING (QZI)
|*  Created         :   2011/5/9
|*  Modified        :
|*  Description     :   CT100 key driver function header
*/
//****************************************************************************/
#ifndef __DRIVER_KEY_H__
#define __DRIVER_KEY_H__



#include "globals.h"
#include "stm32f4xx_gpio.h"
#include "RingBuf.h"

extern BOOLEAN KeyLongPressedFlag;
extern BOOLEAN keyPressedFlag;
extern BOOLEAN KeyLongPressFlag;
extern RING_BUF  KeyDrvRingBuf;


extern BOOLEAN KeyCheck(void);
extern void KeyResetPressedFlag(void);
extern INT8U KeyDrvSetLongKey(INT8U rawKey);


#define KEY_H		  0XFF
#define KEY_L		  0X00


#ifndef HIGH
	#define HIGH 1
#endif


#ifndef LOW
	#define LOW 0
#endif

typedef enum{
	KEY_DOWN_FLAG = TRUE, KEY_UP_FLAG = FALSE
}KEY_PRESSED_STATUS;

typedef struct __KEY_CTRL{
	unsigned char value;
	KEY_PRESSED_STATUS pressed_f;
}KEY_CTRL;



#define KEY_PRESET_1	  	 1
#define KEY_PRESET_2	  	 2
#define KEY_PRESET_3	  	 3
#define KEY_PRESET_4	  	 4
#define KEY_PRESET_5	  	 5
#define KEY_PRESET_6	  	 6
#define KEY_PRESET_7	 	 7
#define KEY_PRESET_8	   	 8
#define KEY_PRESET_9	 	 9
#define KEY_PRESET_10	  	10
#define KEY_PRESET_11	  	11
#define KEY_PRESET_12	  	12
#define KEY_PRESET_13	  	13
#define KEY_PRESET_14	  	14
#define KEY_PRESET_15	  	15
#define KEY_PRESET_16	  	16
#define KEY_NOT_PRESSED     0x0


#define KEY_IN_1      GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_1)
#define KEY_IN_2      GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_0)
#define KEY_IN_3      GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_5)
#define KEY_IN_4      GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_4)


#define KEY_OUT_1_H   GPIO_SetBits(GPIOC,GPIO_Pin_0)
#define KEY_OUT_1_L   GPIO_ResetBits(GPIOC,GPIO_Pin_0)
#define KEY_OUT_2_H   GPIO_SetBits(GPIOC,GPIO_Pin_1)
#define KEY_OUT_2_L   GPIO_ResetBits(GPIOC,GPIO_Pin_1)
#define KEY_OUT_3_H   GPIO_SetBits(GPIOC,GPIO_Pin_2)
#define KEY_OUT_3_L   GPIO_ResetBits(GPIOC,GPIO_Pin_2)
#define KEY_OUT_4_H   GPIO_SetBits(GPIOC,GPIO_Pin_3)
#define KEY_OUT_4_L   GPIO_ResetBits(GPIOC,GPIO_Pin_3)

#define TEST_1_H   GPIO_SetBits(GPIOC,GPIO_Pin_9)
#define TEST_1_L   GPIO_ResetBits(GPIOC,GPIO_Pin_9)

#define LED2_D3_H   GPIO_SetBits(GPIOA,GPIO_Pin_3)
#define LED2_D3_L   GPIO_ResetBits(GPIOA,GPIO_Pin_3)
#define LED1_D4_H   GPIO_SetBits(GPIOA,GPIO_Pin_2)
#define LED1_D4_L   GPIO_ResetBits(GPIOA,GPIO_Pin_2)

extern void KEY_Scan(void);
extern void Key_Init(void);




#endif


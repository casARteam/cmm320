///*****************************************************************************
/*
|*  Copyright       :   (c) 2011 CAS
|*  Filename        :   comm_app.h
|*  Version         :   0.1
|*  Programmer(s)   :   Yun Yeo-Chul (YYC)
|*  Created         :   2016/3/22
|*  Modified        :
|*  Description     :   WC300 Temperature Sensor communication function header
*/
//****************************************************************************/
#ifndef _COMM_APP_H_
#define _COMM_APP_H_


#define   NUL             0x00
#define   HT              0x09
#define   VT              0x0b
#define   FF              0x0c
#define   ASC_CR          0x0d
#define   SO              0x0e
#define   SI              0x0f
#define   DC4             0x14
#define   ASC_ESC         0x1b
#define   DEBUG           0x02
#define   ACK             0x06
#define   FS              0x1c
#define   NAK             0x15 

#define COM_MAX_TX_BUF_HOST_SIZE	1024//(600+32)//(MAX_SIZE_MsgBuf+32)
#define COM_MAX_RX_BUF_HOST_SIZE	1024//(600+32)//(MAX_SIZE_MsgBuf+32)

#define DATA_LABEL		'A'



//<<< Error number >>>
#define ERROR_DATA_END		99 // Data end (데이터가 더 이상 없음 upload 시)
#define ERROR_DATA_NON		98 // 해당 고유번호에 해당 하는 데이터가 없음 (예, PLU 20000번 upload시)
#define ERROR_DATA			97 // 데이터 오류 (데이터가 형식 또는 크기가 맞지 않음)
#define ERROR_INTERPRETER	95 // 데이터를 해석 불능, unknown data type char
#define ERROR_DATA_FULL		89 // 저장 공간 없음 (full)
#define ERROR_DMSG_FULL		88 // direct message 저장 공간 없음 (full)
#define ERROR_RECEIVE		84 // 데이터 Receive 오류 (header, tail, checksum)
#define ERROR_NUMBER_OVER	82 // 고유 번호 범위 오류 (예, PLU 번호 1~999999)
#define ERROR_RS485_ID      79 // 해당하는 ID가 아닌 경우
#define ERROR_OPEN_TICKET   78 // 판매는 하였으나 영수증 출력을 안한경우.
#define ERROR_MASTER_DATA   77 // 하나의 네트워크에 마스터가 2개인 상황.

#define DEC_POWER_CONV_TABLE_NUM 12//10
#define HEX_POWER_CONV_TABLE_NUM 8

extern const INT32U DecPowerConvTable[DEC_POWER_CONV_TABLE_NUM];
extern const INT32U HexPowerConvTable[HEX_POWER_CONV_TABLE_NUM];

#pragma pack(1)
typedef struct {
	INT16U No;
	INT16U Offset;
} DATA_HEADER, *lpDATA_HEADER;

typedef struct {
    INT16U No;
    INT8U currentNum;
    INT8U totalNum;
} DATA_RECEIPT_HEADER, *lpDATA_RECEIPT_HEADER;

typedef struct
{
	INT8U  opCode[2];
	INT32U address;
	INT8U  delilimeter;
	INT16U  length;
	INT8U  colon;
} PACKET_HEADER;

typedef struct
{
	INT8U  colon;
	INT8U  checksum;
	INT8U  cr;
} PACKET_TAIL;


#pragma pack()

extern char MsgTempBuf[];
extern void MsgOut(char *str);
//read command
extern void send_ack(COMM_BUF *CBuf);
extern void send_nak(COMM_BUF *CBuf);
extern void send_error_data(COMM_BUF *CBuf, INT8U errNo);
extern void send_good_data(COMM_BUF *CBuf);
extern void network_tx_proc(void);
extern void CommInterpreter(COMM_BUF *CBuf);
//extern void SendAdRawDATA(INT32U data); //kevin_c : 형변환
extern void SendAdRawDATA(char *data); //kevin_c : 형변환



//CM1000
extern INT8U Int32sToUnsignedDecStr(char *dst, INT32U numData, INT8U size, char fillChar, INT8U decPos, char decChar, INT8U useThousandPt);
extern INT8U Int32sToDecStrMachine(char *dst, INT32S numData, INT8U size, char fillChar, INT8U decPos, char decChar, INT8U useThousandPt, INT8U stableFlag);
extern void CommSendData(char *data, INT8U size, INT8U end_flag);
extern void CommSendData3(char *data, INT8U size, INT8U end_flag);


#endif//_COMM_APP_H_
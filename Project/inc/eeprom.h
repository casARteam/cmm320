///*****************************************************************************
/*
|*  Copyright       :   (c) 2011 CAS
|*  Filename        :   driver_eeprom.h
|*  Version         :   0.1
|*  Programmer(s)   :   Tang Qian Kun(TQK)
|*  Created         :   2011/5/9
|*  Modified        :
|*  Description     :   CT100 eeprom driver function header
*/
//****************************************************************************/
#ifndef __DRIVER_EEPROM_H__
#define __DRIVER_EEPROM_H__

#include "globals.h"
#include "stm32f4xx_gpio.h"

typedef struct _EEPROM_WAIT{
	INT8U wait0;
	INT8U wait1;
}EEPROM_WAIT;

/*
BYTE ADDRESS RANGE:
	EROM_1_START_ADDR > EROM_0_START_ADDR
	EEPROM0 EROM_0_START_ADDR+0x00 ~ EROM_0_START_ADDR+0x1FFFF
	EEPROM1 EROM_1_START_ADDR+0x00 ~ EROM_1_START_ADDR+0x1FFFF
*/

#define EROM_0_START_ADDR    0x00	//the second eeprom start address
#define EROM_1_START_ADDR    0x10000000	//the second eeprom start address
#define EROM_PAGE_SIZE            256        //the page size of the eeprom



#define M25LCxx_CS0_DISABLE      GPIO_SetBits(GPIOC,GPIO_Pin_13)     // spi chip selection
#define M25LCxx_CS1_DISABLE      GPIO_SetBits(GPIOC,GPIO_Pin_13)      // spi chip selection

#define M25LCxx_CS0_ENABLE      GPIO_ResetBits(GPIOC,GPIO_Pin_13)     // spi chip selection
#define M25LCxx_CS1_ENABLE      GPIO_ResetBits(GPIOC,GPIO_Pin_13)      // spi chip selection

#define PWDOWN          0xB9  /*power down the chip*/
#define PWUP            0xAB  /*power up the chip*/

#define WRITE           0x02  /* Write to Memory instruction */
#define WRSR            0x01  /* Write Status Register instruction */
#define WREN            0x06  /* Write enable instruction */

#define READ            0x03  /* Read from Memory instruction */
#define RDSR            0x05  /* Read Status Register instruction  */
#define PERASE          0x42  /* Page Erase instruction */
#define SERASE          0xD8  /* Sector Erase instruction */
#define CERASE          0xC7  /* Chip Erase instruction */

#define Dummy_Byte      0xA5  /* dumy data */

#define WIP_Flag        0x01  /* Write In Progress (WIP) flag */


extern void M25LC1024_ChipErase(INT32U Addr);
extern void M25LC1024_SectorErase(INT32U Sector_Addr);
extern void M25LC1024_PageErase(INT32U Page_Addr);
extern void Eeprom_clear(INT32U write_addr, INT8U src, INT32U size);
extern void M25LC1024_Arrywrite(INT32U write_addr, INT8U* pData, INT32U size);
extern void M25LC1024_Arryread(INT32U read_addr, INT8U* pData, INT32U size);
extern void M25LC1024_Powerdown(INT32U Addr);
extern INT8U M25LC1024_Powerup(INT32U Addr);

extern void Eeprom_clear(INT32U write_addr, INT8U src, INT32U size);
extern void Eeprom_swrite(INT32U write_addr, INT8U* pData, INT32U size);
extern void Eeprom_lwrite(INT32U write_addr, INT8U* pData);
extern void Eeprom_wwrite(INT32U write_addr, INT8U* pData);
extern void Eeprom_bwrite(INT32U write_addr, INT8U* pData);
extern void Eeprom_sread(INT32U read_addr, INT8U* pData, INT32U size);
extern INT32U Eeprom_lread(INT32U read_addr);
extern INT16U Eeprom_wread(INT32U read_addr);
extern INT8U Eeprom_bread(INT32U read_addr);
extern void EepromInit(void);
#endif
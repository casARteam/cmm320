#ifndef _HARDWARE_CONFIG_H
#define _HARDWARE_CONFIG_H

RCC_ClocksTypeDef RCC_Clocks;
RTC_InitTypeDef RTC_InitStructure;

extern void RCC_Init(void);
extern void Rcc_clock_init(void);
extern void Gpio_init(void);

#endif
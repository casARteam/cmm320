///*****************************************************************************
/*
|*  Copyright       :   (c) 2011 CAS
|*  Filename        :   timer.h
|*  Version         :   0.1
|*  Programmer(s)   :   Yun Yeo-Chul (YYC)
|*  Created         :   2016/3/22
|*  Modified        :
|*  Description     :   WC300 Temperature Sensor timer function header
*/
//****************************************************************************/
#ifndef _TIMER_H_
#define _TIMER_H_
/*
********************************************************************************
*                       GLOBAL DEFINITIONS & MACROS
********************************************************************************
*/
/* Insert #define here */
/** @brief 10ms Timer 전체 개수 */
#define TIMER_DELAY_10MS_MAX_NUM	4
/** @brief 100ms Timer 전체 개수 */
#define TIMER_DELAY_100MS_MAX_NUM	1

/** @brief 100ms Timer define*/
#define TIMER_DELAY_100MS_SEND_DATA   0


//#define TIMER_DELAY_100MS_DATA_SYNC        2
//#define TIMER_DELAY_100MS_MASTER_CHECK     3
//#define TIMER_DELAY_100MS_SHIFT_CLEAR      4
//#define TIMER_DELAY_100MS_UPRICE_CLEAR     5
//#define TIMER_DELAY_100MS_COLIISION_CHECK  6
//#define TIMER_DELAY_100MS_CONNECT_CHECK    7


/** @brief 10ms Timer define*/
#define TIMER_DELAY_10MS_DELAY             0
#define TIMER_DELAY_10MS_SEND_DELAY        1
#define TIMER_DELAY_10MS_AD_INIT           2
#define TIMER_DELAY_10MS_CHANGE_SENSOR     3

//#define TIMER_DELAY_100MS_MASTER_CHECK     3
//#define TIMER_DELAY_100MS_SHIFT_CLEAR      4
//#define TIMER_DELAY_100MS_UPRICE_CLEAR     5
//#define TIMER_DELAY_100MS_COLIISION_CHECK  6
//#define TIMER_DELAY_100MS_CONNECT_CHECK    7

extern INT16U SysTimer1ms;
extern INT16U SysTimer100ms;
extern INT16U SysTimer10ms;
extern INT16U SysTimer16ms;

/*
********************************************************************************
*                       GLOBAL(EXPORTED) VARIABLE EXTERNALS
********************************************************************************
*/
/* Insert global variable externals here */
extern void TimerInit(void);
extern INT16U TimerDrvGetSysTimer10ms(void);
extern INT16U TimerDrvGetSysTimer100ms(void);
extern INT16U TimerGetSysTimer100ms(void);
extern void TimerSetDelay100ms(INT8U num, INT16U delay);
extern INT8U TimerCheckDelay100ms(INT8U num);
extern void TimerSetDelay10ms(INT8U num, INT16U delay);
extern INT8U TimerCheckDelay10ms(INT8U num);
extern void _nop__(void);
extern void DelayClock(INT16U count);

#define	delay1Clock()	{_nop__();} //test	//delay 1us
#define	delay2Clock()	{_nop__();_nop__();_nop__();} //test	//delay 2us
#define	delay4Clock()	{_nop__();_nop__();_nop__();_nop__();_nop__();_nop__(); _nop__(); _nop__();} //delay 4us
#define	delay16Clock()	{_nop__();_nop__();} //test	//kevin20180622 : 클럭 변경 테스트중
//#define	delay16Clock()	{_nop__();_nop__();_nop__();_nop__();_nop__();_nop__(); _nop__(); _nop__();\
                         _nop__();_nop__();_nop__();_nop__();_nop__();_nop__(); _nop__(); _nop__();\
                         _nop__();_nop__();_nop__();_nop__();_nop__();_nop__(); _nop__(); _nop__();\
                         _nop__();_nop__();_nop__();_nop__();_nop__();_nop__(); _nop__(); _nop__();\
                        } //delay 4us

#define	delay20Clock()	{_nop__();_nop__();_nop__();_nop__();_nop__();_nop__(); _nop__(); _nop__();\
                         _nop__();_nop__();_nop__();_nop__();_nop__();_nop__(); _nop__(); _nop__();\
                         _nop__();_nop__();_nop__();_nop__();_nop__();_nop__(); _nop__(); _nop__();\
                         _nop__();_nop__();_nop__();_nop__();_nop__();_nop__(); _nop__(); _nop__();\
                         _nop__();_nop__();_nop__();_nop__();_nop__();_nop__(); _nop__(); _nop__();\
                        } //delay 4us
#endif //_TIMER_H_

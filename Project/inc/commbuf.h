///*****************************************************************************
/*
|*  Copyright		:	(c) 2002 CAS
|*  Filename		:	commbuf.h
|*  Version		:	0.1
|*  Programmer(s)	:	Hong Yi-Phyo (HYP)
|*  Created		:	2002/12/10
|*  Description		:	CLP Printer communication buffer routine header    
*/
///****************************************************************************/
#ifndef COMMBUF_STRUCT_DEFINE
#define COMMBUF_STRUCT_DEFINE
typedef struct {
	INT8U	Type; 

	char *TxBuf;
	INT16U MaxTxBufSize;
	INT16U TxBufInPtr;
	INT16U TxBufOutPtr;
	BOOLEAN	Txing;

	char *RxBuf;
	INT16U MaxRxBufSize;
	INT16U RxBufInPtr;
	INT16U RxBufOutPtr;
	BOOLEAN	Rxing;
	
	INT8U  SocketID; //0=client, 1~max=server
	INT8U  DestIP[4];
	INT16U DestPort;  
} COMM_BUF;
#endif

#define _COMMBUF_DEFINE_ALL
#ifndef _COMMBUF_H
#define _COMMBUF_H

#define COM_MAX_TX_BUF_UART1_SIZE   1024
#define COM_MAX_RX_BUF_UART1_SIZE   1024
#define COM_MAX_TX_BUF_UART3_SIZE   1024 //kevin uart3
#define COM_MAX_RX_BUF_UART3_SIZE   1024 //kevin uart3


#define COMM_TYPE_UART1	1
#define COMM_TYPE_UART3	3 //kevin uart3


extern COMM_BUF CommBufUart1;      //UART1
extern COMM_BUF CommBufUart3;      //kevin : UART3

extern void CommBufInit(COMM_BUF *CBuf, INT8U type, INT8U num);
extern void FlushTxBuf(COMM_BUF *CBuf);
extern void FlushRxBuf(COMM_BUF *CBuf);
extern void PutCStrTxBuf(COMM_BUF *CBuf, const char *p);
extern void PutStrTxBuf(COMM_BUF *CBuf, char *p);
extern void PutDataRxBuf(COMM_BUF *CBuf, char *p, INT16U count);
extern void PutDataTxBuf(COMM_BUF *CBuf, char *p, INT16U count);

extern void GetDataRxBuf(COMM_BUF *CBuf, char *p, INT16U count);
extern void GetDataTxBuf(COMM_BUF *CBuf, char *p, INT16U count);
extern void GetDataEthDataTxBuf(COMM_BUF *CBuf, char *p, INT16U count);
extern void PutCharTxBuf(COMM_BUF *CBuf, char c);
extern void PutCharRxBuf(COMM_BUF *CBuf, char);
extern char GetCharTxBuf(COMM_BUF *CBuf);
extern char GetCharRxBuf(COMM_BUF *CBuf);
extern INT16U GetRxBufSize(COMM_BUF *CBuf);
extern BOOLEAN CheckRxBuf(COMM_BUF *CBuf);
extern BOOLEAN CheckTxBuf(COMM_BUF *CBuf);
extern BOOLEAN CheckRxBufCount(COMM_BUF *CBuf, INT16U count);
extern INT16U CheckRxBufChars(COMM_BUF *CBuf);
extern INT16U CheckTxBufChars(COMM_BUF *CBuf);
extern int CheckRxBufCountInt(COMM_BUF *CBuf);
extern void RewindRxBuf(COMM_BUF *CBuf, short num);
extern void WindRxBuf(COMM_BUF *CBuf, short num);
extern void RewindTxBuf(COMM_BUF *CBuf);
extern void PutStrTxBuf_copy(COMM_BUF *CBuf_dst, COMM_BUF *CBuf_src);
extern void PutStrTx2Buf_copy(COMM_BUF *CBuf_dst, COMM_BUF *CBuf_src);
extern void PutStrRxBuf_copy(COMM_BUF *CBuf_dst, COMM_BUF *CBuf_src);
extern void Commbuf_Init();
#endif /* _COMMBUF_H */

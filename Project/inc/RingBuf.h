/**
********************************************************************************
* Copyright (c) 2006 CAS
* @brief   Ring Buffer Header File\n
* @file    RingBuf.h
* @version 1.0
* @date    2006/10/02
* @author  Yi Phyo Hong (HYP)
********************************************************************************
* @remark  [Version History]\n
*          v1.0 2006/10/02 created by HYP \n
********************************************************************************
*/

#ifndef _RING_BUF_H
#define _RING_BUF_H

#ifndef _BITDEFINE
#include "globals.h"
#endif

/*
********************************************************************************
*                       GLOBAL DEFINITIONS & MACROS
********************************************************************************
*/
/* Insert #define here */
/** @brief Ring Buffer struct*/
typedef struct {
	/** @brief Byte Data Array */
	char *Buf;
	/** @brief Byte Data Array Size*/
	INT16U Size;
	/** @brief Array Position for Input */
	INT16U InPtr;
	/** @brief Array Position for output */
	INT16U OutPtr;
} RING_BUF;


/** @brief Protocol Header */
typedef struct {
	char cmd1;
	char cmd2[2];
	char deviceID;
	char length;
	char data[32];
	char bcc;
} COMM_PROTOCOL_STRUCTURE;

extern COMM_PROTOCOL_STRUCTURE CommStr;

/*
********************************************************************************
*                       GLOBAL DATA TYPES & STRUCTURES
********************************************************************************
*/
/* Insert global typedef & struct here */


/*
********************************************************************************
*                       GLOBAL(EXPORTED) VARIABLE EXTERNALS
********************************************************************************
*/
/* Insert global variable externals here */


/*
********************************************************************************
*                       GLOBAL(EXPORTED) FUNCTION PROTOTYPES
********************************************************************************
*/
/* Insert global function prototypes here */

extern void RingBufInit(RING_BUF *rBuf, char *buf, INT16U size);
extern void RingBufFlushData(RING_BUF *rBuf);
extern void RingBufPutChar(RING_BUF *rBuf, char byte);
extern char RingBufGetChar(RING_BUF *rBuf);
extern void RingBufRewindChar(RING_BUF *rBuf);

extern void RingBufPutInt16u(RING_BUF *rBuf, INT16U word);
extern INT16U RingBufGetInt16u(RING_BUF *rBuf);
extern void RingBufPutInt32u(RING_BUF *rBuf, INT32U dword);
extern INT32U RingBufGetInt32u(RING_BUF *rBuf);
extern void RingBufPutInt64sForMog(RING_BUF *rBuf, INT64S dword);
extern INT64S RingBufGetInt64s(RING_BUF *rBuf);
extern void RingBufPutInt32sForAdDrv(RING_BUF *rBuf, INT32S dword);
extern void RingBufPutInt32s(RING_BUF *rBuf, INT32S dword);
extern INT32S RingBufGetInt32s(RING_BUF *rBuf);
extern INT32S RingBufGetInt32sPrevData(RING_BUF *rBuf, INT16U prevCnt) ;

extern void RingBufPutData(RING_BUF *rBuf, char *src, INT16U size);
extern void RingBufPutStr(RING_BUF *rBuf, char *src);
extern INT16U RingBufCountSize(RING_BUF *rBuf);
extern INT8U RingBufCheckBuf(RING_BUF *rBuf);
extern INT16U RingBufCheckMaxBufSize(RING_BUF *rBuf);

extern void RingBufPutCharForKeyInterrupt(RING_BUF *rBuf, char byte);
extern void RingBufPutCharForCommInterrupt(RING_BUF *rBuf, char byte);
extern char RingBufGetCharForCommInterrupt(RING_BUF *rBuf);
extern INT8U RingBufCheckBufForCommInterrupt(RING_BUF *rBuf);

#endif /* _RING_BUF_H */


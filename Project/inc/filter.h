/*****************************************************************************
|*			
|*  Copyright		:	(c) 2002 CAS
|*  Filename		:	filter.c
|*  Version			:	0.1
|*  Programmer(s)	:	Yun Yeo Chul (YYC)
|*  Created			:	2016/07/11
|*  Modify              :       
|*  Description		:	WC300 MOG System
|*				
****************************************************************************/





#ifndef _FILTER_H
#define _FILTER_H

#define MAF_BUF_MAX_SIZE	500//300 -> 1kg
#define MAF_7TH_ORDER_BUF_MAX_SIZE	2100

#define MAF_1ST_ORDER   0x00
//#define MAF_2ND_ORDER   0x01
//#define MAF_3RD_ORDER   0x02
//#define MAF_4TH_ORDER   0x03
//#define MAF_5TH_ORDER   0x04
//#define MAF_6TH_ORDER   0x05
//#define MAF_CREEP_ORDER   0x06
//#define MAF_CREEPTRACKING_ORDER   0x07
//#define MAF_7TH_ORDER  0x08
//#define MAF_DCTRACKING_ORDER 0x09



// 20160718_CYCLE_1.csv
//#define MAF_1ST_ORDER_WIN_SIZE   (4*30-4) //  17.24 Hz
//#define MAF_2ND_ORDER_WIN_SIZE   (4*44+3) // 11.17 Hz
//#define MAF_3RD_ORDER_WIN_SIZE   (4*35-1) //14.39 Hz
//#define MAF_4TH_ORDER_WIN_SIZE   (4*37-1) //13.61 Hz
//#define MAF_5TH_ORDER_WIN_SIZE   (4*44+3) //11.17 Hz
//#define MAF_6TH_ORDER_WIN_SIZE   (4*60-2) //8.40 Hz

//20160721_800g_com_check_100Mhz_CYCLE_MAGNET_2EA_1_temp
//#define MAF_1ST_ORDER_WIN_SIZE   (4*59) //  17.24 Hz
//#define MAF_2ND_ORDER_WIN_SIZE   (4*31+2) // 11.17 Hz
//#define MAF_3RD_ORDER_WIN_SIZE   (4*35-3) //14.39 Hz
//#define MAF_4TH_ORDER_WIN_SIZE   (4*36+2) //13.61 Hz
//#define MAF_5TH_ORDER_WIN_SIZE   (4*46+1) //11.17 Hz
//#define MAF_6TH_ORDER_WIN_SIZE   (4*57) //8.40 Hz
//#define MAF_CREEP_ORDER_WIN_SIZE   (4*60-2) //8.40 Hz
//#define MAF_CREEPTRACKING_ORDER_WIN_SIZE   (4*60-2) //8.40 Hz

//20160721_800g_com_check_100Mhz_CYCLE_MAGNET_2EA_1_temp

#define MAF_1ST_ORDER_WIN_SIZE   (50) //(9.26 Hz)
//#define MAF_2ND_ORDER_WIN_SIZE   2*(4*55) //(9.09 Hz)
//#define MAF_3RD_ORDER_WIN_SIZE   2*(4*54+2) //(9.17 Hz)
//#define MAF_4TH_ORDER_WIN_SIZE   2*(4*54-1)//(9.30 Hz)
//#define MAF_5TH_ORDER_WIN_SIZE   2*(4*55+3)//(8.97 Hz)
//#define MAF_6TH_ORDER_WIN_SIZE   2*(4*58+1) //(8.58 Hz)
//#define MAF_7TH_ORDER_WIN_SIZE   2015
//#define MAF_CREEP_ORDER_WIN_SIZE   (8*54-1) //8.40 Hz
//#define MAF_CREEPTRACKING_ORDER_WIN_SIZE   (8*54-1) //8.40 Hz
//#define MAF_DCTRACKING_ORDER_WIN_SIZE   (5) //8.40 Hz

//DC Tracking Filter
#define B_ZERO          3
#define A_ONE           10
#define Q_FIFTEEN      (B_ZERO+A_ONE)
//DC Tracking Filter




#pragma pack(8)
typedef struct{
    INT32U index;
    INT32U winSize;
    INT32U mafOnFlag;
    INT32U reserved;
    INT32U *buf;
}MAF_STRUCT;
#pragma pack()

extern FP64 Pk_1st;
extern FP64 varP_1st;
extern FP64 R_1st;
extern FP64 Kk_1st;
extern FP64 Xk_1st;

extern FP64 Pk_2nd;
extern FP64 varP_2nd;
extern FP64 R_2nd;
extern FP64 Kk_2nd;
extern FP64 Xk_2nd;


extern MAF_STRUCT Maf1stOrder;
extern MAF_STRUCT Maf2ndOrder;
extern MAF_STRUCT Maf3rdOrder;
extern MAF_STRUCT Maf4thOrder;
extern MAF_STRUCT Maf5thOrder;
extern MAF_STRUCT Maf6thOrder;
extern MAF_STRUCT Maf7thOrder;
extern MAF_STRUCT MafCreepOrder;
extern MAF_STRUCT MafCreepTrackingOrder;
extern MAF_STRUCT MafDCTrackingOrder;

extern INT32U MAF_buf_1st_order[MAF_BUF_MAX_SIZE];
//extern FP64 MAF_buf_2nd_order[MAF_BUF_MAX_SIZE];
//extern FP64 MAF_buf_3rd_order[MAF_BUF_MAX_SIZE];
//extern FP64 MAF_buf_4th_order[MAF_BUF_MAX_SIZE];
//extern FP64 MAF_buf_5th_order[MAF_BUF_MAX_SIZE];
//extern FP64 MAF_buf_6th_order[MAF_BUF_MAX_SIZE];
//extern FP64 MAF_buf_Creep_order[MAF_BUF_MAX_SIZE];
//extern FP64 MAF_buf_CreepTacking_order[MAF_BUF_MAX_SIZE];
//extern FP64 MAF_buf_DCTracking_order[MAF_BUF_MAX_SIZE];
//extern FP64 DctFilterOut;;

extern FP64 WC300_KalmanFilter_1st(FP64 rawData);
extern FP64 WC300_KalmanFilter_2nd(FP64 rawData);
extern INT32U WC300_MovingAVG(MAF_STRUCT *MAFBuf, INT32U inData);
extern void WC300_MAF_Init(MAF_STRUCT *MAFBuf, INT32U windowSize,INT8U order);
extern INT32U DC_Tracking_Filter(INT32U inData);

extern void MAFSInit(INT16U winMinSize, INT16U winMaxSize, INT32U winIncThres, INT32U winBreakThres, INT16U shockWinSize, INT32U shockThres, INT32U thresIncThres);
extern INT32U MAFSProc(INT32U inData);

extern void FilterSettingInit(void);
extern void Wc_filterInit(void);
#endif
#ifndef __GLOBALS_H
#define __GLOBALS_H

//typedef unsigned char BOOLEAN; /* Logical data type (TRUE or FALSE) */
//typedef unsigned char INT8U; /* Unsigned 8 bit value */
//typedef signed char INT8S; /* Signed 8 bit value */
//typedef unsigned short INT16U; /* Unsigned 16 bit value */
//typedef signed short INT16S; /* Signed 16 bit value */
//typedef unsigned long INT32U; /* Unsigned 32 bit value */
//typedef signed long INT32S; /* Signed 32 bit value */
//typedef float FP32; /* 32 bit, single prec. floating-point */
//typedef double FP64; /* 64 bit, double prec. floating-point */
//typedef double INT64S;


typedef unsigned char BOOLEAN; /* Logical data type (TRUE or FALSE) */
typedef unsigned char INT8U; /* Unsigned 8 bit value */
typedef signed char INT8S; /* Signed 8 bit value */
typedef unsigned short INT16U; /* Unsigned 16 bit value */
typedef signed short INT16S; /* Signed 16 bit value */
typedef unsigned int INT32U; /* Unsigned 32 bit value */
typedef signed int INT32S; /* Signed 32 bit value */
typedef float FP32; /* 32 bit, single prec. floating-point */
typedef double FP64; /* 64 bit, double prec. floating-point */
typedef double INT64S;


#define _PACKED_DEF __packed
#define ROMDATA	const
#define HUGEDATA
#define ON  1
#define OFF 0

#define TRUE  1
#define FALSE 0

#define AD_DRV_AD7177 //32bit ADC
//#define AD_DRV_AD7714 //24bit ADC

#define LOWCAPA     0           // 공진기
#define HIGHCAPA    1           // 간섭계 

extern unsigned char TempCapaSetting;

//#define WEIGHT_CAPA_10G  //Off: 1,000g

#define USE_BLOCK_TEMP_SENSOR //block temp sensor

#define USE_INT_WEIGHT_SPAN_CAL
#ifdef WEIGHT_CAPA_10G
#define INT_WEIGHT_VALUE    100000100 //10,000.0100 mg
#else
//#define INT_WEIGHT_VALUE    10000002 //100,000.02 mg
//#define INT_WEIGHT_VALUE    20000005 //200,000.05 mg
//#define INT_WEIGHT_VALUE    50000002 //500,000.02 mg
#define INT_WEIGHT_VALUE    100000004 //1,000,000.04 mg
//#define INT_WEIGHT_VALUE    (-9252400) //500,000.02 mg
#endif

#define USE_MAFS_FILTER //yphong 20170808
#define USE_LINEAR_COMP
//#define USE_LINEAR_2ND_ORDER_COMP   //zero ad
#define USE_LINEAR_2ND_ORDER_COMP_NO_ZERO  //whole ad
//#define USE_LINEAR1_SET_ZERO //linear1 zero
#define USE_LINEAR2_SET_ZERO //normAd(linear2) zero

#define USE_INDI_CAL
//#define USE_DISP_TEMP
#define USE_TEMP_COMP //temp. compensation
#define TEMP_COMP_RATE	1.0//보상 수준
#define TEMP_COMP_RATE_SPAN 1.0
#define TEMP_COMP_RATE_ZERO 1.0

#define USE_SEND_WEGHT2MACHINE  // T= 1000,000.00mgS

#define USE_AUTO_ZERO_TRACKING_PROC
#ifdef WEIGHT_CAPA_10G
#define AUTO_ZERO_TRACKING_RANGE	500
#define AUTO_ZERO_TRACKING_WEIGHT	100000 //10.0000 mg
#define AUTO_ZERO_TRACKING_SPEED	250//10 //max : AUTO_ZERO_TRACKING_RANGE
#else
#define AUTO_ZERO_TRACKING_RANGE	5
#define AUTO_ZERO_TRACKING_WEIGHT	10000 //100.00 mg
#define AUTO_ZERO_TRACKING_SPEED	1 //max : AUTO_ZERO_TRACKING_RANGE
#endif

#ifdef WEIGHT_CAPA_10G
#define MAFS_CAL_COFF   140//28  // 1d != 10 raw adc value
#define MAFS_SRATE      10  //10 sps
#define STABLE_SEND_DETACT_RANGE    (25*MAFS_CAL_COFF) //100 1000 - mafs_coff 14
#define STABLE_CAL_DETACT_RANGE    (25*MAFS_CAL_COFF) //50 1000 - mafs_coff 14
#define STABLE_SEND_DETACT_TIME     (3*MAFS_SRATE)
#else
#define MAFS_CAL_COFF   14  // 1d = 14 raw adc value
//#define MAFS_SRATE      1660//120  //100 sps
#define MAFS_SRATE      10000//120  //100 sps
#define STABLE_SEND_DETACT_RANGE    (50*MAFS_CAL_COFF) //(500*MAFS_CAL_COFF)  //norm value
#define STABLE_CAL_DETACT_RANGE    (10*MAFS_CAL_COFF) //(200*MAFS_CAL_COFF)  //norm value
#define STABLE_SEND_DETACT_TIME     (1*MAFS_SRATE) //(10*MAFS_SRATE)  //sec
#endif

#define KEY_COUNT_MAX   (MAFS_SRATE/10)

//#define USE_HOLD
#ifdef WEIGHT_CAPA_10G
#define HOLD_COMP_RANGE 1000000//100mg
#else
#define HOLD_COMP_RANGE 500000//5g
#endif

//yphong 20170808
#define USE_ZERO_TRACKING
#ifdef WEIGHT_CAPA_10G
#define ZERO_TRACKING_RANGE 1000//500//250//175//100//75//10
//#define ZERO_TRACKING_LONG_RANGE 10
//#define ZERO_TRACKING_LONG_RANGE_TIME (2*MAFS_SRATE)
//#define ZERO_TRACKING_SPEED (ZERO_TRACKING_RANGE/2)// max speed: ZERO_TRACKING_RANGE
#else
#define ZERO_TRACKING_RANGE 5//10//25//10
//#define ZERO_TRACKING_LONG_RANGE 10
//#define ZERO_TRACKING_LONG_RANGE_TIME (2*MAFS_SRATE)
//#define ZERO_TRACKING_SPEED 5// max speed: ZERO_TRACKING_RANGE
#endif
#define STABLE_RANGE 5//12//5

//#define USE_AUTO_ZERO_KEY  //yphong 20170808
#define AUTO_ZERO_WEIGHT    5000

#define USE_WEIGHT_COMP
#ifdef WEIGHT_CAPA_10G
#define WEIGHT_COMP_RANGE   50000
#define WEIGHT_COMP_PERCENT     95
#else
#define WEIGHT_COMP_RANGE   2000
#define WEIGHT_COMP_PERCENT     98
#endif

//#define USE_STABLE_WEIGHT_COMP

//#define USE_ZERO_DISP_FILTER //yphong 20170808
#define ZERO_DISP_FILT_RANGE    WEIGHT_COMP_RANGE//500

#endif /* __GLOBALS_H */
#ifndef _AD_DRV_H
#define _AD_DRV_H
/**
********************************************************************************
* Copyright (c) 2017 CAS
* @brief   AD Driver Header File\n
* @file    AdDrv.h
* @version 1.0
* @date    2017/2/16
* @author  Yun Yeo Chul (YYC)
********************************************************************************
* @remark 
********************************************************************************
*/
#include "Globals.h"
#include "RingBuf.h"
#include "stm32f4xx_gpio.h"

/*
********************************************************************************
*                       GLOBAL DEFINITIONS & MACROS
********************************************************************************
*/
/* Insert #define here */

/*
********************************************************************************
*                       GLOBAL DATA TYPES & STRUCTURES
********************************************************************************
*/
/* Insert global typedef & struct here */
//
#if defined(AD_DRV_AD7714)
#define AD_DATA_IN_HIGH     GPIO_SetBits(GPIOA,GPIO_Pin_7) 
#define AD_DATA_IN_LOW      GPIO_ResetBits(GPIOA,GPIO_Pin_7) 

#define AD_CLK_HIGH         GPIO_SetBits(GPIOA,GPIO_Pin_5) 
#define AD_CLK_LOW          GPIO_ResetBits(GPIOA,GPIO_Pin_5) 

#define AD_DATA_OUT         GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_6)
#define AD_DRDY             GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_4)

#define AD_RESET_HIGH      //GPIO_SetBits(GPIOA,GPIO_Pin_3)
#define AD_RESET_LOW        //GPIO_ResetBits(GPIOA,GPIO_Pin_3)

#elif defined(AD_DRV_AD7177)

#define AD_7177_DOUT_DRDY		GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_6)

#define AD_7177_DATA_IN_HIGH     GPIO_SetBits(GPIOA,GPIO_Pin_7) //STM32F405RG : PA7 PIN23
#define AD_7177_DATA_IN_LOW      GPIO_ResetBits(GPIOA,GPIO_Pin_7) 


#define AD_7177_SYNC_HIGH      GPIO_SetBits(GPIOA,GPIO_Pin_4) //STM32F405RG : PA4 PIN20 
#define AD_7177_SYNC_LOW       GPIO_ResetBits(GPIOA,GPIO_Pin_4) 

#define AD_7177_CLK_HIGH         GPIO_SetBits(GPIOA,GPIO_Pin_5) //STM32F405RG : PA5 PIN21
#define AD_7177_CLK_LOW          GPIO_ResetBits(GPIOA,GPIO_Pin_5) 

#endif

//Temp AD LTC2420 GPIO Port
#define TEMP_AD_CLK_HIGH    GPIO_SetBits(GPIOA,GPIO_Pin_0)
#define TEMP_AD_CLK_LOW     GPIO_ResetBits(GPIOA,GPIO_Pin_0)

#define TEMP_AD_DATA_OUT    GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_1)

/*
********************************************************************************
*                       GLOBAL(EXPORTED) VARIABLE EXTERNALS
********************************************************************************
*/
/* Insert global variable externals here */
extern RING_BUF *adRingBufPtr;
extern RING_BUF *adWeightRingBufPtr; //kevin20180627 ADC CH0 데이터를 읽어오기위한 링버퍼 추가
extern RING_BUF *MagTempAdRingBufPtr;
extern RING_BUF *BlockTempAdRingBufPtr;
/*
********************************************************************************
*                       GLOBAL(EXPORTED) FUNCTION PROTOTYPES
********************************************************************************
*/
#if defined(AD_DRV_AD7714)
/* Insert global function prototypes here */
extern void AdDrvInit(void);

extern INT16U AdDrvGetSamplingRate(void);
extern void AdDrvProc(void);

#elif defined(AD_DRV_AD7177)
extern void AdDrvInit(void);

extern INT16U AdDrvGetSamplingRate(void);
extern void AdDrvProc(void);

#endif
extern void TempAdDrvProc(void);
extern void TempAdDrvInit(void);
#endif /* _AD_DRV_H */

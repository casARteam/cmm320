#ifndef _ADM_DEF_H
#define _ADM_DEF_H




#define	NUM_ADBUFFER		24//14
//#define EXT_RESOLUTION		3000


#include <math.h>		//labs()
#include <string.h>		//memset()

#define	WAIT_4CYCLE()	

typedef unsigned char	BYTE;
typedef unsigned short	WORD;
/*
typedef union {
	unsigned int 	word;
	struct {
		BYTE	high;
		BYTE	low;
	} byte;
} UNION_WORD;
*/ //big endian
#pragma pack(1)
typedef union {
	unsigned short int 	word;
	struct {
		BYTE	low;
		BYTE	high;
	} byte;
} UNION_WORD; //little endian
#pragma pack()


#define		NUM_RCV_BUFFER	16//8


#endif /* _ADM_DEF_H */
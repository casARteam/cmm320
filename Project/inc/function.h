///*****************************************************************************
/*
|*  Copyright		:	(c) 2002 CAS
|*  Filename		:	function.h
|*  Version		:	0.1
|*  Programmer(s)	:	SeungHo Jeong (HYP)
|*  Created		:	2018/07/10
|*  Description		:	header    
*/
///****************************************************************************/


extern void Rcc_clock_init(void);
extern void Timer2_init(void);
extern void Usart1_init(void);
extern void Nvic_init(void);
extern void SPI_Config(void);


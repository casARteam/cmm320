#ifndef __BUZZER_H
#define __BUZZER_H

typedef struct {
	char Times;
	char Status;
} BUZZER_STR;

extern HUGEDATA BUZZER_STR Buzzer;	

#define BUZZER_HIGH    GPIO_SetBits(GPIOA,GPIO_Pin_11) 
#define BUZZER_LOW     GPIO_ResetBits(GPIOA,GPIO_Pin_11) 


void buzzer_init(void);
void Buzzer_On(INT8U cnt);
void BuzOnAdd(INT8U cnt);

#endif /* __BUZZER_H */
#ifndef __LCD_H
#define __LCD_H

#define LCD_C1      0x22
#define LCD_C2 	    0x24
#define LCD_C3 		0x28
#define LCD_C4 		0x30
#define LCD_C123	0x3E
#define LCD_DAT		0x01
#define LCD_CMD		0x00
#define LCD_LED 	0x00
#define LCD_RST 	0x20


#define LCD_CHAR_FONT_WIDTH      6
#define LCD_X_MARGIN			32
#define LCD_X_MARGIN_REAL		48
#define LCD_Y_MARGIN			202
#define LCD_Y_MARGIN_REAL		208
#define DSP_BUF_SIZE			(LCD_Y_MARGIN*(LCD_X_MARGIN/8))
#define DSP_BUF_SIZE_REAL		(LCD_Y_MARGIN_REAL*(LCD_X_MARGIN_REAL/8))

#define LCD_6X8_MODE  0x00
#define LCD_8X16_MODE 0x01



extern INT8U DspBuf[DSP_BUF_SIZE_REAL];
extern INT16U LcdPtr;
extern INT16U LCDAddr;
extern void init_lcd(void);
extern void lcd_set(void);
extern void Dsp_Clear(void);
extern void write_1byte_char(INT8U ch);
extern void write_7SEG_1byte_char(INT8U ch);
extern void write_dspBuf_data(INT8U *str, INT16U sLength);
extern void Dsp_Diffuse(void);

//CMB1000
extern void DispWeightData(INT32S WeightData, INT8U opt);


#endif /* __LCD_H */
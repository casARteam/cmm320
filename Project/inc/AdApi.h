#ifndef _AD_API_H
#define _AD_API_H

#define ADC1_DR_Address    ((u32)0x4001244C)



void AdcInit(void);
INT32U AdGetFilterdRawData(void);

extern INT32U AdGetRawData(void);
extern INT32S AdNormProc(INT32U rawData);
extern INT8U MAFStableFlag;
extern INT32U AdNoFilteredRawData;;


//CMB1000
extern INT8U StableSendDetactFlag;

#endif /* _AD_API_H */

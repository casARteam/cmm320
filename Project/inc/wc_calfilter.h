#ifndef __WCFILTER_H
#define __WCFILTER_H


#define MAF_1ST_WC_ORDER        0
#define SCALESELECTADD          0


/*low capa kel filter �� */
extern FP64 LowCapa_Pk_1st;
extern FP64 LowCapa_varP_1st;
extern FP64 LowCapa_R_1st;
extern FP64 LowCapa_Kk_1st;
extern FP64 LowCapa_Xk_1st;

extern FP64 LowCapa_Pk_2nd;
extern FP64 LowCapa_varP_2nd;
extern FP64 LowCapa_R_2nd;
extern FP64 LowCapa_Kk_2nd;
extern FP64 LowCapa_Xk_2nd;

extern INT32U LowMoving1stwinsize;

/*high capa kel filter �� */
extern FP64 HighCapa_Pk_1st;
extern FP64 HighCapa_varP_1st;
extern FP64 HighCapa_R_1st;
extern FP64 HighCapa_Kk_1st;
extern FP64 HighCapa_Xk_1st;

extern FP64 HighCapa_Pk_2nd;
extern FP64 HighCapa_varP_2nd;
extern FP64 HighCapa_R_2nd;
extern FP64 HighCapa_Kk_2nd;
extern FP64 HighCapa_Xk_2nd;

extern INT32U HighMoving1stwinsize;

extern void CalsettingInit(void);
extern INT32S AdLinearCompProc(INT32U rawData);
extern void EepromCalDataRead(void);


//CMB1000
extern INT8U AdLinearSetZero(INT32U rawData);
extern INT32U AdTempComp(INT32U rawData, INT32U magTemp);
extern void CalLinearCoffSetting(INT8U calFlag);
extern INT8U HoldFlag;
extern INT32S HoldWeight;
extern INT8U TempCompOnFlag;

#endif /* __BUZZER_H */